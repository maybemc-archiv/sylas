package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.notify.Notifier;
import net.maybemc.sylas.protocol.BanBypassDetectedPacket;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class BanBypassDetectedPacketListener implements PacketListener<BanBypassDetectedPacket> {
    private final Notifier notifier;

    public BanBypassDetectedPacketListener(Notifier notifier) {
        this.notifier = notifier;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection connection, BanBypassDetectedPacket packet) {
        notifier.notifyBanBypass(packet.getTestedPlayer(), packet.getAccounts());
        return instantResult(continueHandling());
    }
}
