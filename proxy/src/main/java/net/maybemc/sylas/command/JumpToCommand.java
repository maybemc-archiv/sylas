package net.maybemc.sylas.command;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.sylas.protocol.JumpToPacket;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
public class JumpToCommand extends Command {
    private final LanguagePack languagePack;
    private final PacketClient packetClient;

    public JumpToCommand(LanguagePack languagePack, PacketClient packetClient) {
        super("jumpto", "sylas.jumpto.command", "goto");
        this.languagePack = languagePack;
        this.packetClient = packetClient;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        UserRepository userRepository = Meybee.getInstance().getProvider(UserRepository.class);

        if (args.length == 1) {
            Futures.addCallback(userRepository.findUsers(args[0]), new FutureCallback<List<User>>() {
                @Override
                public void onSuccess(List<User> users) {
                    if (users.isEmpty()) {
                        player.sendMessage(languagePack.translate(player.getUniqueId(), "jumpto-command-target-not-found"));
                    } else {
                        execute(player, users.get(0).getUniqueId());
                    }
                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {
                    FutureCallback.super.onFailure(throwable);
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "jumpto-command-error", throwable.getMessage()));
                }
            });
        } else {
            player.sendMessage(languagePack.translate(player.getUniqueId(), "jumpto-command-usage"));
        }
    }

    private void execute(ProxiedPlayer player, UUID target) {
        Futures.addCallback(packetClient.sendPacket(new JumpToPacket(player.getUniqueId(), target), Boolean.class), new FutureCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                if (!aBoolean) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "jumpto-command-target-not-found"));
                } else {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "jumpto-command-success"));
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                FutureCallback.super.onFailure(throwable);
                player.sendMessage(languagePack.translate(player.getUniqueId(), "jumpto-command-error", throwable.getMessage()));
            }
        });
    }
}
