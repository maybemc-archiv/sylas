package net.maybemc.sylas.spectate.listener;

import me.lucko.helper.scheduler.HelperExecutors;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.protocol.SpectatePacket;
import net.maybemc.sylas.spectate.SpectatorRepository;
import org.bukkit.Bukkit;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class SpectatePacketListener implements PacketListener<SpectatePacket> {
    private final SpectatorRepository repository;

    public SpectatePacketListener(SpectatorRepository repository) {
        this.repository = repository;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, SpectatePacket packet) {
        return CompletableFuture.supplyAsync(() -> {
            if (packet.getTarget_() != null) {
                repository.prepareSpectator(packet.getSpectator(), Bukkit.getPlayer(packet.getTarget_()));
            } else {
                repository.prepareSpectator(packet.getSpectator(), null);
            }
            return continueHandling(packet);
        }, HelperExecutors.sync());
    }
}
