package net.maybemc.sylas.repository.report;

import net.maybemc.sylas.schema.ban.BanReason;
import net.maybemc.sylas.schema.report.Report;
import net.maybemc.sylas.schema.report.ReportJudgement;
import net.maybemc.sylas.schema.report.ReportReason;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public interface ReportRepository {

    CompletableFuture<List<ReportReason>> listReportReasons();

    Optional<ReportReason> findReportReason(String id);

    CompletableFuture<String> report(
        @NotNull String issuer,
        @NotNull UUID target,
        @NotNull ReportReason reason,
        @Nullable String appendant,
        @Nullable List<String> flags
    );

    CompletableFuture<Report> getReportSelection(
        @NotNull UUID moderator
    );

    CompletableFuture<String> judgeReport(
        @NotNull Report report,
        @NotNull ReportJudgement judgement,
        @Nullable UUID moderator,
        @Nullable BanReason banReason
    );

    CompletableFuture<Integer> collectRewards(
        @NotNull UUID player
    );

}
