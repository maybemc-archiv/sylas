package net.maybemc.sylas.protocol;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
public class KickInfoPacket extends KickPacket {
    public KickInfoPacket(UUID issuer, UUID pTarget, String reason) {
        super(issuer, pTarget, reason);
    }

    public KickInfoPacket() {
    }
}
