package net.maybemc.sylas.schema.report;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Getter
public enum ReportStatus {
    OPEN(false, false),
    CLOSED_JUSTIFIED(true, true),
    CLOSED_UNJUSTIFIED(true, false),
    CLOSED_TIMEOUT(true, false);

    private final boolean closed;
    private final boolean justified;
}
