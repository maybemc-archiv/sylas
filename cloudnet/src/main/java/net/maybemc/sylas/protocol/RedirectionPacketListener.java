package net.maybemc.sylas.protocol;

import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServer;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class RedirectionPacketListener<P extends Packet> implements PacketListener<P> {
    private final PacketServer packetServer;
    private final String group;

    public RedirectionPacketListener(PacketServer packetServer, String group) {
        this.packetServer = packetServer;
        this.group = group;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, P packet) {
        for (PacketServerConnection proxy : packetServer.findConnectionsByGroup(group)) {
            proxy.sendUnimportantPacket(packet);
        }
        return instantResult(stopHandling());
    }
}
