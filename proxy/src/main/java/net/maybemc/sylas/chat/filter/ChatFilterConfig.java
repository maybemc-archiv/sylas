package net.maybemc.sylas.chat.filter;

import com.google.gson.Gson;
import lombok.Getter;
import net.maybemc.meybee.api.json.Config;
import net.maybemc.meybee.api.json.GsonClassAdapter;
import net.maybemc.meybee.api.json.GsonCreator;
import net.maybemc.meybee.api.json.GsonProvider;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

/**
 * @author Nico_ND1
 */
@Getter
@Config(filePath = "*/chat-filter")
public class ChatFilterConfig implements GsonCreator {
    private List<ChatFilter> chatFilter;
    private Map<Character, Character> messageReplacements;

    @Override
    public @NotNull Gson createGson(@NotNull GsonProvider gsonProvider) {
        return gsonProvider.createBuilder()
            .registerTypeAdapter(ChatFilter.class, new GsonClassAdapter<>()
                .registerType("message", SimpleRegexChatFilter.class)
                .registerType("spam", SpamChatFilter.class)
                .registerType("list", WordListChatFilter.class)
            ).create();
    }
}
