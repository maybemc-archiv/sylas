package net.maybemc.sylas.spectate;

import me.lucko.helper.item.ItemStackBuilder;
import me.lucko.helper.menu.Item;
import me.lucko.helper.menu.scheme.MenuScheme;
import me.lucko.helper.scheduler.HelperExecutors;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.meybee.api.user.scope.nick.Nick;
import net.maybemc.meybee.spigot.config.GlobalDesignConfig;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Nico_ND1
 */
public class SpectatePlayersGui {
    public static void openGui(Player player, LanguagePack languagePack) {
        UserRepository userRepository = Meybee.getInstance().getProvider(UserRepository.class);
        User user = userRepository.getUser(player.getUniqueId());

        List<Item> items = new ArrayList<>();
        AtomicInteger operationCount = new AtomicInteger(Bukkit.getOnlinePlayers().size() - 1);
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            if (onlinePlayer.equals(player)) {
                continue;
            }

            User onlineUser = userRepository.getUser(onlinePlayer.getUniqueId());
            Futures.addCallback(onlineUser.getNickFor(user), new FutureCallback<>() {
                @Override
                public void onSuccess(Nick nick) {
                    items.add(ItemStackBuilder.of(Material.PLAYER_HEAD)
                        .texture(nick.getSkin().getValue())
                        .name(languagePack.translate(user, "spectate-session-players-entry", nick.getDisplayName()))
                        .buildConsumer(event -> {
                            if (onlinePlayer.isOnline()) {
                                player.teleport(onlinePlayer);
                            } else {
                                player.sendMessage(languagePack.translate(user, "spectate-session-players-entry-offline"));
                            }
                        }));
                    continue_();
                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {
                    items.add(ItemStackBuilder.of(Material.PLAYER_HEAD)
                        .owner(((CraftPlayer) onlinePlayer).getProfile())
                        .name(languagePack.translate(user, "spectate-session-players-entry", onlinePlayer.getDisplayName()))
                        .buildConsumer(event -> {
                            if (onlinePlayer.isOnline()) {
                                player.teleport(onlinePlayer);
                            } else {
                                player.sendMessage(languagePack.translate(user, "spectate-session-players-entry-offline"));
                            }
                        }));
                    continue_();
                }

                private void continue_() {
                    if (operationCount.decrementAndGet() == 0) {
                        GlobalDesignConfig designConfig = Meybee.getInstance().getProvider(GlobalDesignConfig.class);

                        designConfig.createPaginatedGuiBuilder(player)
                            .title(languagePack.translate(player.getUniqueId(), "spectate-session-players-title"))
                            .lines(5)
                            .previousPageSlot(43).nextPageSlot(44)
                            .itemSlots(IntStream.rangeClosed(9, 35).boxed().collect(Collectors.toList()))
                            .scheme(new MenuScheme())
                            .build(player, items)
                            .open();
                    }
                }
            }, HelperExecutors.sync(), 5L, TimeUnit.SECONDS);
        }
    }
}
