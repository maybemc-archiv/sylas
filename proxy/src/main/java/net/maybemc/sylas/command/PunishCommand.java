package net.maybemc.sylas.command;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.sylas.chat.NameTranslator;
import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.repository.exception.TargetAlreadyBannedException;
import net.maybemc.sylas.repository.exception.TooEarlyModificationException;
import net.maybemc.sylas.repository.exception.TranslatableExceptionMessage;
import net.maybemc.sylas.schema.ban.BanReason;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class PunishCommand extends Command implements TabExecutor {
    private final LanguagePack languagePack;
    private final BanRepository repository;
    private final UserRepository userRepository;

    public PunishCommand(LanguagePack languagePack) {
        super("punish", "sylas.punish.command", "mute");
        this.languagePack = languagePack;
        this.repository = Meybee.getInstance().getProvider(BanRepository.class);
        this.userRepository = Meybee.getInstance().getProvider(UserRepository.class);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer sender = (ProxiedPlayer) commandSender;
        /*
        punish <Target> <Reason> [ignorePresentBan:ignoreTimeout] <Appendant[]>
         */

        if (args.length < 2) {
            sender.sendMessage(languagePack.translate(sender.getUniqueId(), "punish-command-usage", repository.listBanReasons().getNow(Collections.emptyList())
                .stream()
                .map(BanReason::getId)
                .collect(Collectors.joining(", "))));
            return;
        }

        // TODO: refactor
        boolean ignorePresentBan = false;
        boolean ignoreTimeout = false;
        String appendant = "/";
        if (args.length >= 3) {
            try {
                String arg = args[2];
                String[] split = arg.split(":");
                if (split.length == 2) {
                    ignorePresentBan = Boolean.parseBoolean(split[0]);
                    ignoreTimeout = Boolean.parseBoolean(split[1]);
                }
            } catch (NumberFormatException ignored) {
            }
            appendant = readAppendant(args, 3);
        } else {
            appendant = readAppendant(args, 2);
        }

        Optional<BanReason> optionalBanReason = repository.findBanReason(args[1]);
        if (!optionalBanReason.isPresent()) {
            sender.sendMessage(languagePack.translate(sender.getUniqueId(), "punish-command-reason-not-found"));
            return;
        }

        BanReason reason = optionalBanReason.get();
        if (!sender.hasPermission(reason.getPermission())) {
            sender.sendMessage(languagePack.translate(sender.getUniqueId(), "punish-command-reason-no-permission"));
            return;
        }

        if (args[0].length() == 36) {
            try {
                UUID target = UUID.fromString(args[0]);
                proceed(target, reason, ignorePresentBan, ignoreTimeout, appendant, sender);
            } catch (IllegalArgumentException ignored) {
                sender.sendMessage(languagePack.translate(sender.getUniqueId(), "punish-command-invalid-uuid"));
            }
            return;
        }

        boolean finalIgnorePresentBan = ignorePresentBan;
        boolean finalIgnoreTimeout = ignoreTimeout;
        String finalAppendant = appendant;
        Futures.addCallback(userRepository.findUsers(args[0]), new FutureCallback<List<User>>() {
            @Override
            public void onSuccess(List<User> users) {
                if (users == null || users.isEmpty()) {
                    sender.sendMessage(languagePack.translate(sender.getUniqueId(), "punish-command-target-not-found"));
                    return;
                }

                User target = users.get(0);
                proceed(target.getUniqueId(), reason, finalIgnorePresentBan, finalIgnoreTimeout, finalAppendant, sender);
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                sender.sendMessage(languagePack.translate(sender.getUniqueId(), "punish-command-target-not-found"));
                Meybee.getLogger().log(Level.WARNING, "Error searching player " + args[0], throwable);
            }
        });
    }

    private String readAppendant(String[] args, int startIndex) {
        String[] cutArgs = new String[args.length - startIndex];
        System.arraycopy(args, startIndex, cutArgs, 0, cutArgs.length);
        return String.join(" ", cutArgs);
    }

    private void proceed(UUID target, BanReason reason, boolean ignorePresentBan, boolean ignoreTimeout, String appendant, ProxiedPlayer sender) {
        String creator = sender.getUniqueId().toString();

        Futures.addCallback(repository.ban(creator, target, reason, appendant, ignorePresentBan, ignoreTimeout), new FutureCallback<String>() {
            @Override
            public void onSuccess(String banId) {
                if (banId == null) {
                    sender.sendMessage(languagePack.translate(sender.getUniqueId(), "punish-command-success-error"));
                } else {
                    NameTranslator.sendNameMessage(sender, languagePack, "punish-command-success", target, 1, banId, null, reason.getId());
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                throwable = throwable.getCause();
                if (throwable instanceof TooEarlyModificationException) {
                    ComponentBuilder builder = new ComponentBuilder().append(TextComponent.fromLegacyText(languagePack.translate(sender.getUniqueId(), "punish-command-too-early-header")))
                        .append(languagePack.translate(sender.getUniqueId(), "punish-command-too-early-yes"))
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] {new TextComponent(languagePack.translate(sender.getUniqueId(), "punish-command-too-early-hover"))}))
                        .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/punish " + target + " " + reason.getId() + " " + (ignorePresentBan + ":" + true) + " " + appendant));
                    sender.sendMessage(builder.create());
                } else if (throwable instanceof TargetAlreadyBannedException) {
                    ComponentBuilder builder = new ComponentBuilder().append(TextComponent.fromLegacyText(languagePack.translate(sender.getUniqueId(), "punish-command-already-banned-header")))
                        .append(languagePack.translate(sender.getUniqueId(), "punish-command-already-banned-yes"))
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] {new TextComponent(languagePack.translate(sender.getUniqueId(), "punish-command-already-banned-hover"))}))
                        .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/punish " + target + " " + reason.getId() + " " + (true + ":" + ignoreTimeout) + " " + appendant));
                    sender.sendMessage(builder.create());
                } else if (throwable instanceof TranslatableExceptionMessage) {
                    TranslatableExceptionMessage exception = (TranslatableExceptionMessage) throwable;
                    sender.sendMessage(exception.translate(languagePack, sender.getUniqueId()));
                } else {
                    sender.sendMessage(languagePack.translate(sender.getUniqueId(), "punish-command-error", throwable.getMessage()));
                    throwable.printStackTrace();
                }
            }
        });
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length == 1) {
            IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
            try {
                Collection<String> names = playerManager.onlinePlayers().asNamesAsync().get(1L, TimeUnit.SECONDS);
                return names.stream()
                    .sorted(String::compareTo)
                    .filter(name -> name.toLowerCase().startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                e.printStackTrace();
            }
        } else if (args.length == 2) {
            List<BanReason> reasons = repository.listBanReasons().getNow(Collections.emptyList());
            return reasons.stream()
                .sorted(Comparator.comparing(BanReason::getId))
                .filter(reason -> sender.hasPermission(reason.getPermission()))
                .map(BanReason::getId)
                .filter(id -> id.toLowerCase().startsWith(args[1].toLowerCase()))
                .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }
}
