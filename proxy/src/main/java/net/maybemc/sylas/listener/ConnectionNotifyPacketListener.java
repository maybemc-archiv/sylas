package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.notify.Notifier;
import net.maybemc.sylas.protocol.ConnectionNotifyPacket;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class ConnectionNotifyPacketListener implements PacketListener<ConnectionNotifyPacket> {
    private final Notifier notifier;

    public ConnectionNotifyPacketListener(Notifier notifier) {
        this.notifier = notifier;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, ConnectionNotifyPacket packet) {
        notifier.notifyConnection(packet.getPlayer(), packet.isJoin());
        return instantResult(stopHandling());
    }
}
