package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
public class KickPacket extends Packet {
    private UUID issuer;
    private UUID pTarget;
    private String reason;

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        writeUUID(byteBuf, issuer);
        writeUUID(byteBuf, pTarget);
        writeString(byteBuf, reason);
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
        issuer = readUUID(byteBuf);
        pTarget = readUUID(byteBuf);
        reason = readString(byteBuf);
    }
}
