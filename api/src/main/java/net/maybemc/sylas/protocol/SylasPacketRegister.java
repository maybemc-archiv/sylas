package net.maybemc.sylas.protocol;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.protocol.PacketRegistry;
import net.maybemc.sylas.repository.ban.BanRepository;

/**
 * @author Nico_ND1
 */
public class SylasPacketRegister {
    public static void register(PacketRegistry registry) {
        Meybee.getInstance().findProvider(BanRepository.class).ifPresent(banRepository -> {
            registry.register(500, BanCreatedPacket.class, () -> new BanCreatedPacket(banRepository));
            registry.register(501, ChatlogRequestPacket.class);
            registry.register(502, ChatlogResponsePacket.class);
            registry.register(503, KickPacket.class);
            registry.register(504, PardonCreatedPacket.class, () -> new PardonCreatedPacket(banRepository));
            registry.register(505, ReportCreatedPacket.class);
            registry.register(506, ReportJudgedPacket.class);
            registry.register(507, ReportSelectedPacket.class);
            registry.register(508, JumpToPacket.class);
            registry.register(509, TeamMembersRequestPacket.class);
            registry.register(510, TeamMembersResponsePacket.class);
            registry.register(511, PingRequestPacket.class);
            registry.register(512, KickInfoPacket.class);
            registry.register(513, BroadcastMessagePacket.class);
            registry.register(514, TeamChatMessagePacket.class);
            registry.register(515, ConnectionNotifyPacket.class);
            registry.register(516, OpenReportsPacket.class);
            registry.register(517, ChatClearPacket.class);
            registry.register(525, BanBypassDetectedPacket.class);
            registry.register(526, SendExploitNotifyPacket.class);
        });
        registry.register(522, SpectatePacket.class);
        registry.register(523, SpectateStopPacket.class);
        registry.register(524, RequestOnlinePlayersPacket.class);
    }
}
