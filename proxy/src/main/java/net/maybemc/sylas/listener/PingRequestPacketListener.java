package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.protocol.PingRequestPacket;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class PingRequestPacketListener implements PacketListener<PingRequestPacket> {
    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection connection, PingRequestPacket packet) {
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(packet.getPTarget());
        if (player != null && player.isConnected()) {
            return instantResult(stopHandling(player.getPing()));
        }
        return instantResult(stopHandling(-1));
    }
}
