package net.maybemc.sylas.command;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.protocol.atarget.PacketTarget;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.sylas.protocol.JumpToPacket;
import net.maybemc.sylas.protocol.SpectatePacket;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class SpectateCommand extends Command {
    private final LanguagePack languagePack;
    private final PacketClient packetClient;

    public SpectateCommand(LanguagePack languagePack, PacketClient packetClient) {
        super("spectate", "sylas.spectate.command", "spec");
        this.languagePack = languagePack;
        this.packetClient = packetClient;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        UserRepository userRepository = Meybee.getInstance().getProvider(UserRepository.class);

        if (args.length == 1) {
            Futures.addCallback(userRepository.findUsers(args[0]), new FutureCallback<>() {
                @Override
                public void onSuccess(List<User> users) {
                    if (users.isEmpty()) {
                        player.sendMessage(languagePack.translate(player.getUniqueId(), "spectate-command-target-not-found"));
                    } else {
                        execute(player, users.get(0).getUniqueId());
                    }
                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {
                    FutureCallback.super.onFailure(throwable);
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "spectate-command-error", throwable.getMessage()));
                }
            });
        } else if (args.length == 0) {
            execute(player, (UUID) null);
        } else {
            player.sendMessage(languagePack.translate(player.getUniqueId(), "spectate-command-usage"));
        }
    }

    private void execute(ProxiedPlayer player, UUID target) {
        CompletableFuture<SpectatePacket> future;
        if (target == null) {
            future = packetClient.sendPacket(new SpectatePacket(player.getUniqueId(), null).target(PacketTarget.playerServer(player.getUniqueId())), SpectatePacket.class);
        } else {
            future = packetClient.sendPacket(new SpectatePacket(player.getUniqueId(), target).target(PacketTarget.playerServer(target)), SpectatePacket.class);
        }
        Futures.addCallback(future, new FutureCallback<>() {
            @Override
            public void onSuccess(SpectatePacket response) {
                if (!player.getUniqueId().equals(response.getSpectator())) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "spectate-command-spectate-prepare-error"));
                } else {
                    if (target != null) {
                        packetClient.sendPacket(new JumpToPacket(player.getUniqueId(), target));
                    }
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "spectate-command-success"));
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                FutureCallback.super.onFailure(throwable);
                player.sendMessage(languagePack.translate(player.getUniqueId(), "spectate-command-error", throwable.getMessage()));
            }
        }, 5L, TimeUnit.SECONDS);
    }
}
