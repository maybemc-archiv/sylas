package net.maybemc.sylas.chat.filter;

import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * @author Nico_ND1
 */
public interface ChatFilter {
    ChatFilterResult testMessage(ProxiedPlayer player, String message);
}
