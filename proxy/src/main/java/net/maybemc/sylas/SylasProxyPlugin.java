package net.maybemc.sylas;

import lombok.SneakyThrows;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.i18n.LanguageRepository;
import net.maybemc.meybee.api.json.GsonProvider;
import net.maybemc.meybee.api.protocol.PacketRegistry;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.sylas.chat.filter.ChatFilterConfig;
import net.maybemc.sylas.chat.filter.ChatFilterExecutor;
import net.maybemc.sylas.chat.filter.ChatFilterPreparer;
import net.maybemc.sylas.chat.log.ChatlogCollector;
import net.maybemc.sylas.command.BanPointsCommand;
import net.maybemc.sylas.command.BroadcastCommand;
import net.maybemc.sylas.command.ChatClearCommand;
import net.maybemc.sylas.command.ChatlogCommand;
import net.maybemc.sylas.command.FindCommand;
import net.maybemc.sylas.command.GlistCommand;
import net.maybemc.sylas.command.SpectateCommand;
import net.maybemc.sylas.command.JumpToCommand;
import net.maybemc.sylas.command.KickCommand;
import net.maybemc.sylas.command.NotifyCommand;
import net.maybemc.sylas.command.PardonCommand;
import net.maybemc.sylas.command.PingCommand;
import net.maybemc.sylas.command.ProxyCommand;
import net.maybemc.sylas.command.PunishCommand;
import net.maybemc.sylas.command.PunishInfoCommand;
import net.maybemc.sylas.command.SendCommand;
import net.maybemc.sylas.command.SpectateCommand;
import net.maybemc.sylas.command.TeamChatCommand;
import net.maybemc.sylas.command.TeamMembersCommand;
import net.maybemc.sylas.listener.BanBypassDetectedPacketListener;
import net.maybemc.sylas.listener.BanCreatedPacketListener;
import net.maybemc.sylas.listener.BroadcastMessagePacketListener;
import net.maybemc.sylas.listener.ChatClearPacketListener;
import net.maybemc.sylas.listener.ChatListener;
import net.maybemc.sylas.listener.ChatlogRequestListener;
import net.maybemc.sylas.listener.ConnectionNotifyPacketListener;
import net.maybemc.sylas.listener.KickInfoPacketListener;
import net.maybemc.sylas.listener.KickPacketListener;
import net.maybemc.sylas.listener.LoginListener;
import net.maybemc.sylas.listener.OpenReportsPacketListener;
import net.maybemc.sylas.listener.PardonCreatedPacketListener;
import net.maybemc.sylas.listener.PingRequestPacketListener;
import net.maybemc.sylas.listener.ReportCreatedPacketListener;
import net.maybemc.sylas.listener.ReportJudgedPacketListener;
import net.maybemc.sylas.listener.ReportSelectedPacketListener;
import net.maybemc.sylas.listener.SendExploitNotifyPacketListener;
import net.maybemc.sylas.listener.ServerConnectBanListener;
import net.maybemc.sylas.listener.ServerSwitchListener;
import net.maybemc.sylas.listener.TeamChatMessagePacketListener;
import net.maybemc.sylas.listener.TeamMembersRequestPacketListener;
import net.maybemc.sylas.notify.Notifier;
import net.maybemc.sylas.pardon.AutoPardonApplier;
import net.maybemc.sylas.pardon.NameAutoPardonApplier;
import net.maybemc.sylas.protocol.BanBypassDetectedPacket;
import net.maybemc.sylas.protocol.BanCreatedPacket;
import net.maybemc.sylas.protocol.BroadcastMessagePacket;
import net.maybemc.sylas.protocol.ChatClearPacket;
import net.maybemc.sylas.protocol.ChatlogRequestPacket;
import net.maybemc.sylas.protocol.ConnectionNotifyPacket;
import net.maybemc.sylas.protocol.KickInfoPacket;
import net.maybemc.sylas.protocol.KickPacket;
import net.maybemc.sylas.protocol.OpenReportsPacket;
import net.maybemc.sylas.protocol.PardonCreatedPacket;
import net.maybemc.sylas.protocol.PingRequestPacket;
import net.maybemc.sylas.protocol.ReportCreatedPacket;
import net.maybemc.sylas.protocol.ReportJudgedPacket;
import net.maybemc.sylas.protocol.ReportSelectedPacket;
import net.maybemc.sylas.protocol.SendExploitNotifyPacket;
import net.maybemc.sylas.protocol.SylasPacketRegister;
import net.maybemc.sylas.protocol.TeamChatMessagePacket;
import net.maybemc.sylas.protocol.TeamMembersRequestPacket;
import net.maybemc.sylas.repository.RestClientProvider;
import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.repository.ban.RestBanRepository;
import net.maybemc.sylas.repository.chatlog.ChatlogRepository;
import net.maybemc.sylas.repository.chatlog.RestChatlogRepository;
import net.maybemc.sylas.repository.report.ReportRepository;
import net.maybemc.sylas.repository.report.RestReportRepository;
import net.maybemc.sylas.repository.user.RestUserRepository;
import net.maybemc.sylas.repository.user.UserRepository;
import net.md_5.bungee.api.plugin.Plugin;

import javax.ws.rs.client.Client;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nico_ND1
 */
// TODO: maintenance countdown
public class SylasProxyPlugin extends Plugin {
    @SneakyThrows
    @Override
    public void onEnable() {
        Meybee.getInstance().registerProviderLoader(Client.class, new RestClientProvider());
        Meybee.getInstance().registerProvider(BanRepository.class, new RestBanRepository());
        Meybee.getInstance().registerProvider(ReportRepository.class, new RestReportRepository());
        Meybee.getInstance().registerProvider(UserRepository.class, new RestUserRepository());
        Meybee.getInstance().registerProvider(ChatlogRepository.class, new RestChatlogRepository());

        LanguagePack languagePack = Meybee.getInstance().getProvider(LanguageRepository.class).loadPack("sylas");
        ChatlogCollector chatlogCollector = new ChatlogCollector();
        PacketClient packetClient = Meybee.getInstance().getProvider(PacketClient.class, PacketRegistry.DEFAULT_NAME);
        packetClient.addGroup("log-receiver");
        SylasPacketRegister.register(packetClient.getPacketRegistry());
        packetClient.getPacketRegistry().registerListener(ChatlogRequestPacket.class, new ChatlogRequestListener(chatlogCollector), 0);

        ChatFilterConfig chatFilterConfig = Meybee.getInstance().getProvider(GsonProvider.class).preLoadAndGetConfig(ChatFilterConfig.class);
        ChatFilterPreparer chatFilterPreparer = new ChatFilterPreparer(chatFilterConfig.getMessageReplacements());
        ChatFilterExecutor chatFilterExecutor = new ChatFilterExecutor(chatFilterConfig.getChatFilter(), chatFilterPreparer);

        Notifier notifier = new Notifier(languagePack);
        Meybee.getInstance().registerProvider(notifier);

        getProxy().getPluginManager().registerCommand(this, new PardonCommand(languagePack));
        getProxy().getPluginManager().registerCommand(this, new PunishCommand(languagePack));
        getProxy().getPluginManager().registerCommand(this, new ChatlogCommand(languagePack));
        getProxy().getPluginManager().registerCommand(this, new JumpToCommand(languagePack, packetClient));
        getProxy().getPluginManager().registerCommand(this, new TeamMembersCommand(languagePack, packetClient));
        getProxy().getPluginManager().registerCommand(this, new PingCommand(languagePack, packetClient));
        getProxy().getPluginManager().registerCommand(this, new ProxyCommand(languagePack));
        getProxy().getPluginManager().registerCommand(this, new KickCommand(languagePack, packetClient));
        getProxy().getPluginManager().registerCommand(this, new BroadcastCommand(languagePack, packetClient));
        getProxy().getPluginManager().registerCommand(this, new NotifyCommand(languagePack));
        getProxy().getPluginManager().registerCommand(this, new SendCommand(languagePack));
        getProxy().getPluginManager().registerCommand(this, new FindCommand(languagePack));
        getProxy().getPluginManager().registerCommand(this, new GlistCommand(languagePack));
        getProxy().getPluginManager().registerCommand(this, new TeamChatCommand(languagePack, packetClient, false));
        getProxy().getPluginManager().registerCommand(this, new TeamChatCommand(languagePack, packetClient, true));
        getProxy().getPluginManager().registerCommand(this, new ChatClearCommand(languagePack, packetClient));
        getProxy().getPluginManager().registerCommand(this, new SpectateCommand(languagePack, packetClient));
        getProxy().getPluginManager().registerCommand(this, new PunishInfoCommand(languagePack));

        // TODO: config
        List<AutoPardonApplier> autoPardonAppliers = new ArrayList<>();
        autoPardonAppliers.add(new NameAutoPardonApplier("NAME", "REQUEST_ACCEPTED"));

        LoginListener loginListener;
        getProxy().getPluginManager().registerListener(this, loginListener = new LoginListener(languagePack, packetClient, Meybee.getInstance().getProvider(ReportRepository.class), autoPardonAppliers));
        getProxy().getPluginManager().registerListener(this, new ChatListener(languagePack, chatFilterExecutor, chatlogCollector));
        getProxy().getPluginManager().registerListener(this, new ServerSwitchListener(chatlogCollector));

        packetClient.getPacketRegistry().registerListener(BanCreatedPacket.class, new BanCreatedPacketListener(loginListener, notifier), 0);
        packetClient.getPacketRegistry().registerListener(BanBypassDetectedPacket.class, new BanBypassDetectedPacketListener(notifier), 0);
        packetClient.getPacketRegistry().registerListener(PardonCreatedPacket.class, new PardonCreatedPacketListener(notifier), 0);
        packetClient.getPacketRegistry().registerListener(TeamMembersRequestPacket.class, new TeamMembersRequestPacketListener(), 0);
        packetClient.getPacketRegistry().registerListener(PingRequestPacket.class, new PingRequestPacketListener(), 0);
        packetClient.getPacketRegistry().registerListener(KickPacket.class, new KickPacketListener(packetClient, languagePack), 0);
        packetClient.getPacketRegistry().registerListener(KickInfoPacket.class, new KickInfoPacketListener(notifier), 0);
        packetClient.getPacketRegistry().registerListener(BroadcastMessagePacket.class, new BroadcastMessagePacketListener(languagePack), 0);
        packetClient.getPacketRegistry().registerListener(TeamChatMessagePacket.class, new TeamChatMessagePacketListener(notifier), 0);
        packetClient.getPacketRegistry().registerListener(ConnectionNotifyPacket.class, new ConnectionNotifyPacketListener(notifier), 0);
        packetClient.getPacketRegistry().registerListener(OpenReportsPacket.class, new OpenReportsPacketListener(notifier), 0);
        packetClient.getPacketRegistry().registerListener(ChatClearPacket.class, new ChatClearPacketListener(languagePack), 0);
        packetClient.getPacketRegistry().registerListener(ReportJudgedPacket.class, new ReportJudgedPacketListener(packetClient), 0);
        packetClient.getPacketRegistry().registerListener(ReportSelectedPacket.class, new ReportSelectedPacketListener(Meybee.getInstance().getProvider(ReportRepository.class), packetClient), 0);
        packetClient.getPacketRegistry().registerListener(ReportCreatedPacket.class, new ReportCreatedPacketListener(notifier), 0);
        packetClient.getPacketRegistry().registerListener(SendExploitNotifyPacket.class, new SendExploitNotifyPacketListener(notifier), 0);

        try {
            SylasProxyConfig config = Meybee.getInstance().getProvider(GsonProvider.class).preLoadAndGetConfig(SylasProxyConfig.class);
            for (SylasProxyConfig.ServerBanListener listener : config.getServerBanListeners()) {
                getProxy().getPluginManager().registerListener(this, new ServerConnectBanListener(languagePack, listener.getBanType(), listener.getServerNamePrefixes()));
            }
        } catch (IOException | InstantiationException | IllegalAccessException exception) {
            exception.printStackTrace();
        }
    }
}
