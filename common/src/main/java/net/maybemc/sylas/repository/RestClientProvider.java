package net.maybemc.sylas.repository;

import net.maybemc.meybee.api.provider.ProviderLoader;
import org.glassfish.jersey.client.ClientProperties;
import org.jetbrains.annotations.NotNull;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.util.Optional;

/**
 * @author Nico_ND1
 */
public class RestClientProvider implements ProviderLoader<Client> {
    @Override
    public @NotNull Optional<Client> load(Object... objects) {
        Client client = ClientBuilder.newClient()
            .register(new GsonRestHelper())
            .property(ClientProperties.READ_TIMEOUT, 5000L)
            .property(ClientProperties.CONNECT_TIMEOUT, 5000L);
        return Optional.of(client);
    }
}
