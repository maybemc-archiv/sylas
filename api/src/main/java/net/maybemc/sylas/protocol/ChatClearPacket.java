package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ChatClearPacket extends Packet {
    private UUID issuer;
    private String server;

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        writeUUID(byteBuf, issuer);
        writeString(byteBuf, server);
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
        issuer = readUUID(byteBuf);
        server = readString(byteBuf);
    }
}
