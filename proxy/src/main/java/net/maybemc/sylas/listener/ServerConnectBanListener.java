package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.sylas.schema.ban.Ban;
import net.maybemc.sylas.schema.ban.BanType;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

/**
 * @author Nico_ND1
 */
public class ServerConnectBanListener extends BanListener implements Listener {
    private final BanType banType;
    private final String[] serverNamePrefixes;

    public ServerConnectBanListener(LanguagePack languagePack, BanType banType, String... serverNamePrefixes) {
        super(languagePack);
        this.banType = banType;
        this.serverNamePrefixes = serverNamePrefixes;
    }

    @EventHandler
    public void onSwitch(ServerConnectEvent event) {
        if (!acceptServer(event.getTarget())) {
            return;
        }

        ProxiedPlayer player = event.getPlayer();
        try {
            Ban ban = repository.getActiveBan(player.getUniqueId(), banType).get(5L, TimeUnit.SECONDS);
            if (ban != null) {
                disallowConnect(player, event, ban);
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            Meybee.getLogger().log(Level.WARNING, "Error retrieving active ban of " + player.getUniqueId() + " for type " + banType, e);
        }
    }

    private boolean acceptServer(ServerInfo serverInfo) {
        boolean accept = false;
        for (String serverNamePrefix : serverNamePrefixes) {
            if (serverInfo.getName().startsWith(serverNamePrefix)) {
                accept = true;
                break;
            }
        }
        return accept;
    }

    private void disallowConnect(ProxiedPlayer player, ServerConnectEvent event, Ban ban) {
        if (!ban.isActive()) {
            Meybee.getLogger().warning("Ban is present for " + ban.getTarget() + " (" + ban.getId() + ") but its not active");
            return;
        }

        event.setCancelled(true);
        player.sendMessages(languagePack.translate(ban.getTarget(), "server-banned-" + banType.name(),
            ban.getId(),
            ban.getCreator(),
            languagePack.translate(ban.getTarget(), "ban-reason-" + ban.getReason().getId()),
            ban.getAppendant(),
            formatDuration(ban.getTarget(), ban)
        ).split("\n"));
    }
}
