package net.maybemc.sylas.command;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.user.User;
import net.maybemc.sylas.chat.NameTranslator;
import net.maybemc.sylas.repository.exception.UserNotFoundException;
import net.maybemc.sylas.repository.user.UserRepository;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
public class BanPointsCommand extends Command {
    private final LanguagePack languagePack;
    private final UserRepository userRepository;

    public BanPointsCommand(LanguagePack languagePack) {
        super("banpoints", "sylas.banpoints.command", "bp");
        this.languagePack = languagePack;
        this.userRepository = Meybee.getInstance().getProvider(UserRepository.class);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;

        if (args.length == 1 && player.hasPermission("sylas.banpoints.command.other")) {
            net.maybemc.meybee.api.user.UserRepository userRepository = Meybee.getInstance().getProvider(net.maybemc.meybee.api.user.UserRepository.class);
            Futures.addCallback(userRepository.findUsers(args[0]), new FutureCallback<List<User>>() {
                @Override
                public void onSuccess(List<User> users) {
                    if (users.isEmpty()) {
                        player.sendMessage(languagePack.translate(player.getUniqueId(), "banpoints-command-target-not-found"));
                    } else {
                        execute(player, users.get(0).getUniqueId());
                    }
                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {
                    FutureCallback.super.onFailure(throwable);
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "banpoints-command-error", throwable.getMessage()));
                }
            });
        } else {
            execute(player, player.getUniqueId());
        }
    }

    private void execute(ProxiedPlayer player, UUID target) {
        Futures.addCallback(userRepository.getBanPoints(target), new FutureCallback<Integer>() {
            @Override
            public void onSuccess(Integer points) {
                if (target.equals(player.getUniqueId())) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "banpoints-command-success-self", points));
                } else {
                    NameTranslator.sendNameMessage(player, languagePack, "banpoints-command-success-other", target, 1, points, null);
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                if (throwable instanceof UserNotFoundException || throwable.getCause() instanceof UserNotFoundException) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "banpoints-command-target-not-found"));
                } else {
                    FutureCallback.super.onFailure(throwable);
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "banpoints-command-error", throwable.getMessage()));
                }
            }
        });
    }
}
