package net.maybemc.sylas.spectate.listener;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.protocol.SpectateStopPacket;
import net.maybemc.sylas.spectate.SpectatorRepository;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class SpectateStopPacketListener implements PacketListener<SpectateStopPacket> {
    private final SpectatorRepository repository;

    public SpectateStopPacketListener(SpectatorRepository repository) {
        this.repository = repository;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, SpectateStopPacket packet) {
        Player player = Bukkit.getPlayer(packet.getSpectator());
        if (player != null) {
            repository.getSession(player).ifPresent(session -> {
                IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
                playerManager.getPlayerExecutor(packet.getSpectator()).connectToFallback();
            });
        }
        return instantResult(continueHandling());
    }
}
