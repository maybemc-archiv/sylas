package net.maybemc.sylas.command;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.sylas.protocol.ChatClearPacket;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;
import org.jetbrains.annotations.NotNull;

import java.util.logging.Level;

/**
 * @author Nico_ND1
 */
public class ChatClearCommand extends Command {
    private final LanguagePack languagePack;
    private final PacketClient packetClient;

    public ChatClearCommand(LanguagePack languagePack, PacketClient packetClient) {
        super("chatclear", "sylas.chatclear.command", "cc");
        this.languagePack = languagePack;
        this.packetClient = packetClient;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        Server server = player.getServer();
        if (server == null || server.getInfo() == null) {
            player.sendMessage(languagePack.translate(player.getUniqueId(), "chatclear-command-no-server"));
            return;
        }

        Futures.addCallback(packetClient.sendPacket(new ChatClearPacket(player.getUniqueId(), server.getInfo().getName()), Integer.class), new FutureCallback<Integer>() {
            @Override
            public void onSuccess(Integer result) {
                if (result == null) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "chatclear-command-error-unknown"));
                    return;
                }

                switch (result) {
                    case 0: // success
                        player.sendMessage(languagePack.translate(player.getUniqueId(), "chatclear-command-success"));
                        break;
                    case 1: // issuer cooldown
                        player.sendMessage(languagePack.translate(player.getUniqueId(), "chatclear-command-cooldown-issuer"));
                        break;
                    case 2: // server cooldown
                        player.sendMessage(languagePack.translate(player.getUniqueId(), "chatclear-command-cooldown-server"));
                        break;
                    default:
                        player.sendMessage(languagePack.translate(player.getUniqueId(), "chatclear-command-error-unknown"));
                        break;
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                Meybee.getLogger().log(Level.WARNING, "Error sending chatclear request from " + player.getUniqueId() + " for " + server.getInfo().getName(), throwable);
                player.sendMessage(languagePack.translate(player.getUniqueId(), "chatclear-command-error", throwable.getMessage()));
            }
        });
    }
}
