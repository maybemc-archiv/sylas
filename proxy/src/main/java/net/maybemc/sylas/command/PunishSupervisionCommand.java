package net.maybemc.sylas.command;

import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.schema.Pagination;
import net.maybemc.sylas.schema.ban.Ban;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
public class PunishSupervisionCommand extends Command implements TabExecutor {
    private final BanRepository banRepository;
    private final LanguagePack languagePack;

    public PunishSupervisionCommand(BanRepository banRepository, LanguagePack languagePack) {
        super("punishsupervision", "sylas.punish.supervision");
        this.banRepository = banRepository;
        this.languagePack = languagePack;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        /*
        punishsupervision list [Page]
        punishsupervision supervise <BanId> <BanReasonId>
         */

        ProxiedPlayer player = (ProxiedPlayer) sender;
        if ((args.length == 1 || args.length == 2) && args[0].equalsIgnoreCase("list")) {
            int page = 1;
            if (args.length == 2) {
                try {
                    page = Integer.parseInt(args[1]);
                } catch (NumberFormatException ignored) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "punish-supervision-not-a-number"));
                    return;
                }
            }

            executeList(player, page);
        } else if (args.length == 3 && args[0].equalsIgnoreCase("supervise")) {
            String banId = args[1];
            UUID supervisor = player.getUniqueId();
            String banReasonId = args[2];
        }
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        return null;
    }

    private void executeList(ProxiedPlayer player, int page) {
        Futures.addCallback(banRepository.getUnsupervisedBans(page), new FutureCallback<>() {
            @Override
            public void onSuccess(Pagination<Ban> pagination) {
                int maxPages = pagination.getMaxPages();
                if (page <= maxPages && page > 0) {
                    List<Ban> items = pagination.getItems();
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "punish-supervision-list-header", page, maxPages, items.size()));

                    for (Ban item : items) {
                        player.sendMessage(languagePack.translate(player.getUniqueId(),""));
                    }
                } else {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "punish-supervision-list-invalid-page", page, maxPages));
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                FutureCallback.super.onFailure(throwable);
                player.sendMessage(languagePack.translate(player.getUniqueId(), "error"));
            }
        });
    }
}
