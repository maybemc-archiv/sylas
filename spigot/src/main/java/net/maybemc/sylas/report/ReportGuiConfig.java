package net.maybemc.sylas.report;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.maybemc.meybee.api.json.Config;
import net.maybemc.meybee.api.json.DefaultConfig;

import java.util.Collections;
import java.util.List;

/**
 * @author Nico_ND1
 */
@Config(filePath = "*/report-gui")
@Getter
public class ReportGuiConfig implements DefaultConfig {
    private int lines;
    private List<Entry> entries;

    @Override
    public void setDefaultValues() {
        lines = 4;
        entries = Collections.emptyList();
    }

    @Data
    @NoArgsConstructor
    public static class Entry {
        private String reportReasonId;
        private int slot;
    }
}
