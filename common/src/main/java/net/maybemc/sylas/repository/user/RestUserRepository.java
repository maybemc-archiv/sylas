package net.maybemc.sylas.repository.user;

import net.maybemc.sylas.repository.RestRepository;
import net.maybemc.sylas.repository.exception.UnknownStatusException;
import net.maybemc.sylas.repository.exception.UserNotFoundException;
import net.maybemc.sylas.repository.exception.UserNotOnlineException;
import org.jetbrains.annotations.NotNull;

import javax.ws.rs.core.Response;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class RestUserRepository extends RestRepository implements UserRepository {
    @Override
    public void login(@NotNull UUID player, @NotNull String ip) {
        asyncExecutor().submit(() -> {
            requestJson(createTarget("user/{uuid}/login")
                .resolveTemplate("uuid", player))
                .header("ip", ip)
                .post(newEntity());
        });
    }

    @Override
    public CompletableFuture<Void> kick(@NotNull UUID player, @NotNull String reason) {
        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(requestJson(createTarget("user/{uuid}/kick")
                .resolveTemplate("uuid", player))
                .header("reason", reason)
                .post(newEntity()));

            switch (response.getStatus()) {
                case 200:
                    return null;
                case 404:
                    throw new UserNotOnlineException();
                default:
                    throw new UnknownStatusException(response);
            }
        }, asyncExecutor());
    }

    @Override
    public CompletableFuture<Integer> getBanPoints(@NotNull UUID player) {
        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(requestJson(createTarget("user/{uuid}/points")
                .resolveTemplate("uuid", player))
                .get());

            switch (response.getStatus()) {
                case 200:
                    return response.readEntity(Integer.class);
                case 404:
                    throw new UserNotFoundException();
                default:
                    throw new UnknownStatusException(response);
            }
        }, asyncExecutor());
    }
}
