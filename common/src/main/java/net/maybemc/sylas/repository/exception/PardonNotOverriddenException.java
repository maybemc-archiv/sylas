package net.maybemc.sylas.repository.exception;

/**
 * @author Nico_ND1
 */
public class PardonNotOverriddenException extends RuntimeException implements TranslatableExceptionMessage {
    @Override
    public String getLocaleKey() {
        return "exception-pardonnotoverridden";
    }
}
