package net.maybemc.sylas.schema.ban;

import lombok.Data;

/**
 * @author Nico_ND1
 */
@Data
public class Pardon {
    private final PardonReason reason;
    private final String issuer;
    private final String appendant;
    private final long time;
}
