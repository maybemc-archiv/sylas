package net.maybemc.sylas.schema.ban;

import lombok.Data;

/**
 * @author Nico_ND1
 */
@Data
public class PardonReason {
    private final String id;
    private final String permission;
    private final Type type;
    private final long value;
    private final boolean wasBanJustified;

    public enum Type {
        UNBAN,
        REDUCE_BY_PERCENT,
        REDUCE_BY_MILLIS,
        SET_TO_DURATION
    }
}
