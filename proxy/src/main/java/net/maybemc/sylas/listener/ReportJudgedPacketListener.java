package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.protocol.atarget.PacketTarget;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.protocol.ReportJudgedPacket;
import net.maybemc.sylas.protocol.SpectateStopPacket;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class ReportJudgedPacketListener implements PacketListener<ReportJudgedPacket> {
    private final PacketClient packetClient;

    public ReportJudgedPacketListener(PacketClient packetClient) {
        this.packetClient = packetClient;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, ReportJudgedPacket packet) {
        packetClient.sendPacket(new SpectateStopPacket(packet.getPlayer()).target(PacketTarget.playerServer(packet.getPlayer())));
        return instantResult(continueHandling());
    }
}
