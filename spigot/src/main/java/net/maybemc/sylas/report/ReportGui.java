package net.maybemc.sylas.report;

import me.lucko.helper.item.ItemStackBuilder;
import me.lucko.helper.menu.Gui;
import me.lucko.helper.menu.Item;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.spigot.config.GlobalDesignConfig;
import net.maybemc.sylas.repository.report.ReportRepository;
import net.maybemc.sylas.schema.report.ReportReason;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;

import java.util.Collections;

/**
 * @author Nico_ND1
 */
public class ReportGui extends Gui {
    private final String target;

    private final LanguagePack languagePack;
    private final ReportRepository repository;

    public ReportGui(Player player, String target, LanguagePack languagePack, ReportRepository repository) {
        super(player, Meybee.getInstance().getProvider(ReportGuiConfig.class).getLines(), languagePack.translate(player.getUniqueId(), "report-gui-title"));
        this.target = target;
        this.languagePack = languagePack;
        this.repository = repository;
    }

    @Override
    public void redraw() {
        ReportGuiConfig config = Meybee.getInstance().getProvider(ReportGuiConfig.class);
        if (isFirstDraw()) {
            GlobalDesignConfig designConfig = Meybee.getInstance().getProvider(GlobalDesignConfig.class);
            designConfig.drawGlass(this, getPlayer(), 0, config.getLines() - 1);
        }

        if (config.getEntries().isEmpty()) {
            for (ReportReason reportReason : repository.listReportReasons().getNow(Collections.emptyList())) {
                if (reportReason.getInventorySlot() != -1) {
                    setItem(reportReason.getInventorySlot(), createItemFor(reportReason));
                }
            }
        } else {
            for (ReportGuiConfig.Entry entry : config.getEntries()) {
                repository.findReportReason(entry.getReportReasonId()).ifPresent(reason -> {
                    setItem(entry.getSlot(), createItemFor(reason));
                });
            }
        }
    }

    private Item createItemFor(ReportReason reason) {
        return ItemStackBuilder.of(Material.valueOf(reason.getInventoryIcon()))
            .flag(ItemFlag.HIDE_ATTRIBUTES)
            .name(languagePack.translate(getPlayer().getUniqueId(), "report-reason-" + reason.getId() + "-name"))
            .lore(languagePack.translate(getPlayer().getUniqueId(), "report-reason-" + reason.getId() + "-lore").split("\n"))
            .buildConsumer(event -> {
                getPlayer().playSound(getPlayer(), Sound.UI_BUTTON_CLICK, 1F, 1F);
                getPlayer().performCommand("report " + target + " " + reason.getId() + " please-confirm");
            });
    }
}
