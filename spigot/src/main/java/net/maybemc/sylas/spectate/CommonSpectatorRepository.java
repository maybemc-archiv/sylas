package net.maybemc.sylas.spectate;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import me.lucko.helper.Schedulers;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.i18n.LanguageRepository;
import net.maybemc.meybee.api.protocol.PacketRegistry;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.sylas.SylasSpigotPlugin;
import net.maybemc.sylas.protocol.SpectatePacket;
import net.maybemc.sylas.protocol.SpectateStopPacket;
import net.maybemc.sylas.spectate.event.SpectateStartEvent;
import net.maybemc.sylas.spectate.event.SpectateStopEvent;
import net.maybemc.sylas.spectate.listener.SpectatePacketListener;
import net.maybemc.sylas.spectate.listener.SpectateStopPacketListener;
import net.maybemc.sylas.spectate.listener.SpectatorListener;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Nico_ND1
 */
public class CommonSpectatorRepository implements SpectatorRepository {
    private final LanguagePack languagePack = Meybee.getInstance().getProvider(LanguageRepository.class).loadPack("sylas");
    private final Map<UUID, SpectatorSession> sessions = new ConcurrentHashMap<>();

    public CommonSpectatorRepository() {
        PacketClient packetClient = Meybee.getInstance().getProvider(PacketClient.class, PacketRegistry.DEFAULT_NAME);
        packetClient.getPacketRegistry().registerListener(SpectatePacket.class, new SpectatePacketListener(this), 0);
        packetClient.getPacketRegistry().registerListener(SpectateStopPacket.class, new SpectateStopPacketListener(this), 0);

        Bukkit.getPluginManager().registerEvents(new SpectatorListener(this, packetClient, languagePack), JavaPlugin.getPlugin(SylasSpigotPlugin.class));
        Schedulers.sync().runRepeating(new SpectateSessionTask(languagePack, this), 1L, 1L);
    }

    @Override
    public SpectatorSession prepareSpectator(UUID spectator, @Nullable Player target) {
        SpectatorSession session;
        if (sessions.containsKey(spectator)) {
            session = sessions.get(spectator);
            session.target(target);
        } else {
            session = new SpectatorSession(languagePack, spectator);
            session.target(target);
            sessions.put(spectator, session);
        }

        Bukkit.getPluginManager().callEvent(new SpectateStartEvent(session));
        return session;
    }

    @Override
    public Optional<SpectatorSession> getSession(Player spectator) {
        return Optional.ofNullable(sessions.get(spectator.getUniqueId()));
    }

    @Override
    public void endSession(SpectatorSession session) {
        if (session.getTarget().isPresent()) {
            IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
            playerManager.getPlayerExecutor(session.getSpectator()).connectToFallback();
        } else {
            if (session.getBossBar() != null) {
                session.getBossBar().removeAll();
            }
            sessions.remove(session.getSpectator());

            Bukkit.getPluginManager().callEvent(new SpectateStopEvent(session));
        }
    }

    @Override
    public void invalidateSession(Player player) {
        SpectatorSession removedSession = sessions.remove(player.getUniqueId());
        if (removedSession != null) {
            Bukkit.getPluginManager().callEvent(new SpectateStopEvent(removedSession));
        }
    }

    @Override
    public Collection<SpectatorSession> getSessions() {
        return sessions.values();
    }
}
