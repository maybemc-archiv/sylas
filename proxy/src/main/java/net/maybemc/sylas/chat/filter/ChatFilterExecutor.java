package net.maybemc.sylas.chat.filter;

import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.List;

/**
 * @author Nico_ND1
 */
public class ChatFilterExecutor {
    private final List<ChatFilter> chatFilters;
    private final ChatFilterPreparer messagePreparer;

    public ChatFilterExecutor(List<ChatFilter> chatFilters, ChatFilterPreparer messagePreparer) {
        this.chatFilters = chatFilters;
        this.messagePreparer = messagePreparer;
    }

    public ChatFilterResult test(ProxiedPlayer player, String message) {
        String preparedMessage = messagePreparer.prepare(message);

        for (ChatFilter chatFilter : chatFilters) {
            ChatFilterResult result = chatFilter.testMessage(player, preparedMessage);
            if (result != null) {
                System.out.println("result of chatfilter " + chatFilter.getClass().getSimpleName() + ": " + result.isBlocked() + " : " + result.getDetectedWords().size() + " : " + result.getFlags().size() + " : " + result.getFlagType());
            }
            if (result == null || result.getFlags().isEmpty()) {
                continue;
            }
            return result;
        }
        return null;
    }
}
