package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;
import net.maybemc.sylas.schema.report.ReportJudgement;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
public class ReportJudgedPacket extends Packet {
    private String reportId;
    private UUID player;
    private ReportJudgement judgement;

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        writeString(byteBuf, reportId);
        writeUUID(byteBuf, player);
        byteBuf.writeInt(judgement.ordinal());
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
        reportId = readString(byteBuf);
        player = readUUID(byteBuf);
        judgement = ReportJudgement.values()[byteBuf.readInt()];
    }
}
