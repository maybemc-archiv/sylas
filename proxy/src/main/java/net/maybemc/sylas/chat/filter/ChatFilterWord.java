package net.maybemc.sylas.chat.filter;

import lombok.Data;

import java.util.regex.Pattern;

/**
 * @author Nico_ND1
 */
@Data
public class ChatFilterWord {
    // TODO: add name of chatfilter, which gets used as the issuer in a report or punishment
    private final String word;
    private final Pattern[] otherVariants; // word is in here
    private final String punishReasonId;
    private final String reportReasonId;
    private final boolean singleWord; // should the word be a single word? (if true f.e. "hia" wouldn't match with "lahia" but if false it would)
    private final boolean cancelSend;
}
