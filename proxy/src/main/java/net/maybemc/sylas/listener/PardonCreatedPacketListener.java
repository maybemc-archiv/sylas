package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.notify.Notifier;
import net.maybemc.sylas.protocol.PardonCreatedPacket;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class PardonCreatedPacketListener implements PacketListener<PardonCreatedPacket> {
    private final Notifier notifier;

    public PardonCreatedPacketListener(Notifier notifier) {
        this.notifier = notifier;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection connection, PardonCreatedPacket packet) {
        notifier.notifyPardonApplied(packet.getBan());
        return instantResult(stopHandling());
    }
}
