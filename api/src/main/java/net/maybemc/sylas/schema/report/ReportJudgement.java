package net.maybemc.sylas.schema.report;

/**
 * @author Nico_ND1
 */
public enum ReportJudgement {
    ACCEPT,
    DISMISS,
    DECLINE
}
