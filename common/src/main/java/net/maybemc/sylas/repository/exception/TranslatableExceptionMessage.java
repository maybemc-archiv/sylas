package net.maybemc.sylas.repository.exception;

import net.maybemc.meybee.api.i18n.LanguagePack;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
public interface TranslatableExceptionMessage {

    String getLocaleKey();

    default String translate(LanguagePack languagePack, UUID player) {
        return languagePack.translate(player, getLocaleKey());
    }

}
