package net.maybemc.sylas.spectate.listener;

import me.lucko.helper.Schedulers;
import me.lucko.helper.item.ItemStackBuilder;
import me.lucko.helper.menu.Gui;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.protocol.atarget.PacketTarget;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.meybee.api.user.message.ExecuteProxyCommandPacket;
import net.maybemc.sylas.SylasSpigotPlugin;
import net.maybemc.sylas.spectate.SpectatePlayersGui;
import net.maybemc.sylas.spectate.SpectateSessionGui;
import net.maybemc.sylas.spectate.SpectatorRepository;
import net.maybemc.sylas.spectate.SpectatorSession;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Nico_ND1
 */
public class SpectatorListener implements Listener {
    private final SpectatorRepository repository;
    private final PacketClient packetClient;
    private final LanguagePack languagePack;

    public SpectatorListener(SpectatorRepository repository, PacketClient packetClient, LanguagePack languagePack) {
        this.repository = repository;
        this.packetClient = packetClient;
        this.languagePack = languagePack;
    }

    @EventHandler
    public void onClick(PlayerInteractEvent event) {
        if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
            onClick(event.getPlayer(), SpectatorSession.Click.Type.LEFT);
        } else if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            onClick(event.getPlayer(), SpectatorSession.Click.Type.RIGHT);
        }
    }

    @EventHandler
    public void onDamageByPlayer(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player player) {
            onClick(player, SpectatorSession.Click.Type.LEFT);
        }
    }

    private void onClick(Player player, SpectatorSession.Click.Type type) {
        for (SpectatorSession session : repository.getSessions()) {
            if (session.getTarget().filter(player::equals).isPresent()) {
                session.onTargetClick(type);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onKnockbackTest(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player damaged && event.getDamager() instanceof Player damager) {
            repository.getSession(damager).ifPresent(session -> {
                if (session.canTestKnockback()) {
                    EntityDamageEvent lastDamageCause = damaged.getLastDamageCause();

                    event.setCancelled(false);
                    event.setDamage(0.0D);

                    Schedulers.sync().runLater(() -> {
                        damaged.setLastDamageCause(lastDamageCause);
                    }, 1L);
                    damaged.setLastDamageCause(lastDamageCause);
                } else {
                    event.setCancelled(true);
                }
            });
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player player) {
            repository.getSession(player).ifPresent(session -> {
                event.setCancelled(true);
            });
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        for (SpectatorSession session : repository.getSessions()) {
            if (event.getPlayer().equals(session.getSpectatorPlayer())) {
                continue;
            }

            event.getPlayer().hidePlayer(JavaPlugin.getPlugin(SylasSpigotPlugin.class), session.getSpectatorPlayer());
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (!(event.hasItem() && (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR))) {
            return;
        }

        Player player = event.getPlayer();
        repository.getSession(player).ifPresent(session -> {
            switch (player.getInventory().getHeldItemSlot()) {
                case 2 -> {
                    Gui gui = new SpectateSessionGui(player, Meybee.getInstance().getProvider(UserRepository.class).getUser(player.getUniqueId()), languagePack, session);
                    gui.open();
                }
                case 4 -> {
                    SpectatePlayersGui.openGui(player, languagePack);
                }
                case 6 -> {
                    repository.endSession(session);
                }
                case 8 -> {
                    if (player.getInventory().getItem(8) != null && player.getInventory().getItem(8).getType() == Material.PLAYER_HEAD) {
                        packetClient.sendPacket(new ExecuteProxyCommandPacket(player.getUniqueId(), "spectate " + session.getTargetName()).target(PacketTarget.playerProxy(player.getUniqueId())));
                    }
                }
            }
        });
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onQuit(PlayerQuitEvent event) {
        Player target = event.getPlayer();
        for (SpectatorSession session : repository.getSessions()) {
            if (session.getTarget().filter(player -> player.equals(target)).isPresent()) {
                Player spectatorPlayer = session.getSpectatorPlayer();
                spectatorPlayer.spigot().sendMessage(new ComponentBuilder().append(TextComponent.fromLegacyText(languagePack.translate(spectatorPlayer.getUniqueId(), "spectator-target-move-server", target.getName())))
                    .event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/spectate " + target.getName()))
                    .create());
                spectatorPlayer.getInventory().setItem(8, ItemStackBuilder.of(Material.PLAYER_HEAD)
                    .texture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOWI5MzRhYjk0YjU2YzM0OGFkYTQyZTIxMjZmNGY5NzQzMjkxYzJiNmVmNDQ5ZGM0ZTM4MzE1ZGM3YjFhYyJ9fX0=")
                    .name(languagePack.translate(spectatorPlayer.getUniqueId(), "spectator-target-move-server-item"))
                    .build());
            }
        }

        repository.invalidateSession(target);
    }
}
