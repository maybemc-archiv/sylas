package net.maybemc.sylas.schema.chatlog;

import lombok.Data;

/**
 * @author Nico_ND1
 */
@Data
public class ChatlogEntry {
    private final long time;
    private final Type type;
    private final String issuer;
    private final String value;
    private final ChatlogEntryFlag[] flags;

    public ChatlogEntry cleanUp() {
        return new ChatlogEntry(time, type, issuer, value, new ChatlogEntryFlag[0]);
    }

    public enum Type {
        MESSAGE,
        SERVER_SWITCH
    }
}
