package net.maybemc.sylas;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.module.ModulePostStartEvent;
import de.dytanic.cloudnet.driver.module.IModuleWrapper;
import de.dytanic.cloudnet.driver.module.ModuleLifeCycle;
import de.dytanic.cloudnet.driver.module.ModuleTask;
import de.dytanic.cloudnet.module.NodeCloudNetModule;

/**
 * @author Nico_ND1
 */
public class SylasCloudModule extends NodeCloudNetModule {
    @ModuleTask(event = ModuleLifeCycle.STARTED)
    public void onStart() {
        CloudNetDriver.getInstance().getEventManager().registerListener(this);

        IModuleWrapper module = CloudNetDriver.getInstance().getModuleProvider().getModule("meybee");
        if (module != null && module.getModuleLifeCycle() == ModuleLifeCycle.STARTED) {
            ModuleLoader.init();
        }
    }

    @EventListener
    public void onModuleLoad(ModulePostStartEvent event) {
        if (event.getModule().getModuleConfiguration().getName().equals("meybee")) {
            ModuleLoader.init();
        }
    }
}
