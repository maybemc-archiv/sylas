package net.maybemc.sylas.pardon;

import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.schema.ban.Ban;
import net.md_5.bungee.api.connection.PendingConnection;

/**
 * @author Nico_ND1
 */
public interface AutoPardonApplier {
    boolean applyPardon(BanRepository repository, Ban ban, PendingConnection connection);
}
