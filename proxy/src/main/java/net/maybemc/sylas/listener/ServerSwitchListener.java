package net.maybemc.sylas.listener;

import net.maybemc.sylas.chat.log.ChatlogCollector;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * @author Nico_ND1
 */
public class ServerSwitchListener implements Listener {
    private final ChatlogCollector chatlogCollector;

    public ServerSwitchListener(ChatlogCollector chatlogCollector) {
        this.chatlogCollector = chatlogCollector;
    }

    @EventHandler
    public void onServerSwitch(ServerConnectedEvent event) {
        chatlogCollector.switchServer(event.getPlayer().getUniqueId(), event.getServer().getInfo().getName());
    }
}
