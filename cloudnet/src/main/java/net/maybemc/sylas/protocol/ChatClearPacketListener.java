package net.maybemc.sylas.protocol;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServer;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class ChatClearPacketListener implements PacketListener<ChatClearPacket> {
    private final PacketServer packetServer;

    public ChatClearPacketListener(PacketServer packetServer) {
        this.packetServer = packetServer;
    }

    private final Cache<UUID, Long> playerCooldown = CacheBuilder.newBuilder()
        .expireAfterWrite(30L, TimeUnit.SECONDS)
        .build();
    private final Cache<String, Long> serverCooldown = CacheBuilder.newBuilder()
        .expireAfterWrite(15L, TimeUnit.SECONDS)
        .build();

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, ChatClearPacket packet) {
        if (playerCooldown.getIfPresent(packet.getIssuer()) != null) {
            return instantResult(stopHandling(1));
        } else if (serverCooldown.getIfPresent(packet.getServer()) != null) {
            return instantResult(stopHandling(2));
        } else {
            playerCooldown.put(packet.getIssuer(), System.currentTimeMillis());
            serverCooldown.put(packet.getServer(), System.currentTimeMillis());
            for (PacketServerConnection proxy : packetServer.findConnectionsByGroup("proxy")) {
                proxy.sendUnimportantPacket(packet);
            }
            return instantResult(stopHandling(0));
        }
    }
}
