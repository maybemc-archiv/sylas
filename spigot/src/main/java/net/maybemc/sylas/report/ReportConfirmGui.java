package net.maybemc.sylas.report;

import me.lucko.helper.item.ItemStackBuilder;
import me.lucko.helper.menu.Gui;
import me.lucko.helper.menu.Item;
import me.lucko.helper.scheduler.HelperExecutors;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.meybee.api.user.scope.nick.Nick;
import net.maybemc.meybee.spigot.config.GlobalDesignConfig;
import net.maybemc.sylas.repository.report.ReportRepository;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * @author Nico_ND1
 */
public class ReportConfirmGui extends Gui {
    public static void open(User user, User targetUser, String inputName, String reasonId, LanguagePack languagePack, ReportRepository repository) {
        Futures.addCallback(targetUser.getNickFor(user), new FutureCallback<Nick>() {
            @Override
            public void onSuccess(Nick targetNick) {
                Player player = Bukkit.getPlayer(user.getUniqueId());
                if (player != null && player.isOnline()) {
                    ReportConfirmGui gui = new ReportConfirmGui(player, targetNick, inputName, reasonId, languagePack, repository);
                    gui.open();
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                Meybee.getLogger().log(Level.WARNING, "Error opening report confirm gui for " + user.getUniqueId() + " (target " + targetUser.getUniqueId() + ")", throwable);
                Player player = Bukkit.getPlayer(user.getUniqueId());
                if (player != null && player.isOnline()) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "report-command-error", "/"));
                }
            }
        }, HelperExecutors.sync(), 3L, TimeUnit.SECONDS);
    }

    private final Nick targetNick;
    private final String inputName;
    private final String reasonId;

    private final LanguagePack languagePack;
    private final ReportRepository repository;

    public ReportConfirmGui(Player player, Nick targetNick, String inputName, String reasonId, LanguagePack languagePack, ReportRepository repository) {
        super(player, 3, languagePack.translate(player.getUniqueId(), "report-confirm-gui-title"));
        this.targetNick = targetNick;
        this.inputName = inputName;
        this.reasonId = reasonId;
        this.languagePack = languagePack;
        this.repository = repository;
    }

    @Override
    public void redraw() {
        User user = Meybee.getInstance().getProvider(UserRepository.class).getUser(getPlayer().getUniqueId());
        GlobalDesignConfig designConfig = Meybee.getInstance().getProvider(GlobalDesignConfig.class);
        if (isFirstDraw()) {
            Item item = Item.builder(designConfig.getGlassItemFor(user)).build();
            for (int i = 0; i < 9; i++) {
                setItem(i, item);
            }
            for (int i = 18; i < 27; i++) {
                setItem(i, item);
            }
        }

        setItem(11, ItemStackBuilder.of(Material.PLAYER_HEAD)
            .texture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjg0ZjU5NzEzMWJiZTI1ZGMwNThhZjg4OGNiMjk4MzFmNzk1OTliYzY3Yzk1YzgwMjkyNWNlNGFmYmEzMzJmYyJ9fX0=")
            .name(languagePack.translate(getPlayer().getUniqueId(), "report-confirm-back-name"))
            .buildConsumer(event -> {
                ReportGui reportGui = new ReportGui(getPlayer(), inputName, languagePack, repository);
                reportGui.open();
                getPlayer().playSound(getPlayer(), Sound.UI_BUTTON_CLICK, 1F, 1F);
            }));

        setItem(13, ItemStackBuilder.of(Material.PLAYER_HEAD)
            .texture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYTkyZTMxZmZiNTljOTBhYjA4ZmM5ZGMxZmUyNjgwMjAzNWEzYTQ3YzQyZmVlNjM0MjNiY2RiNDI2MmVjYjliNiJ9fX0=")
            .name(languagePack.translate(getPlayer().getUniqueId(), "report-confirm-ok-name"))
            .lore(languagePack.translate(getPlayer().getUniqueId(), "report-confirm-ok-lore").split("\n"))
            .buildConsumer(event -> {
                getPlayer().performCommand("report " + inputName + " " + reasonId);
                close();
            }));

        setItem(15, Item.builder(ItemStackBuilder.of(Material.PLAYER_HEAD)
            .texture(targetNick.getSkin().getValue())
            .name(languagePack.translate(getPlayer().getUniqueId(), "report-confirm-target-name", targetNick.getDisplayName()))
            .build()).build());
    }
}
