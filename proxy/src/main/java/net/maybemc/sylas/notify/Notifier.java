package net.maybemc.sylas.notify;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.meybee.api.user.scope.UserScope;
import net.maybemc.meybee.api.user.scope.UserScopeKey;
import net.maybemc.sylas.chat.NameTranslator;
import net.maybemc.sylas.protocol.BanBypassDetectedPacket;
import net.maybemc.sylas.protocol.KickInfoPacket;
import net.maybemc.sylas.protocol.TeamChatMessagePacket;
import net.maybemc.sylas.schema.ban.Ban;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class Notifier {
    public static final UserScopeKey<NotifyEntry> NOTIFY_SCOPE_KEY = UserScopeKey.createKey("sylas-notify", NotifyEntry.class, new NotifyPublisher());

    private final LanguagePack languagePack;

    public Notifier(LanguagePack languagePack) {
        this.languagePack = languagePack;
    }

    public CompletableFuture<List<ProxiedPlayer>> getEligiblePlayers(String type, NotifyType... allowedTypes) {
        CompletableFuture<List<ProxiedPlayer>> future = new CompletableFuture<>();
        List<ProxiedPlayer> possiblePlayers = ProxyServer.getInstance().getPlayers().stream()
            .filter(player -> player.hasPermission("sylas.notify." + type))
            .collect(Collectors.toList());
        if (possiblePlayers.isEmpty()) {
            future.complete(Collections.emptyList());
            return future;
        }

        UserRepository userRepository = Meybee.getInstance().getProvider(UserRepository.class);

        List<ProxiedPlayer> players = new ArrayList<>();
        AtomicInteger count = new AtomicInteger();
        for (ProxiedPlayer possiblePlayer : possiblePlayers) {
            User user = userRepository.getOfflineUser(possiblePlayer.getUniqueId());
            UserScope<NotifyEntry> notifyScope = user.getUserScope(NOTIFY_SCOPE_KEY);

            Futures.addCallback(notifyScope.loadValue(), new FutureCallback<NotifyEntry>() {
                @Override
                public void onSuccess(NotifyEntry notifyEntry) {
                    for (NotifyType allowedType : allowedTypes) {
                        if (notifyEntry.getValue(allowedType)) {
                            players.add(possiblePlayer);
                            break;
                        }
                    }
                    proceed();
                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {
                    FutureCallback.super.onFailure(throwable);
                    proceed();
                }

                private void proceed() {
                    if (count.incrementAndGet() == possiblePlayers.size()) {
                        future.complete(players);
                    }
                }
            }, 5L, TimeUnit.SECONDS);
        }

        return future;
    }

    public void notify(String type, Consumer<ProxiedPlayer> playerConsumer, NotifyType... notifyTypes) {
        Futures.addCallback(getEligiblePlayers(type, notifyTypes), new FutureCallback<List<ProxiedPlayer>>() {
            @Override
            public void onSuccess(List<ProxiedPlayer> proxiedPlayers) {
                proxiedPlayers.forEach(playerConsumer);
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                Meybee.getLogger().log(Level.WARNING, "Can't broadcast notify for type " + type, throwable);
            }
        });
    }

    public void notifyBanCreated(Ban ban) {
        notify("create-ban", player -> {
            if (ban.getCreator().length() == 36) {
                UUID creator = UUID.fromString(ban.getCreator());
                NameTranslator.sendNamesMessage(player, languagePack, "notify-create-ban", new UUID[] {creator, ban.getTarget()}, new int[] {0, 1}, null, null, ban.getReason().getId());
            } else {
                NameTranslator.sendNameMessage(player, languagePack, "notify-create-ban", ban.getTarget(), 1, ban.getCreator(), null, ban.getReason().getId());
            }
        }, NotifyType.PUNISH_LOG);
    }

    public void notifyPardonApplied(Ban ban) {
        notify("apply-pardon", player -> {
            if (ban.getActivePardon().getIssuer().length() == 36) {
                UUID issuer = UUID.fromString(ban.getActivePardon().getIssuer());
                NameTranslator.sendNamesMessage(player, languagePack, "notify-apply-pardon", new UUID[] {ban.getTarget(), issuer}, new int[] {0, 1}, null, null, ban.getActivePardon().getReason().getId());
            } else {
                NameTranslator.sendNameMessage(player, languagePack, "notify-apply-pardon", ban.getTarget(), 0, null, ban.getActivePardon().getIssuer(), ban.getActivePardon().getReason().getId());
            }
        }, NotifyType.PUNISH_LOG);
    }

    public void notifyKick(KickInfoPacket packet) {
        notify("kick", player -> {
            if (packet.getIssuer() == null) {
                NameTranslator.sendNameMessage(player, languagePack, "notify-kick", packet.getPTarget(), 1, "Konsole", null, packet.getReason());
            } else {
                NameTranslator.sendNamesMessage(player, languagePack, "notify-kick", new UUID[] {packet.getIssuer(), packet.getPTarget()}, new int[] {0, 1}, null, null, packet.getReason());
            }
        }, NotifyType.PUNISH_LOG);
    }

    public void notifyTeamChat(TeamChatMessagePacket packet) {
        notify("teamchat", player -> {
            if (packet.getServer() != null) {
                Server server = player.getServer();
                if (server != null && server.getInfo() != null && server.getInfo().getName().equals(packet.getServer())) {
                    NameTranslator.sendNameMessage(player, languagePack, "notify-teamchat-local", packet.getSender(), 0, null, packet.getMessage());
                }
            } else {
                NameTranslator.sendNameMessage(player, languagePack, "notify-teamchat", packet.getSender(), 0, null, packet.getMessage());
            }
        }, NotifyType.TEAM_CHAT);
    }

    public void notifyConnection(UUID player, boolean join) {
        notify("connection", targetPlayer -> {
            NameTranslator.sendNameMessage(targetPlayer, languagePack, "notify-" + (join ? "join" : "quit"), player, 0);
        }, NotifyType.TEAM_CONNECTIONS);
    }

    public void notifyOpenReports(int amount) {
        notify("open-reports", player -> {
            ComponentBuilder componentBuilder = new ComponentBuilder().append(TextComponent.fromLegacyText(languagePack.translate(player.getUniqueId(), "notify-open-reports", amount)));
            componentBuilder.append(languagePack.translate(player.getUniqueId(), "notify-open-reports-link"))
                .event(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://levi.maybemc.net/moderation/")); // TODO: config url
            player.sendMessage(componentBuilder.create());
        }, NotifyType.REPORT_INFO);
    }

    public void notifyReportCreated(String reportId) {
        notify("report-created", player -> {
            player.sendMessage(languagePack.translate(player.getUniqueId(), "notify-report-created"));
        }, NotifyType.REPORT_INFO);
    }

    public void notifyBanBypass(UUID testedPlayer, List<BanBypassDetectedPacket.Account> accounts) {
        notify("ban-bypass", player -> {
            NameTranslator.sendNameMessage(player, languagePack, "notify-ban-bypass-header", testedPlayer, 0);
            for (BanBypassDetectedPacket.Account account : accounts) {
                NameTranslator.sendNameMessage(player, languagePack, "notify-ban-bypass-entry", account.getUuid(), 0, null, account.getIp(), account.getBanId(), account.getBanType());
            }
        }, NotifyType.BAN_BYPASS);
    }

    public void notifyExploit(UUID exploiter, String checkName, int violationPoints, String packetsText) {
        notify("possible-exploit", player -> {
            NameTranslator.sendNameMessage(player, languagePack, "notify-possible-exploit", exploiter, 0, null, checkName, violationPoints, packetsText);
        }, NotifyType.REPORT_INFO);
    }
}
