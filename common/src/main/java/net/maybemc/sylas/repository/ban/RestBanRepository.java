package net.maybemc.sylas.repository.ban;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import de.dytanic.cloudnet.common.collection.Pair;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.sylas.repository.RestRepository;
import net.maybemc.sylas.repository.exception.BanNotFoundException;
import net.maybemc.sylas.repository.exception.PardonNotOverriddenException;
import net.maybemc.sylas.repository.exception.TargetAlreadyBannedException;
import net.maybemc.sylas.repository.exception.TooEarlyModificationException;
import net.maybemc.sylas.repository.exception.UnknownStatusException;
import net.maybemc.sylas.repository.exception.UserNotFoundException;
import net.maybemc.sylas.schema.Pagination;
import net.maybemc.sylas.schema.ban.Ban;
import net.maybemc.sylas.schema.ban.BanReason;
import net.maybemc.sylas.schema.ban.BanType;
import net.maybemc.sylas.schema.ban.PardonReason;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class RestBanRepository extends RestRepository implements BanRepository {
    private CompletableFuture<List<BanReason>> banReasons;
    private CompletableFuture<List<PardonReason>> pardonReasons;

    private final Cache<Pair<UUID, BanType>, Ban> banCache;

    public RestBanRepository() {
        this.banCache = CacheBuilder.newBuilder()
            .expireAfterAccess(1L, TimeUnit.MINUTES)
            .expireAfterWrite(2L, TimeUnit.MINUTES)
            .build();

        Futures.scheduledExecutor().scheduleAtFixedRate(() -> {
            listBanReasons();
            listPardonReasons();
        }, 0L, 10L, TimeUnit.MINUTES);
    }

    @Override
    public CompletableFuture<List<BanReason>> listBanReasons() {
        if (banReasons == null) {
            banReasons = CompletableFuture.supplyAsync(() -> {
                Response response = testResponse(createJsonTarget("ban/reasons").get());
                return response.readEntity(new GenericType<List<BanReason>>() {
                });
            }, asyncExecutor());
        }
        return banReasons;
    }

    @Override
    public Optional<BanReason> findBanReason(String id) {
        return listBanReasons().getNow(Collections.emptyList()).stream()
            .filter(reason -> reason.getId().equalsIgnoreCase(id))
            .findFirst();
    }

    @Override
    public CompletableFuture<List<PardonReason>> listPardonReasons() {
        if (pardonReasons == null) {
            pardonReasons = CompletableFuture.supplyAsync(() -> {
                Response response = testResponse(createJsonTarget("ban/pardon/reasons").get());
                return response.readEntity(new GenericType<List<PardonReason>>() {
                });
            }, asyncExecutor());
        }
        return pardonReasons;
    }

    @Override
    public Optional<PardonReason> findPardonReason(String id) {
        return listPardonReasons().getNow(Collections.emptyList()).stream()
            .filter(reason -> reason.getId().equalsIgnoreCase(id))
            .findFirst();
    }

    @Override
    public CompletableFuture<Ban> findBan(String id) {
        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(requestJson(createTarget("ban/{id}")
                .resolveTemplate("id", id))
                .get());

            switch (response.getStatus()) {
                case 404:
                    throw new BanNotFoundException();
                case 200:
                    return response.readEntity(Ban.class);
                default:
                    throw new UnknownStatusException(response);
            }
        }, asyncExecutor());
    }

    @Override
    public CompletableFuture<String> ban(@NotNull String creator, @NotNull UUID target, @NotNull BanReason reason, @NotNull String appendant, @Nullable Boolean ignorePresentBan, @Nullable Boolean ignoreTimeout) {
        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(createJsonTarget("ban")
                .header("creator", creator)
                .header("target", target)
                .header("reasonId", reason.getId())
                .header("appendant", appendant)
                .header("ignorePresentBan", ignorePresentBan)
                .header("ignoreTimeout", ignoreTimeout)
                .post(newEntity()), 408);

            switch (response.getStatus()) {
                case 408:
                    throw new TooEarlyModificationException();
                case 409:
                    throw new TargetAlreadyBannedException();
                case 200:
                    return response.readEntity(String.class);
                default:
                    throw new UnknownStatusException(response);
            }
        }, asyncExecutor());
    }

    @Override
    public CompletableFuture<Void> pardon(@NotNull String issuer, @NotNull String id, @NotNull PardonReason reason, @NotNull String appendant, @Nullable Boolean canOverride, @Nullable Boolean ignoreTimeout) {
        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(createJsonTarget("ban/pardon")
                .header("issuer", issuer)
                .header("id", id)
                .header("reasonId", reason.getId())
                .header("appendant", appendant)
                .header("canOverride", canOverride)
                .header("ignoreTimeout", ignoreTimeout)
                .post(newEntity()), 408);

            switch (response.getStatus()) {
                case 403:
                    throw new PardonNotOverriddenException();
                case 404:
                    throw new BanNotFoundException();
                case 408:
                    throw new TooEarlyModificationException();
                case 200:
                    return null;
                default:
                    throw new UnknownStatusException(response);
            }
        }, asyncExecutor());
    }

    @Override
    public CompletableFuture<Ban> getActiveBan(@NotNull UUID player, @NotNull BanType banType) {
        Ban cachedBan = banCache.getIfPresent(new Pair<>(player, banType));
        if (cachedBan != null) {
            return CompletableFuture.completedFuture(cachedBan);
        }

        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(requestJson(createTarget("user/{uuid}/activeBan/{banType}")
                .resolveTemplate("uuid", player)
                .resolveTemplate("banType", banType.name()))
                .get());

            switch (response.getStatus()) {
                case 204:
                    return null;
                case 404:
                    throw new UserNotFoundException();
                case 200:
                    Ban ban = response.readEntity(Ban.class);
                    banCache.put(new Pair<>(player, banType), ban);
                    return ban;
                default:
                    throw new UnknownStatusException(response);
            }
        }, asyncExecutor());
    }

    public void invalidateBanCache(@NotNull UUID player, @NotNull BanType banType) {
        banCache.invalidate(new Pair<>(player, banType));
    }

    @Override
    public CompletableFuture<List<Ban>> getActiveBans(@NotNull UUID player) {
        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(requestJson(createTarget("user/{uuid}/activeBans")
                .resolveTemplate("uuid", player))
                .get());

            switch (response.getStatus()) {
                case 200:
                    return response.readEntity(new GenericType<List<Ban>>() {
                    });
                default:
                    throw new UnknownStatusException(response);
            }
        }, asyncExecutor());
    }

    @Override
    public CompletableFuture<Pagination<Ban>> getUnsupervisedBans(int page) {
        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(requestJson(createTarget("supervision/bans/{page}")
                .resolveTemplate("page", page))
                .get());

            switch (response.getStatus()) {
                case 200:
                    return response.readEntity(new GenericType<Pagination<Ban>>() {
                    });
                default:
                    throw new UnknownStatusException(response);
            }
        }, asyncExecutor());
    }

    @Override
    public CompletableFuture<Object> supervise(@NotNull String banId, @NotNull UUID supervisor, @NotNull BanReason reason) {
        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(createJsonTarget("supervision/bans")
                .header("banId", banId)
                .header("supervisor", supervisor)
                .header("banReasonId", reason.getId())
                .post(newEntity()));

            switch (response.getStatus()) {
                case 404:
                    throw new BanNotFoundException();
                case 200:
                    return response.getEntity();
                default:
                    throw new UnknownStatusException(response);
            }
        }, asyncExecutor());
    }
}
