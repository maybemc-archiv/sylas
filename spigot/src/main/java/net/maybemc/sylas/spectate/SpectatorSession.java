package net.maybemc.sylas.spectate;

import me.lucko.helper.item.ItemStackBuilder;
import net.maybemc.meybee.api.i18n.LanguagePack;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.boss.BossBar;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class SpectatorSession {
    private final LanguagePack languagePack;

    private final UUID spectator;
    private Player spectatorPlayer;
    private BossBar bossBar;

    private Player target;
    private String targetName; // TODO: use for proxy follow command when interacting with item
    private final List<Click> targetClicks;
    private boolean showTargetClicks;

    private boolean canTestKnockback;

    public SpectatorSession(LanguagePack languagePack, UUID spectator) {
        this.languagePack = languagePack;
        this.spectator = spectator;
        this.targetClicks = new ArrayList<>(60);
    }

    public void giveItems() {
        Player player = getSpectatorPlayer();
        player.getInventory().setItem(2, ItemStackBuilder.of(Material.COMPARATOR)
            .name(languagePack.translate(player.getUniqueId(), "spectate-session-item-tools"))
            .build());
        player.getInventory().setItem(4, ItemStackBuilder.of(Material.COMPASS)
            .name(languagePack.translate(player.getUniqueId(), "spectate-session-item-players"))
            .build());
        player.getInventory().setItem(6, ItemStackBuilder.of(Material.PLAYER_HEAD)
            .texture("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYmViNTg4YjIxYTZmOThhZDFmZjRlMDg1YzU1MmRjYjA1MGVmYzljYWI0MjdmNDYwNDhmMThmYzgwMzQ3NWY3In19fQ==")
            .name(languagePack.translate(player.getUniqueId(), "spectate-session-item-leave"))
            .build());
    }

    public int getTargetPing() {
        return getTarget().map(player -> {
            CraftPlayer craftPlayer = (CraftPlayer) player;
            return craftPlayer.getPing();
        }).orElse(-1);
    }

    public Optional<Player> getTarget() {
        if (target != null && !target.isOnline()) {
            target = null;
        }
        return Optional.ofNullable(target);
    }

    public void target(Player target) {
        this.target = target;
        if (target != null) {
            this.targetName = target.getName();
        }
    }

    public String getTargetName() {
        return targetName;
    }

    public void onTargetClick(Click.Type type) {
        targetClicks.add(new Click(System.currentTimeMillis(), type));
    }

    public void timeoutTargetClicks() {
        targetClicks.removeIf(click -> click.time() + Click.LIFETIME < System.currentTimeMillis());
    }

    public int getClicksPerSecond(Click.Type type) {
        timeoutTargetClicks();
        return (int) targetClicks.stream()
            .filter(click -> click.type() == type)
            .count();
    }

    public boolean showTargetClicks() {
        return showTargetClicks;
    }

    public void setShowTargetClicks(boolean showTargetClicks) {
        this.showTargetClicks = showTargetClicks;
    }

    public boolean canTestKnockback() {
        return canTestKnockback;
    }

    public void setCanTestKnockback(boolean canTestKnockback) {
        this.canTestKnockback = canTestKnockback;
    }

    public UUID getSpectator() {
        return spectator;
    }

    public void setSpectatorPlayer(Player spectatorPlayer) {
        this.spectatorPlayer = spectatorPlayer;
    }

    public Player getSpectatorPlayer() {
        if (spectatorPlayer == null || (!spectatorPlayer.isOnline() && spectatorPlayer.getTicksLived() > 20)) {
            return spectatorPlayer = Bukkit.getPlayer(spectator);
        }
        return spectatorPlayer;
    }

    public BossBar getBossBar() {
        return bossBar;
    }

    public void setBossBar(BossBar bossBar) {
        this.bossBar = bossBar;
    }

    public record Click(long time, Type type) {
        private static final long LIFETIME = TimeUnit.SECONDS.toMillis(1);

        public enum Type {
            LEFT,
            RIGHT
        }
    }
}
