package net.maybemc.sylas.protocol;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class RequestOnlinePlayersPacketListener implements PacketListener<RequestOnlinePlayersPacket> {
    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, RequestOnlinePlayersPacket packet) {
        CompletableFuture<HandleResult> future = new CompletableFuture<>();
        IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
        playerManager.onlinePlayers().asUUIDsAsync().onComplete(uuids -> {
            future.complete(continueHandling(new RequestOnlinePlayersPacket(new ArrayList<>(uuids))));
        }).onFailure(future::completeExceptionally);
        return future;
    }
}
