package net.maybemc.sylas;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.maybemc.meybee.api.json.Config;
import net.maybemc.meybee.api.json.DefaultConfig;
import net.maybemc.sylas.schema.ban.BanType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nico_ND1
 */
@Config(filePath = "*/sylas")
public class SylasProxyConfig implements DefaultConfig {
    private List<ServerBanListener> serverBanListeners;

    @Override
    public void setDefaultValues() {
        serverBanListeners = new ArrayList<>();
        serverBanListeners.add(new ServerBanListener(BanType.GAME_TTT, new String[] {"TTT"}));
    }

    public List<ServerBanListener> getServerBanListeners() {
        return serverBanListeners;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ServerBanListener {
        private BanType banType;
        private String[] serverNamePrefixes;
    }
}
