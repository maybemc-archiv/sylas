package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
public class ReportCreatedPacket extends Packet {
    private String reportId;

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        writeString(byteBuf, reportId);
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
        reportId = readString(byteBuf);
    }
}
