package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.notify.Notifier;
import net.maybemc.sylas.protocol.TeamChatMessagePacket;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class TeamChatMessagePacketListener implements PacketListener<TeamChatMessagePacket> {
    private final Notifier notifier;

    public TeamChatMessagePacketListener(Notifier notifier) {
        this.notifier = notifier;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, TeamChatMessagePacket packet) {
        notifier.notifyTeamChat(packet);
        return instantResult(stopHandling());
    }
}
