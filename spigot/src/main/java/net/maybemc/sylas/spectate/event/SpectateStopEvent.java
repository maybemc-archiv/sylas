package net.maybemc.sylas.spectate.event;

import net.maybemc.sylas.spectate.SpectatorSession;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
public class SpectateStopEvent extends Event {
    private static final HandlerList HANDLER_LIST = new HandlerList();

    private final SpectatorSession session;

    public SpectateStopEvent(SpectatorSession session) {
        this.session = session;
    }

    public SpectatorSession getSession() {
        return session;
    }

    public UUID getUniqueId() {
        return getSession().getSpectator();
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }
}
