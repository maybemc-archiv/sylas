package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.notify.Notifier;
import net.maybemc.sylas.protocol.OpenReportsPacket;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class OpenReportsPacketListener implements PacketListener<OpenReportsPacket> {
    private final Notifier notifier;

    public OpenReportsPacketListener(Notifier notifier) {
        this.notifier = notifier;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, OpenReportsPacket packet) {
        notifier.notifyOpenReports(packet.getAmount());
        return instantResult(stopHandling());
    }
}
