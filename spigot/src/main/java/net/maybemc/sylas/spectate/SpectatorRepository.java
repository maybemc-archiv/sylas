package net.maybemc.sylas.spectate;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
public interface SpectatorRepository {
    SpectatorSession prepareSpectator(UUID spectator, @Nullable Player target);

    Optional<SpectatorSession> getSession(Player spectator);

    void endSession(SpectatorSession session);

    default boolean shouldJoinAsSpectator(Player player) {
        return getSession(player).isPresent();
    }

    void invalidateSession(Player player);

    Collection<SpectatorSession> getSessions();
}
