package net.maybemc.sylas.repository.ban;

import net.maybemc.sylas.schema.Pagination;
import net.maybemc.sylas.schema.ban.Ban;
import net.maybemc.sylas.schema.ban.BanReason;
import net.maybemc.sylas.schema.ban.BanType;
import net.maybemc.sylas.schema.ban.PardonReason;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public interface BanRepository {

    CompletableFuture<List<BanReason>> listBanReasons();

    Optional<BanReason> findBanReason(String id);

    CompletableFuture<List<PardonReason>> listPardonReasons();

    Optional<PardonReason> findPardonReason(String id);

    CompletableFuture<Ban> findBan(String id);

    CompletableFuture<String> ban(
        @NotNull String creator,
        @NotNull UUID target,
        @NotNull BanReason reason,
        @NotNull String appendant,
        @Nullable Boolean ignorePresentBan,
        @Nullable Boolean ignoreTimeout
    );

    CompletableFuture<Void> pardon(
        @NotNull String issuer,
        @NotNull String id,
        @NotNull PardonReason reason,
        @NotNull String appendant,
        @Nullable Boolean canOverride,
        @Nullable Boolean ignoreTimeout
    );

    CompletableFuture<Ban> getActiveBan(
        @NotNull UUID player,
        @NotNull BanType banType
    );

    CompletableFuture<List<Ban>> getActiveBans(
        @NotNull UUID player
    );

    CompletableFuture<Pagination<Ban>> getUnsupervisedBans(
        int page
    );

    CompletableFuture<Object> supervise(
        @NotNull String banId,
        @NotNull UUID supervisor,
        @NotNull BanReason reason
    );

}
