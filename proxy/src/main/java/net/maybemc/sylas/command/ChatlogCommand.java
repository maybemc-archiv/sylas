package net.maybemc.sylas.command;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.sylas.repository.chatlog.ChatlogRepository;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.logging.Level;

/**
 * @author Nico_ND1
 */
public class ChatlogCommand extends Command {
    private final LanguagePack languagePack;
    private final ChatlogRepository repository;
    private final UserRepository userRepository;

    public ChatlogCommand(LanguagePack languagePack) {
        super("chatlog", "sylas.log.command");
        this.languagePack = languagePack;
        this.repository = Meybee.getInstance().getProvider(ChatlogRepository.class);
        this.userRepository = Meybee.getInstance().getProvider(UserRepository.class);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;

        if (args.length == 1) {
            String name = args[0];

            Futures.addCallback(userRepository.findUsers(name), new FutureCallback<List<User>>() {
                @Override
                public void onSuccess(List<User> users) {
                    if (users.isEmpty()) {
                        player.sendMessage(languagePack.translate(player.getUniqueId(), "log-command-target-not-found"));
                        return;
                    }

                    Futures.addCallback(repository.createChatlog(users.get(0).getUniqueId().toString()), new FutureCallback<String>() {
                        @Override
                        public void onSuccess(String chatlogId) {
                            player.sendMessage(languagePack.translate(player.getUniqueId(), "log-command-success", chatlogId));
                        }

                        @Override
                        public void onFailure(@NotNull Throwable throwable) {
                            player.sendMessage(languagePack.translate(player.getUniqueId(), "log-command-error", throwable.getMessage()));
                            Meybee.getLogger().log(Level.WARNING, "Couldn't create log for " + name, throwable);
                        }
                    });
                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "log-command-target-not-found"));
                    Meybee.getLogger().log(Level.WARNING, "Error searching player " + name, throwable);
                }
            });
        } else {
            player.sendMessage(languagePack.translate(player.getUniqueId(), "log-command-usage"));
        }
    }
}
