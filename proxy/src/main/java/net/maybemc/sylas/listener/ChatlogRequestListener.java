package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.chat.log.ChatlogCollector;
import net.maybemc.sylas.protocol.ChatlogRequestPacket;
import net.maybemc.sylas.protocol.ChatlogResponsePacket;
import net.maybemc.sylas.schema.chatlog.ChatlogEntry;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class ChatlogRequestListener implements PacketListener<ChatlogRequestPacket> {
    private final ChatlogCollector chatlogCollector;

    public ChatlogRequestListener(ChatlogCollector chatlogCollector) {
        this.chatlogCollector = chatlogCollector;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection connection, ChatlogRequestPacket packet) {
        Meybee.getLogger().info("Received ChatlogRequestPacket for " + packet.getPTarget());
        if (packet.getPTarget().length() == 36) {
            UUID target = UUID.fromString(packet.getPTarget());
            List<ChatlogEntry> entries = chatlogCollector.getEntries(target);
            Meybee.getLogger().info("Found target: " + target + " = " + entries);
            if (entries == null) {
                Meybee.getLogger().info("Returning empty");
                return instantResult(stopHandling(new ChatlogResponsePacket(new ChatlogEntry[0])));
            }
            Meybee.getLogger().info("Returning array " + entries.size());
            return instantResult(stopHandling(new ChatlogResponsePacket(entries.toArray(new ChatlogEntry[0]))));
        } else {
            // TODO: implement server chatlogs
            Meybee.getLogger().info("Returning empty 2");
            return instantResult(stopHandling(new ChatlogResponsePacket(new ChatlogEntry[0])));
        }
    }
}
