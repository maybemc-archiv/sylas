package net.maybemc.sylas.schema.ban;

import lombok.Data;

/**
 * @author Nico_ND1
 */
@Data
public class BanReason {
    private final String id;
    private final String permission;
    private final BanType type;
    private final int priority;
}
