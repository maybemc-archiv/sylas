package net.maybemc.sylas;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.i18n.LanguageRepository;
import net.maybemc.meybee.api.json.GsonProvider;
import net.maybemc.meybee.api.protocol.PacketRegistry;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.sylas.exploitfixer.ExploitFixerListener;
import net.maybemc.sylas.exploitfixer.ExploitFixerSimulateCommand;
import net.maybemc.sylas.protocol.SylasPacketRegister;
import net.maybemc.sylas.report.ReportCommand;
import net.maybemc.sylas.report.ReportGuiConfig;
import net.maybemc.sylas.repository.RestClientProvider;
import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.repository.ban.RestBanRepository;
import net.maybemc.sylas.repository.report.ReportRepository;
import net.maybemc.sylas.repository.report.RestReportRepository;
import net.maybemc.sylas.repository.user.RestUserRepository;
import net.maybemc.sylas.repository.user.UserRepository;
import net.maybemc.sylas.spectate.CommonSpectatorRepository;
import net.maybemc.sylas.spectate.SpectatorRepository;
import org.bukkit.plugin.java.JavaPlugin;

import javax.ws.rs.client.Client;
import java.io.IOException;

/**
 * @author Nico_ND1
 */
public class SylasSpigotPlugin extends JavaPlugin {
    @Override
    public void onEnable() {
        Meybee.getInstance().registerProviderLoader(Client.class, new RestClientProvider());
        Meybee.getInstance().registerProvider(BanRepository.class, new RestBanRepository());
        Meybee.getInstance().registerProvider(ReportRepository.class, new RestReportRepository());
        Meybee.getInstance().registerProvider(UserRepository.class, new RestUserRepository());

        PacketClient packetClient = Meybee.getInstance().getProvider(PacketClient.class, PacketRegistry.DEFAULT_NAME);
        SylasPacketRegister.register(packetClient.getPacketRegistry());

        try {
            Meybee.getInstance().getProvider(GsonProvider.class).preLoadConfig(ReportGuiConfig.class);
        } catch (IOException | InstantiationException | IllegalAccessException exception) {
            exception.printStackTrace();
        }

        LanguagePack languagePack = Meybee.getInstance().getProvider(LanguageRepository.class).loadPack("sylas");

        getCommand("report").setExecutor(new ReportCommand(languagePack));

        Meybee.getInstance().registerProvider(SpectatorRepository.class, new CommonSpectatorRepository());

        if (getServer().getPluginManager().isPluginEnabled("ExploitFixer")) {
            try {
                getServer().getPluginManager().registerEvents(new ExploitFixerListener(packetClient), this);
            } catch (IOException | InstantiationException | IllegalAccessException exception) {
                exception.printStackTrace();
            }
            getCommand("simulateexploit").setExecutor(new ExploitFixerSimulateCommand());
        }
    }
}
