package net.maybemc.sylas.spectate;

import me.lucko.helper.item.ItemStackBuilder;
import me.lucko.helper.menu.Gui;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.meybee.spigot.config.GlobalDesignConfig;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class SpectateSessionGui extends Gui {
    private final User user;
    private final LanguagePack languagePack;
    private final SpectatorSession session;

    public SpectateSessionGui(Player player, User user, LanguagePack languagePack, SpectatorSession session) {
        super(player, 3, languagePack.translate(user, "spectate-session-gui-title"));
        this.user = user;
        this.languagePack = languagePack;
        this.session = session;
    }

    @Override
    public void redraw() {
        if (isFirstDraw()) {
            GlobalDesignConfig designConfig = Meybee.getInstance().getProvider(GlobalDesignConfig.class);
            designConfig.drawGlass(this, user, 0, 2);
        }

        drawTargetItem();
        drawTeleportItem();
        drawCPSCheckItem();
        drawKnockbackCheckItem();
    }

    private void drawCPSCheckItem() {
        boolean activated = session.showTargetClicks();
        ItemStackBuilder itemStackBuilder = ItemStackBuilder.of(Material.COMMAND_BLOCK)
            .name(languagePack.translate(user, "spectate-session-gui-cps-" + (activated ? "active" : "inactive")))
            .hideAttributes();
        if (activated) {
            itemStackBuilder = itemStackBuilder.enchant(Enchantment.KNOCKBACK);
        }

        setItem(10, itemStackBuilder.buildConsumer(event -> {
            session.setShowTargetClicks(!activated);
            redraw();
            getPlayer().playSound(getPlayer(), Sound.ITEM_ARMOR_EQUIP_GOLD, 1F, 1F);
        }));
    }

    private void drawKnockbackCheckItem() {
        if (!getPlayer().hasPermission("sylas.spectate.command.knockback")) {
            setItem(13, ItemStackBuilder.of(Material.BARRIER)
                .name(languagePack.translate(user, "spectate-session-gui-knockback-no-permission"))
                .buildItem().build());
            return;
        }

        boolean activated = session.canTestKnockback();
        ItemStackBuilder itemStackBuilder = ItemStackBuilder.of(Material.LEATHER_BOOTS)
            .name(languagePack.translate(user, "spectate-session-gui-knockback-" + (activated ? "active" : "inactive")))
            .hideAttributes();
        if (activated) {
            itemStackBuilder = itemStackBuilder.enchant(Enchantment.KNOCKBACK);
        }

        setItem(13, itemStackBuilder.buildConsumer(event -> {
            session.setCanTestKnockback(!activated);
            redraw();
            getPlayer().playSound(getPlayer(), Sound.ITEM_ARMOR_EQUIP_GOLD, 1F, 1F);
        }));
    }

    private void drawTeleportItem() {
        Optional<Player> optionalTarget = session.getTarget();
        if (optionalTarget.isPresent()) {
            Player target = optionalTarget.get();
            setItem(16, ItemStackBuilder.of(Material.ENDER_PEARL)
                .name(languagePack.translate(user, "spectate-session-gui-teleport"))
                .buildConsumer(event -> {
                    close();
                    getPlayer().teleport(target);
                    getPlayer().playSound(getPlayer(), Sound.ENTITY_ENDERMAN_TELEPORT, 1F, 1F);
                }));
        } else {
            setItem(16, ItemStackBuilder.of(Material.BARRIER)
                .name(languagePack.translate(user, "spectate-session-gui-teleport-none"))
                .buildItem().build());
        }
    }

    private void drawTargetItem() {
        Optional<Player> optionalTarget = session.getTarget();
        if (optionalTarget.isPresent()) {
            Player target = optionalTarget.get();
            User targetUser = Meybee.getInstance().getProvider(UserRepository.class).getUser(target.getUniqueId());
            Futures.thenAcceptBoth(targetUser.getNickFor(user), targetUser.getRealNick(), (nick, realNick) -> {
                String name;
                String[] lore;
                if (nick.equals(realNick)) {
                    name = languagePack.translate(user, "spectate-session-gui-target-name", realNick.getDisplayName());
                    lore = languagePack.translate(user, "spectate-session-gui-target-lore", realNick.getDisplayName()).split("\n");
                } else {
                    name = languagePack.translate(user, "spectate-session-gui-target-name-nicked", nick.getDisplayName(), realNick.getDisplayName());
                    lore = languagePack.translate(user, "spectate-session-gui-target-lore-nicked", nick.getDisplayName(), realNick.getDisplayName()).split("\n");
                }

                setItem(4, ItemStackBuilder.of(Material.PLAYER_HEAD)
                    .texture(nick.getSkin().getValue())
                    .name(name).lore(lore)
                    .buildItem().build());
            }, 10L, TimeUnit.SECONDS).exceptionally(throwable -> {
                setItem(4, ItemStackBuilder.of(Material.PLAYER_HEAD)
                    .owner(((CraftPlayer) target).getProfile())
                    .name(languagePack.translate(user, "spectate-session-gui-target-name", target.getDisplayName()))
                    .buildItem().build());
                throwable.printStackTrace();
                return null;
            });
        } else {
            setItem(4, ItemStackBuilder.of(Material.BARRIER)
                .name(languagePack.translate(getPlayer().getUniqueId(), "spectate-session-gui-target-none"))
                .buildItem().build());
        }
    }
}
