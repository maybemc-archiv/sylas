package net.maybemc.sylas.command;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.sylas.chat.NameTranslator;
import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.repository.exception.TranslatableExceptionMessage;
import net.maybemc.sylas.schema.Ids;
import net.maybemc.sylas.schema.ban.Ban;
import net.maybemc.sylas.schema.ban.Pardon;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class PunishInfoCommand extends Command {
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    private final LanguagePack languagePack;
    private final BanRepository banRepository;
    private final UserRepository userRepository;

    public PunishInfoCommand(LanguagePack languagePack) {
        super("punishinfo", "sylas.punish.info.command", "baninfo");
        this.languagePack = languagePack;
        this.banRepository = Meybee.getInstance().getProvider(BanRepository.class);
        this.userRepository = Meybee.getInstance().getProvider(UserRepository.class);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) commandSender;

        if (args.length != 1) {
            player.sendMessage(languagePack.translate(player.getUniqueId(), "punishinfo-command-usage"));
            return;
        }

        String input = args[0];
        Ids ids = Ids.findById(input);
        if (ids == Ids.BAN) {
            executeInfoForId(player, input);
        } else {
            findUser(player, input);
        }
    }

    private void executeInfoForId(ProxiedPlayer player, String id) {
        Futures.addCallback(banRepository.findBan(id), new FutureCallback<>() {
            @Override
            public void onSuccess(Ban ban) {
                if (ban == null) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "punishinfo-command-ban-not-found"));
                } else {
                    UUID[] uuids;
                    if (ban.getCreator().length() == 36) {
                        uuids = new UUID[] {ban.getTarget(), UUID.fromString(ban.getCreator())};
                    } else {
                        uuids = new UUID[] {ban.getTarget()};
                    }

                    boolean endless = ban.isEndless();
                    NameTranslator.sendNamesMessage(player, languagePack, "punishinfo-command-success" + (endless ? "-endless" : ""),
                        uuids,
                        new int[] {0, 1},
                        null,
                        ban.getCreator(),
                        ban.getId(),
                        DATE_FORMAT.format(new Date(ban.getTime())),
                        endless ? "-" : DATE_FORMAT.format(new Date(ban.getEndTime())),
                        ban.getReason().getId(), ban.getReason().getType(),
                        ban.getAppendant()
                    );

                    Pardon pardon = ban.getActivePardon();
                    if (pardon != null) {
                        if (pardon.getIssuer().length() == 36) {
                            uuids = new UUID[] {UUID.fromString(pardon.getIssuer())};
                        } else {
                            uuids = new UUID[0];
                        }

                        NameTranslator.sendNamesMessage(player, languagePack, "punishinfo-command-success-pardon",
                            uuids,
                            new int[] {0},
                            pardon.getIssuer(),
                            pardon.getReason().getId(),
                            pardon.getAppendant(),
                            DATE_FORMAT.format(new Date(pardon.getTime()))
                        );
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                if (throwable.getCause() instanceof TranslatableExceptionMessage translatableExceptionMessage) {
                    player.sendMessage(translatableExceptionMessage.translate(languagePack, player.getUniqueId()));
                } else {
                    throwable.printStackTrace();
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "error"));
                }
            }
        }, 15L, TimeUnit.SECONDS);
    }

    private void findUser(ProxiedPlayer player, String input) {
        Futures.addCallback(userRepository.findUsers(input), new FutureCallback<>() {
            @Override
            public void onSuccess(List<User> users) {
                if (users.isEmpty()) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "punishinfo-command-target-not-found"));
                } else {
                    User targetUser = users.get(0);
                    printBans(player, targetUser);
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                FutureCallback.super.onFailure(throwable);
                player.sendMessage(languagePack.translate(player.getUniqueId(), "error"));
            }
        });
    }

    private void printBans(ProxiedPlayer player, User targetUser) {
        Futures.addCallback(banRepository.getActiveBans(targetUser.getUniqueId()), new FutureCallback<>() {
            @Override
            public void onSuccess(List<Ban> bans) {
                if (bans == null || bans.isEmpty()) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "punishinfo-command-target-not-banned"));
                } else {
                    NameTranslator.sendNameMessage(player, languagePack, "punishinfo-command-list-header", targetUser.getUniqueId(), 0).thenRun(() -> {
                        for (Ban ban : bans) {
                            player.sendMessage(new ComponentBuilder()
                                .append(TextComponent.fromLegacyText(languagePack.translate(player.getUniqueId(), "punishinfo-command-list-entry",
                                    ban.getId(),
                                    ban.getReason().getId(),
                                    ban.getReason().getType().name()
                                ))).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/punishinfo " + ban.getId()))
                                .create());
                        }
                    });
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                if (throwable.getCause() instanceof TranslatableExceptionMessage translatableExceptionMessage) {
                    player.sendMessage(translatableExceptionMessage.translate(languagePack, player.getUniqueId()));
                } else {
                    throwable.printStackTrace();
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "error"));
                }
            }
        }, 15L, TimeUnit.SECONDS);
    }
}
