package net.maybemc.sylas.chat;

import lombok.ToString;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.meybee.api.user.scope.nick.Nick;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;

/**
 * @author Nico_ND1
 */
public class NameTranslator {
    private static final Queue<Data> DATA_QUEUE = new ArrayDeque<>();
    private static final Lock DATA_QUEUE_LOCK = new ReentrantLock(true);

    private static void nextDataFinished() {
        try {
            DATA_QUEUE_LOCK.lock();
            Data peek = DATA_QUEUE.peek();
            if (peek != null && peek.readySupplier.get()) {
                peek.runnable.run();
                DATA_QUEUE.remove();

                nextDataFinished();
            }
        } finally {
            DATA_QUEUE_LOCK.unlock();
        }
    }

    public static CompletableFuture<Void> sendNameMessage(ProxiedPlayer player, LanguagePack languagePack, String key, UUID targetUuid, int argIndex, Object... args) {
        return sendNamesMessage(player, languagePack, key, new UUID[] {targetUuid}, new int[] {argIndex}, args);
    }

    public static CompletableFuture<Void> sendNamesMessage(ProxiedPlayer player, LanguagePack languagePack, String key, UUID[] targetUuids, int[] argIndexes, Object... args) {
        CompletableFuture<Void> future = new CompletableFuture<>();

        Data data = new Data();
        data.readySupplier = new AtomicReference<>(false);
        try {
            DATA_QUEUE_LOCK.lock();
            DATA_QUEUE.add(data);
        } finally {
            DATA_QUEUE_LOCK.unlock();
        }

        if (targetUuids.length == 0) {
            data.runnable = () -> {
                player.sendMessage(languagePack.translate(player.getUniqueId(), key, args));
            };
            data.readySupplier.set(true);
            nextDataFinished();
            future.complete(null);
            return future;
        }

        AtomicReference<Object[]> argsReference = new AtomicReference<>(args);
        UserRepository userRepository = Meybee.getInstance().getProvider(UserRepository.class);
        String[] displayNames = new String[targetUuids.length];
        Lock lock = new ReentrantLock();
        AtomicInteger count = new AtomicInteger();

        for (int i = 0; i < targetUuids.length; i++) {
            int j = i;
            UUID targetUuid = targetUuids[i];
            User targetUser = userRepository.getOfflineUser(targetUuid);
            Futures.addCallback(targetUser.asNick(), new FutureCallback<Nick>() {
                @Override
                public void onSuccess(Nick nick) {
                    try {
                        lock.lock();
                        displayNames[j] = nick.getDisplayName();
                    } finally {
                        lock.unlock();
                    }

                    proceed();
                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {
                    Meybee.getLogger().log(Level.WARNING, "Couldn't send name message to " + player.getUniqueId() + ": " + key, throwable);
                    displayNames[j] = targetUuid.toString();
                    proceed();
                }

                private void proceed() {
                    if (count.incrementAndGet() == targetUuids.length) {
                        Object[] args = argsReference.get();
                        if (args.length == 0) {
                            args = new Object[targetUuids.length];
                        }

                        for (int k = 0; k < argIndexes.length; k++) {
                            int argIndex = argIndexes[k];
                            args[argIndex] = displayNames[k];
                        }

                        Object[] finalArgs = args;
                        data.runnable = () -> {
                            if (player.isConnected()) {
                                player.sendMessage(languagePack.translate(player.getUniqueId(), key, finalArgs));
                            }
                        };
                        data.readySupplier.set(true);
                        nextDataFinished();

                        future.complete(null);
                    }
                }
            }, 5L, TimeUnit.SECONDS);
        }
        return future;
    }

    @ToString
    private static class Data {
        private AtomicReference<Boolean> readySupplier;
        private Runnable runnable;
    }
}
