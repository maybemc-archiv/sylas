package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.scope.UserScope;
import net.maybemc.meybee.api.user.scope.currency.Currency;
import net.maybemc.meybee.api.user.scope.publisher.PublisherType;
import net.maybemc.sylas.pardon.AutoPardonApplier;
import net.maybemc.sylas.protocol.ConnectionNotifyPacket;
import net.maybemc.sylas.repository.report.ReportRepository;
import net.maybemc.sylas.repository.user.UserRepository;
import net.maybemc.sylas.schema.ban.Ban;
import net.maybemc.sylas.schema.ban.BanType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import org.jetbrains.annotations.NotNull;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

/**
 * @author Nico_ND1
 */
public class LoginListener extends BanListener implements Listener {
    private final PacketClient packetClient;
    private final UserRepository userRepository;
    private final ReportRepository reportRepository;
    private final List<AutoPardonApplier> autoPardonAppliers;

    public LoginListener(LanguagePack languagePack, PacketClient packetClient, ReportRepository reportRepository, List<AutoPardonApplier> autoPardonAppliers) {
        super(languagePack);
        this.packetClient = packetClient;
        this.reportRepository = reportRepository;
        this.autoPardonAppliers = autoPardonAppliers;
        this.userRepository = Meybee.getInstance().getProvider(UserRepository.class);
    }

    @EventHandler(priority = Byte.MIN_VALUE)
    public void onLogin(LoginEvent event) {
        PendingConnection connection = event.getConnection();

        try {
            Ban ban = repository.getActiveBan(connection.getUniqueId(), BanType.NETWORK).get(5L, TimeUnit.SECONDS);
            if (ban != null) {
                boolean pardon = false;
                for (AutoPardonApplier autoPardonApplier : autoPardonAppliers) {
                    if (autoPardonApplier.applyPardon(repository, ban, connection)) {
                        pardon = true;
                        break;
                    }
                }

                if (!pardon) {
                    disallowLogin(event, ban);
                    return;
                }
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            Meybee.getLogger().log(Level.WARNING, "Error retrieving active ban of " + connection, e);
        }

        Meybee.getLogger().info("Notify login of " + connection.getUniqueId());
        InetSocketAddress address = connection.getAddress();
        userRepository.login(connection.getUniqueId(), address.getAddress().getHostAddress());

        collectReportRewards(connection.getUniqueId());
    }

    @EventHandler
    public void onPostLogin(PostLoginEvent event) {
        if (event.getPlayer().hasPermission("sylas.notify.connection")) {
            packetClient.sendPacket(new ConnectionNotifyPacket(event.getPlayer().getUniqueId(), true));
        }
    }

    @EventHandler
    public void onDisconnect(PlayerDisconnectEvent event) {
        if (event.getPlayer().hasPermission("sylas.notify.connection")) {
            packetClient.sendPacket(new ConnectionNotifyPacket(event.getPlayer().getUniqueId(), false));
        }
    }

    private void disallowLogin(LoginEvent event, Ban ban) {
        if (!ban.isActive()) {
            Meybee.getLogger().warning("Ban is present for " + ban.getTarget() + " (" + ban.getId() + ") but its not active");
            return;
        }

        event.setCancelled(true);
        Locale locale = getLocale(ban.getTarget());
        event.setCancelReason(getLoginScreen(ban, locale));
    }

    public String getLoginScreen(Ban ban, UUID uuid) {
        return languagePack.translate(uuid, "login-banned",
            ban.getId(),
            ban.getCreator(),
            languagePack.translate(uuid, "ban-reason-" + ban.getReason().getId()),
            ban.getAppendant(),
            formatDuration(ban.getTarget(), ban)
        );
    }

    public String getLoginScreen(Ban ban, Locale locale) {
        return languagePack.translate(locale, "login-banned",
            ban.getId(),
            ban.getCreator(),
            languagePack.translate(locale, "ban-reason-" + ban.getReason().getId()),
            ban.getAppendant(),
            formatDuration(ban.getTarget(), ban)
        );
    }

    private void collectReportRewards(UUID player) {
        Futures.addCallback(reportRepository.collectRewards(player), new FutureCallback<Integer>() {
            @Override
            public void onSuccess(Integer amount) {
                if (amount != null && amount > 0) {
                    Meybee.getLogger().info("Giving report reward for " + player + ": " + amount);

                    net.maybemc.meybee.api.user.UserRepository userRepository = Meybee.getInstance().getProvider(net.maybemc.meybee.api.user.UserRepository.class);
                    User user = userRepository.getOfflineUser(player);
                    UserScope<Currency> currencyScope = user.getUserScope(User.COINS_KEY);
                    currencyScope.setValueAndPublish(new Currency(amount), PublisherType.DATABASE);

                    ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(player);
                    if (proxiedPlayer != null && proxiedPlayer.isConnected()) {
                        proxiedPlayer.sendMessage(languagePack.translate(proxiedPlayer.getUniqueId(), "report-rewards", amount));
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                Meybee.getLogger().log(Level.WARNING, "Couldn't get amount of coins for report reward for " + player);
            }
        });
    }
}
