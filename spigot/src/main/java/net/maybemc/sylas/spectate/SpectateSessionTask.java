package net.maybemc.sylas.spectate;

import net.maybemc.meybee.api.i18n.LanguagePack;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import java.awt.*;

/**
 * @author Nico_ND1
 */
public class SpectateSessionTask implements Runnable {
    private static final Color DARK_GREEN = ChatColor.DARK_GREEN.getColor();
    private static final Color GREEN = ChatColor.GREEN.getColor();
    private static final Color YELLOW = ChatColor.YELLOW.getColor();
    private static final Color RED = ChatColor.RED.getColor();

    private final LanguagePack languagePack;
    private final SpectatorRepository repository;

    public SpectateSessionTask(LanguagePack languagePack, SpectatorRepository repository) {
        this.languagePack = languagePack;
        this.repository = repository;
    }

    @Override
    public void run() {
        for (SpectatorSession session : repository.getSessions()) {
            Player spectator = session.getSpectatorPlayer();
            if (!session.showTargetClicks()) {
                if (session.getBossBar() != null) {
                    BossBar bossBar = session.getBossBar();
                    bossBar.removePlayer(spectator);
                    session.setBossBar(bossBar);
                }
                continue;
            }

            String title = languagePack.translate(session.getSpectator(), "spectate-session-bossbar",
                formatCPS(session, SpectatorSession.Click.Type.LEFT),
                formatCPS(session, SpectatorSession.Click.Type.RIGHT),
                formatPing(session)
            );

            BossBar bossBar;
            if (session.getBossBar() != null) {
                bossBar = session.getBossBar();
                bossBar.setTitle(title);
            } else {
                bossBar = Bukkit.createBossBar(title, BarColor.WHITE, BarStyle.SOLID);
                session.setBossBar(bossBar);
            }

            if (!bossBar.getPlayers().contains(spectator)) {
                bossBar.addPlayer(spectator);
            }
            bossBar.setVisible(true);
            bossBar.setProgress(1);
        }
    }

    private String formatCPS(SpectatorSession session, SpectatorSession.Click.Type clickType) {
        int cps = session.getClicksPerSecond(clickType);
        return languagePack.translate(session.getSpectator(), "spectate-session-bossbar-cps-" + clickType.name(), cps, colorString(cps, 20, 40, 60));
    }

    private String formatPing(SpectatorSession session) {
        int ping = session.getTargetPing();
        return languagePack.translate(session.getSpectator(), "spectate-session-bossbar-ping", ping, colorString(ping, 30, 100, 200));
    }

    private ChatColor colorString(int actual, int greenThreshold, int yellowThreshold, int redThreshold) {
        if (actual < greenThreshold) {
            return colorTransition(actual, greenThreshold, DARK_GREEN, GREEN);
        } else if (actual < yellowThreshold) {
            return colorTransition(actual, yellowThreshold, GREEN, YELLOW);
        } else if (actual < redThreshold) {
            return colorTransition(actual, redThreshold, YELLOW, RED);
        } else {
            return ChatColor.RED;
        }
    }

    private ChatColor colorTransition(int actual, int higherBound, Color lowerColor, Color higherColor) {
        double percentage = (double) actual / (double) (higherBound - actual);
        return ChatColor.of(new Color(
            (int) (lowerColor.getRed() + percentage * (higherColor.getRed() - lowerColor.getRed())),
            (int) (lowerColor.getGreen() + percentage * (higherColor.getGreen() - lowerColor.getGreen())),
            (int) (lowerColor.getBlue() + percentage * (higherColor.getBlue() - lowerColor.getBlue()))
        ));
    }
}
