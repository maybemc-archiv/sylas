package net.maybemc.sylas.listener;

import com.google.common.base.Strings;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.chat.NameTranslator;
import net.maybemc.sylas.protocol.ChatClearPacket;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class ChatClearPacketListener implements PacketListener<ChatClearPacket> {
    private final LanguagePack languagePack;

    public ChatClearPacketListener(LanguagePack languagePack) {
        this.languagePack = languagePack;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, ChatClearPacket packet) {
        String clearMessage = Strings.repeat("\n§r", 50);

        for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
            Server server = player.getServer();
            if (server == null || server.getInfo() == null || !server.getInfo().getName().equals(packet.getServer())) {
                continue;
            }

            if (player.hasPermission("sylas.chatclear.bypass")) {
                NameTranslator.sendNameMessage(player, languagePack, "chatclear-cleared-by", packet.getIssuer(), 0);
            } else {
                player.sendMessage(clearMessage);
                player.sendMessage(languagePack.translate(player.getUniqueId(), "chatclear-cleared"));
            }
        }
        return instantResult(stopHandling());
    }
}
