package net.maybemc.sylas.command;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.sylas.chat.NameTranslator;
import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.repository.exception.PardonNotOverriddenException;
import net.maybemc.sylas.repository.exception.TooEarlyModificationException;
import net.maybemc.sylas.repository.exception.TranslatableExceptionMessage;
import net.maybemc.sylas.schema.Ids;
import net.maybemc.sylas.schema.ban.Ban;
import net.maybemc.sylas.schema.ban.PardonReason;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class PardonCommand extends Command implements TabExecutor {
    private final LanguagePack languagePack;
    private final BanRepository repository;
    private final UserRepository userRepository;

    public PardonCommand(LanguagePack languagePack) {
        super("unban", "sylas.pardon.command");
        this.languagePack = languagePack;
        this.repository = Meybee.getInstance().getProvider(BanRepository.class);
        this.userRepository = Meybee.getInstance().getProvider(UserRepository.class);
    }

    /*
    none -> only own unbans
    only pardon ban reasons that they have permission for

    ban info
     */

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer sender = (ProxiedPlayer) commandSender;
        /*
        pardon <Target|ID> <Reason> [canOverride:ignoreTimeout] <Appendant[]>
         */

        if (args.length < 2) {
            sender.sendMessage(languagePack.translate(sender.getUniqueId(), "pardon-command-usage"));
            return;
        }

        // TODO: refactor
        boolean canOverride = false;
        boolean ignoreTimeout = false;
        String appendant = "/";
        if (args.length >= 3) {
            try {
                String arg = args[2];
                String[] split = arg.split(":");
                if (split.length == 2) {
                    canOverride = Boolean.parseBoolean(split[0]);
                    ignoreTimeout = Boolean.parseBoolean(split[1]);
                }
            } catch (NumberFormatException ignored) {
                appendant = readAppendant(args, 3);
            }
        } else {
            appendant = readAppendant(args, 2);
        }

        Optional<PardonReason> optionalPardonReason = repository.findPardonReason(args[1]);
        if (!optionalPardonReason.isPresent()) {
            sender.sendMessage(languagePack.translate(sender.getUniqueId(), "pardon-command-reason-not-found"));
            return;
        }

        PardonReason reason = optionalPardonReason.get();
        if (!sender.hasPermission(reason.getPermission())) {
            sender.sendMessage(languagePack.translate(sender.getUniqueId(), "pardon-command-reason-no-permission"));
            return;
        }

        boolean finalCanOverride = canOverride;
        boolean finalIgnoreTimeout = ignoreTimeout;
        String finalAppendant = appendant;

        Ids id = Ids.findById(args[0]);
        if (id == Ids.BAN) {
            handleBanCheck(sender, args[0], () -> handlePardon(null, reason, finalCanOverride, finalIgnoreTimeout, finalAppendant, sender, args[0]));
            return;
        }

        if (args[0].length() == 36) {
            try {
                UUID target = UUID.fromString(args[0]);
                proceed(target, reason, canOverride, ignoreTimeout, appendant, sender);
            } catch (IllegalArgumentException ignored) {
                sender.sendMessage(languagePack.translate(sender.getUniqueId(), "pardon-command-invalid-uuid"));
            }
            return;
        }

        Futures.addCallback(userRepository.findUsers(args[0]), new FutureCallback<List<User>>() {
            @Override
            public void onSuccess(List<User> users) {
                if (users == null || users.isEmpty()) {
                    sender.sendMessage(languagePack.translate(sender.getUniqueId(), "pardon-command-target-not-found"));
                    return;
                }

                User target = users.get(0);
                proceed(target.getUniqueId(), reason, finalCanOverride, finalIgnoreTimeout, finalAppendant, sender);
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                sender.sendMessage(languagePack.translate(sender.getUniqueId(), "pardon-command-target-not-found"));
                Meybee.getLogger().log(Level.WARNING, "Error searching player " + args[0], throwable);
            }
        });
    }

    private String readAppendant(String[] args, int startIndex) {
        String[] cutArgs = new String[args.length - startIndex];
        System.arraycopy(args, startIndex, cutArgs, 0, cutArgs.length);
        return String.join(" ", cutArgs);
    }

    private void proceed(UUID target, PardonReason reason, boolean canOverride, boolean ignoreTimeout, String appendant, ProxiedPlayer sender) {
        Futures.addCallback(repository.getActiveBans(target), new FutureCallback<List<Ban>>() {
            @Override
            public void onSuccess(List<Ban> bans) {
                if (bans == null || bans.isEmpty()) {
                    sender.sendMessage(languagePack.translate(sender.getUniqueId(), "pardon-command-target-not-banned"));
                } else if (bans.size() == 1) {
                    Ban ban = bans.get(0);
                    if (sender.hasPermission(ban.getReason().getPermission())) {
                        handlePardon(target, reason, canOverride, ignoreTimeout, appendant, sender, ban.getId());
                    } else {
                        sender.sendMessage(languagePack.translate(sender.getUniqueId(), "pardon-command-target-ban-no-permission"));
                    }
                } else {
                    ComponentBuilder builder = new ComponentBuilder().append(TextComponent.fromLegacyText(languagePack.translate(sender.getUniqueId(), "pardon-command-multiple-bans-header")));
                    for (Ban ban : bans) {
                        builder.append("\n")
                            .append(languagePack.translate(sender.getUniqueId(), "pardon-command-multiple-bans-entry-prefix"))
                            .append(languagePack.translate(sender.getUniqueId(), "pardon-command-multiple-bans-entry-value", ban.getReason().getId()))
                            .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/unban " + ban.getId() + " " + reason.getId() + " " + (canOverride + ":" + ignoreTimeout) + " " + appendant))
                            .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] {
                                new TextComponent(languagePack.translate(sender.getUniqueId(), "pardon-command-multiple-bans-entry-value-hover", ban.getId()))
                            }));
                    }
                    sender.sendMessage(builder.create());
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                sender.sendMessage(languagePack.translate(sender.getUniqueId(), "pardon-command-multiple-bans-error"));
                Meybee.getLogger().log(Level.WARNING, "Error retrieving active bans of " + target, throwable);
            }
        });
    }

    private void handleBanCheck(ProxiedPlayer sender, String banId, Runnable successRunnable) {
        Futures.addCallback(repository.findBan(banId), new FutureCallback<Ban>() {
            @Override
            public void onSuccess(Ban ban) {
                if (sender.hasPermission(ban.getReason().getPermission())) {
                    successRunnable.run();
                } else {
                    sender.sendMessage(languagePack.translate(sender.getUniqueId(), "pardon-command-target-ban-no-permission"));
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                FutureCallback.super.onFailure(throwable);
                successRunnable.run();
            }
        });
    }

    // TODO: possibility to undo pardon
    private void handlePardon(UUID target, PardonReason reason, boolean canOverride, boolean ignoreTimeout, String appendant, ProxiedPlayer sender, String banId) {
        Futures.addCallback(repository.pardon(sender.getUniqueId().toString(), banId, reason, appendant, canOverride, ignoreTimeout), new FutureCallback<Void>() {
            @Override
            public void onSuccess(Void unused) {
                NameTranslator.sendNameMessage(sender, languagePack, "pardon-command-success", target, 0);
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                throwable = throwable.getCause();
                String argToUse = target == null ? banId : target.toString();

                if (throwable instanceof TooEarlyModificationException) {
                    ComponentBuilder builder = new ComponentBuilder().append(TextComponent.fromLegacyText(languagePack.translate(sender.getUniqueId(), "pardon-command-too-early-header")))
                        .append(languagePack.translate(sender.getUniqueId(), "pardon-command-too-early-yes"))
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] {new TextComponent(languagePack.translate(sender.getUniqueId(), "pardon-command-too-early-hover"))}))
                        .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/unban " + argToUse + " " + reason.getId() + " " + (canOverride + ":" + true) + " " + appendant));
                    sender.sendMessage(builder.create());
                } else if (throwable instanceof PardonNotOverriddenException) {
                    ComponentBuilder builder = new ComponentBuilder().append(TextComponent.fromLegacyText(languagePack.translate(sender.getUniqueId(), "pardon-command-already-pardon-header")))
                        .append(languagePack.translate(sender.getUniqueId(), "pardon-command-already-pardon-yes"))
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] {new TextComponent(languagePack.translate(sender.getUniqueId(), "pardon-command-already-pardon-hover"))}))
                        .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/unban " + argToUse + " " + reason.getId() + " " + (true + ":" + ignoreTimeout) + " " + appendant));
                    sender.sendMessage(builder.create());
                } else if (throwable instanceof TranslatableExceptionMessage) {
                    TranslatableExceptionMessage exception = (TranslatableExceptionMessage) throwable;
                    sender.sendMessage(exception.translate(languagePack, sender.getUniqueId()));
                } else {
                    sender.sendMessage(languagePack.translate(sender.getUniqueId(), "pardon-command-error", throwable.getMessage()));
                    throwable.printStackTrace();
                }
            }
        });
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length == 1) {
            IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
            try {
                Collection<String> names = playerManager.onlinePlayers().asNamesAsync().get(1L, TimeUnit.SECONDS);
                return names.stream()
                    .sorted(String::compareTo)
                    .filter(name -> name.toLowerCase().startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                e.printStackTrace();
            }
        } else if (args.length == 2) {
            List<PardonReason> reasons = repository.listPardonReasons().getNow(Collections.emptyList());
            return reasons.stream()
                .sorted(Comparator.comparing(PardonReason::getId))
                .filter(reason -> sender.hasPermission(reason.getPermission()))
                .map(PardonReason::getId)
                .filter(id -> id.toLowerCase().startsWith(args[1].toLowerCase()))
                .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }
}
