package net.maybemc.sylas.repository.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Getter
public class ReportRateLimitExceededException extends RuntimeException implements TranslatableExceptionMessage {
    private final String who;

    @Override
    public String getLocaleKey() {
        return "exception-reportratelimitexceeded-" + who;
    }
}
