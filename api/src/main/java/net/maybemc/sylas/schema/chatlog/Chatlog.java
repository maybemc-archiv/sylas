package net.maybemc.sylas.schema.chatlog;

import lombok.Data;

/**
 * @author Nico_ND1
 */
@Data
public class Chatlog {
    private final String id;
    private final String target;
    private final long timeCreated;
    private final ChatlogEntry[] entries;
}
