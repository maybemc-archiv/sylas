package net.maybemc.sylas.command;

import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.protocol.atarget.PacketTarget;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.sylas.protocol.TeamChatMessagePacket;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;

/**
 * @author Nico_ND1
 */
public class TeamChatCommand extends Command {
    private final LanguagePack languagePack;
    private final PacketClient packetClient;
    private final boolean local;

    public TeamChatCommand(LanguagePack languagePack, PacketClient packetClient, boolean local) {
        super(local ? "localchat" : "teamchat", "sylas.teamchat.command", local ? "lc" : "tc");
        this.languagePack = languagePack;
        this.packetClient = packetClient;
        this.local = local;
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) commandSender;
        if (args.length == 0) {
            player.sendMessage(languagePack.translate(player.getUniqueId(), (local ? "localchat" : "teamchat") + "-command-usage"));
            return;
        }

        String server = null;
        if (local) {
            Server playerServer = player.getServer();
            if (playerServer == null || playerServer.getInfo() == null) {
                player.sendMessage(languagePack.translate(player.getUniqueId(), "localchat-command-no-server"));
                return;
            }

            server = playerServer.getInfo().getName();
        }

        String message = ChatColor.translateAlternateColorCodes('&', String.join(" ", args));
        packetClient.sendPacket(new TeamChatMessagePacket(-1, -1L, player.getUniqueId(), message, server).target(PacketTarget.server("sylas")));
    }
}
