package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.notify.Notifier;
import net.maybemc.sylas.protocol.ReportCreatedPacket;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class ReportCreatedPacketListener implements PacketListener<ReportCreatedPacket> {
    private final Notifier notifier;

    public ReportCreatedPacketListener(Notifier notifier) {
        this.notifier = notifier;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection connection, ReportCreatedPacket packet) {
        notifier.notifyReportCreated(packet.getReportId());
        return instantResult(stopHandling());
    }
}
