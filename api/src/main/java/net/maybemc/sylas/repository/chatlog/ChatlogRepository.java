package net.maybemc.sylas.repository.chatlog;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public interface ChatlogRepository {

    CompletableFuture<String> createChatlog(@NotNull String target);

}
