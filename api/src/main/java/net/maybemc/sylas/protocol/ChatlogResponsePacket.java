package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;
import net.maybemc.sylas.schema.chatlog.ChatlogEntry;
import net.maybemc.sylas.schema.chatlog.ChatlogEntryFlag;

/**
 * @author Nico_ND1
 */
// TODO: test if packet max size can be exceeded (and if so slice it into many)
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ChatlogResponsePacket extends Packet {
    private ChatlogEntry[] entries;

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        byteBuf.writeInt(entries.length);
        for (ChatlogEntry entry : entries) {
            byteBuf.writeLong(entry.getTime());
            byteBuf.writeInt(entry.getType().ordinal());
            writeString(byteBuf, entry.getIssuer());
            writeString(byteBuf, entry.getValue());

            byteBuf.writeInt(entry.getFlags().length);
            for (ChatlogEntryFlag flag : entry.getFlags()) {
                byteBuf.writeInt(flag.getStartIndex());
                byteBuf.writeInt(flag.getEndIndex());
                byteBuf.writeInt(flag.getType().ordinal());
            }
        }
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
        entries = new ChatlogEntry[byteBuf.readInt()];
        for (int i = 0; i < entries.length; i++) {
            long time = byteBuf.readLong();
            ChatlogEntry.Type type = ChatlogEntry.Type.values()[byteBuf.readInt()];
            String issuer = readString(byteBuf);
            String value = readString(byteBuf);

            ChatlogEntryFlag[] entryFlags = new ChatlogEntryFlag[byteBuf.readInt()];
            for (int i1 = 0; i1 < entryFlags.length; i1++) {
                int startIndex = byteBuf.readInt();
                int endIndex = byteBuf.readInt();
                ChatlogEntryFlag.Type type1 = ChatlogEntryFlag.Type.values()[byteBuf.readInt()];

                entryFlags[i1] = new ChatlogEntryFlag(startIndex, endIndex, type1);
            }

            entries[i] = new ChatlogEntry(time, type, issuer, value, entryFlags);
        }
    }
}
