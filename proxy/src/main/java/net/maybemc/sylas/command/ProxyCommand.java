package net.maybemc.sylas.command;

import de.dytanic.cloudnet.wrapper.Wrapper;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * @author Nico_ND1
 */
public class ProxyCommand extends Command {
    private final LanguagePack languagePack;

    public ProxyCommand(LanguagePack languagePack) {
        super("proxy");
        this.languagePack = languagePack;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        String proxyName = Wrapper.getInstance().getServiceId().getName();
        sender.sendMessage(languagePack.translate(player.getUniqueId(), "proxy-command", proxyName));
    }
}
