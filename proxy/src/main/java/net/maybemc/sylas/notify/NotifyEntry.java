package net.maybemc.sylas.notify;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Nico_ND1
 */
public class NotifyEntry {
    private final Map<NotifyType, Boolean> values;
    private final Map<NotifyType, Boolean> changedValues;

    public NotifyEntry(Map<NotifyType, Boolean> values) {
        this.values = values;
        this.changedValues = new HashMap<>();
    }

    public boolean getValue(NotifyType notifyType) {
        if (changedValues.containsKey(notifyType)) {
            return changedValues.get(notifyType);
        }
        return values.getOrDefault(notifyType, notifyType.getDefaultValue());
    }

    public void setValue(NotifyType notifyType, boolean value) {
        changedValues.put(notifyType, value);
    }

    public Map<NotifyType, Boolean> getDifferentValues() {
        Map<NotifyType, Boolean> map = new HashMap<>();
        for (NotifyType notifyType : NotifyType.values()) {
            Boolean value = values.get(notifyType);
            Boolean newValue = changedValues.get(notifyType);

            if (newValue != null && (value == null || value != newValue)) {
                map.put(notifyType, newValue);
            }
        }
        return map;
    }
}
