package net.maybemc.sylas.schema.report;

import lombok.Data;

import java.util.List;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
@Data
public class Report {
    private final String id;
    private final long timeCreated;
    private ReportStatus status;
    private final List<String> issuer;
    private final UUID target;
    private final ReportReason reason;
    private final List<String> appendants;
    private final List<String> flags;
    private final List<UUID> moderators;
}
