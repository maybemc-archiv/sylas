package net.maybemc.sylas.chat.filter;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.repository.chatlog.ChatlogRepository;
import net.maybemc.sylas.repository.report.ReportRepository;
import net.maybemc.sylas.schema.ban.BanReason;
import net.maybemc.sylas.schema.report.ReportReason;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class ChatFilterPunisher {
    private final BanRepository banRepository;
    private final ReportRepository reportRepository;
    private final ChatlogRepository chatlogRepository;

    public ChatFilterPunisher(BanRepository banRepository, ReportRepository reportRepository, ChatlogRepository chatlogRepository) {
        this.banRepository = banRepository;
        this.reportRepository = reportRepository;
        this.chatlogRepository = chatlogRepository;
    }

    public ChatFilterPunisher() {
        this(
            Meybee.getInstance().getProvider(BanRepository.class),
            Meybee.getInstance().getProvider(ReportRepository.class),
            Meybee.getInstance().getProvider(ChatlogRepository.class)
        );
    }

    public void apply(UUID player, ChatFilterResult result) {
        String punishReasonId = null;
        String reportReasonId = null;

        for (ChatFilterWord detectedWord : result.getDetectedWords()) {
            if (detectedWord.getPunishReasonId() != null) {
                punishReasonId = detectedWord.getPunishReasonId();
            } else if (detectedWord.getReportReasonId() != null) {
                reportReasonId = detectedWord.getReportReasonId();
            }
        }

        if (punishReasonId != null) {
            String finalPunishReasonId = punishReasonId;
            findChatlogAndProceed(player, id -> {
                BanReason banReason = banRepository.findBanReason(finalPunishReasonId).orElseThrow(() -> new NullPointerException("punish reason not found: " + finalPunishReasonId));
                banRepository.ban("chatfilter", player, banReason, id, true, true);
            });
        } else if (reportReasonId != null) {
            String finalReportReasonId = reportReasonId;
            System.out.println("punishing with id " + reportReasonId);
            findChatlogAndProceed(player, id -> {
                System.out.println("found chatlog: " + id);
                ReportReason reportReason = reportRepository.findReportReason(finalReportReasonId).orElseThrow(() -> new NullPointerException("report reason not found: " + finalReportReasonId));
                reportRepository.report("chatfilter", player, reportReason, "", result.getDetectedWords().stream()
                    .map(word -> "chatfilter:" + word.getWord())
                    .collect(Collectors.toList()));
            });
        }
    }

    private void findChatlogAndProceed(UUID target, Consumer<String> idConsumer) {
        Futures.addCallback(chatlogRepository.createChatlog(target.toString()), new FutureCallback<>() {
            @Override
            public void onSuccess(String chatlogId) {
                idConsumer.accept(chatlogId == null ? "" : chatlogId);
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                FutureCallback.super.onFailure(throwable);
                idConsumer.accept("");
            }
        });
    }

}
