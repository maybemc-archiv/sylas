package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;
import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.schema.ban.Ban;
import net.maybemc.sylas.schema.ban.Pardon;
import net.maybemc.sylas.schema.ban.PardonReason;

/**
 * @author Nico_ND1
 */
public class PardonCreatedPacket extends BanCreatedPacket {
    public PardonCreatedPacket(BanRepository banRepository, Ban ban) {
        super(banRepository, ban);
    }

    public PardonCreatedPacket(BanRepository banRepository) {
        super(banRepository);
    }

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        super.write(byteBuf);

        Pardon pardon = ban.getActivePardon();
        writeString(byteBuf, pardon.getReason().getId());
        writeString(byteBuf, pardon.getIssuer());
        writeString(byteBuf, pardon.getAppendant());
        byteBuf.writeLong(pardon.getTime());
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
        super.read(byteBuf);


        String pardonReasonId = readString(byteBuf);
        PardonReason reason = banRepository.findPardonReason(pardonReasonId).orElseThrow(() -> new PacketReadWriteException("pardon reason id not found: " + pardonReasonId));
        String issuer = readString(byteBuf);
        String appendant = readString(byteBuf);
        long time = byteBuf.readLong();

        Pardon pardon = new Pardon(reason, issuer, appendant, time);
        ban.setActivePardon(pardon);
    }
}
