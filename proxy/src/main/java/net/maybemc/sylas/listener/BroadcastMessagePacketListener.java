package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.protocol.BroadcastMessagePacket;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class BroadcastMessagePacketListener implements PacketListener<BroadcastMessagePacket> {
    private final LanguagePack languagePack;

    public BroadcastMessagePacketListener(LanguagePack languagePack) {
        this.languagePack = languagePack;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, BroadcastMessagePacket packet) {
        String message = ChatColor.translateAlternateColorCodes('&', packet.getMessage());
        for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
            player.sendMessage(languagePack.translate(player.getUniqueId(), "broadcast-message", message));
        }
        return instantResult(stopHandling());
    }
}
