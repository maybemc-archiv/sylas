package net.maybemc.sylas.repository.report;

import net.maybemc.meybee.api.future.Futures;
import net.maybemc.sylas.repository.RestRepository;
import net.maybemc.sylas.repository.exception.ReportAlreadyJudgedException;
import net.maybemc.sylas.repository.exception.ReportNotFoundException;
import net.maybemc.sylas.repository.exception.ReportRateLimitExceededException;
import net.maybemc.sylas.repository.exception.UnknownStatusException;
import net.maybemc.sylas.schema.ban.BanReason;
import net.maybemc.sylas.schema.report.Report;
import net.maybemc.sylas.schema.report.ReportJudgement;
import net.maybemc.sylas.schema.report.ReportReason;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class RestReportRepository extends RestRepository implements ReportRepository {
    private CompletableFuture<List<ReportReason>> reportReasons;

    public RestReportRepository() {
        Futures.scheduledExecutor().scheduleAtFixedRate(this::listReportReasons, 0L, 60L, TimeUnit.MINUTES);
    }

    @Override
    public CompletableFuture<List<ReportReason>> listReportReasons() {
        if (reportReasons == null) {
            reportReasons = CompletableFuture.supplyAsync(() -> {
                Response response = testResponse(createJsonTarget("report/reasons").get());
                return response.readEntity(new GenericType<List<ReportReason>>() {
                });
            }, asyncExecutor());
        }
        return reportReasons;
    }

    @Override
    public Optional<ReportReason> findReportReason(String id) {
        return listReportReasons().getNow(Collections.emptyList()).stream()
            .filter(reason -> reason.getId().equalsIgnoreCase(id))
            .findFirst();
    }

    @Override
    public CompletableFuture<String> report(@NotNull String issuer, @NotNull UUID target, @NotNull ReportReason reason, @Nullable String appendant, @Nullable List<String> flags) {
        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(createJsonTarget("report")
                .header("issuer", issuer)
                .header("target", target)
                .header("reasonId", reason.getId())
                .header("appendant", appendant)
                .header("flags", flags)
                .post(newEntity()));

            switch (response.getStatus()) {
                case 429:
                    throw new ReportRateLimitExceededException(response.readEntity(String.class));
                case 200:
                    return response.readEntity(String.class);
                default:
                    throw new UnknownStatusException(response);
            }
        }, asyncExecutor());
    }

    @Override
    public CompletableFuture<Report> getReportSelection(@NotNull UUID moderator) {
        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(requestJson(createTarget("report/selection/{uuid}")
                .resolveTemplate("uuid", moderator))
                .get());

            switch (response.getStatus()) {
                case 204:
                    return null;
                case 200:
                    return response.readEntity(Report.class);
                default:
                    throw new UnknownStatusException(response);
            }
        });
    }

    @Override
    public CompletableFuture<String> judgeReport(@NotNull Report report, @NotNull ReportJudgement judgement, @Nullable UUID moderator, @Nullable BanReason banReason) {
        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(requestJson(createTarget("report/{id}")
                .resolveTemplate("id", report.getId()))
                .header("moderator", moderator)
                .header("judgement", judgement.name())
                .header("banReasonId", banReason == null ? null : banReason.getId())
                .post(newEntity()));

            switch (response.getStatus()) {
                case 409:
                    throw new ReportAlreadyJudgedException();
                case 404:
                    throw new ReportNotFoundException();
                case 204:
                    return null;
                case 200:
                    return response.readEntity(String.class);
                default:
                    throw new UnknownStatusException(response);
            }
        });
    }

    @Override
    public CompletableFuture<Integer> collectRewards(@NotNull UUID player) {
        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(createJsonTarget("report/collect-rewards")
                .header("player", player)
                .post(newEntity()));

            switch (response.getStatus()) {
                case 200:
                    return response.readEntity(Integer.class);
                default:
                    throw new UnknownStatusException(response);
            }
        }, asyncExecutor());
    }
}
