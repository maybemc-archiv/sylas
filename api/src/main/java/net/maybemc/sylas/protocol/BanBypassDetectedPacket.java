package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;
import net.maybemc.sylas.schema.ban.BanType;

import java.util.List;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class BanBypassDetectedPacket extends Packet {
    private UUID testedPlayer;
    private List<Account> accounts;

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        writeUUID(byteBuf, testedPlayer);
        writeList(byteBuf, accounts, (byteBuf1, account) -> {
            writeUUID(byteBuf1, account.getUuid());
            writeString(byteBuf1, account.getIp());
            writeString(byteBuf1, account.getBanId());
            writeVarInt(account.getBanType().ordinal(), byteBuf1);
        });
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
        testedPlayer = readUUID(byteBuf);
        accounts = readList(byteBuf, byteBuf1 -> new Account(
            readUUID(byteBuf1),
            readString(byteBuf1),
            readString(byteBuf1),
            BanType.values()[readVarInt(byteBuf1)]
        ));
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Account {
        private UUID uuid;
        private String ip;
        private String banId;
        private BanType banType;
    }
}
