package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.permission.Group;
import net.maybemc.meybee.api.permission.GroupRepository;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.meybee.api.user.scope.UserScope;
import net.maybemc.sylas.protocol.TeamMembersRequestPacket;
import net.maybemc.sylas.protocol.TeamMembersResponsePacket;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class TeamMembersRequestPacketListener implements PacketListener<TeamMembersRequestPacket> {
    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection connection, TeamMembersRequestPacket packet) {
        CompletableFuture<HandleResult> future = new CompletableFuture<>();
        List<TeamMembersResponsePacket.TeamMember> members = new ArrayList<>();
        UserRepository userRepository = Meybee.getInstance().getProvider(UserRepository.class);
        GroupRepository groupRepository = Meybee.getInstance().getProvider(GroupRepository.class);
        AtomicInteger count = new AtomicInteger();

        List<ProxiedPlayer> players = ProxyServer.getInstance().getPlayers().stream()
            .filter(player -> player.hasPermission("teammember"))
            .collect(Collectors.toList());
        if (players.isEmpty()) {
            return instantResult(stopHandling(new TeamMembersResponsePacket(members)));
        }

        for (ProxiedPlayer player : players) {
            User user = userRepository.getOfflineUser(player.getUniqueId());
            UserScope<Group> groupScope = user.getUserScope(User.GROUP_KEY);

            AtomicReference<String> serverName = new AtomicReference<>("?");
            Server server = player.getServer();
            if (server != null && server.getInfo() != null) {
                serverName.set(server.getInfo().getName());
            }

            Futures.addCallback(groupScope.loadValue(), new FutureCallback<Group>() {
                @Override
                public void onSuccess(Group group) {
                    members.add(new TeamMembersResponsePacket.TeamMember(player.getName(), group.getID(), serverName.get()));
                    process();
                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {
                    members.add(new TeamMembersResponsePacket.TeamMember(player.getName(), groupRepository.getTotalGroups().get(0).getID(), serverName.get()));
                    process();
                }

                private void process() {
                    if (count.incrementAndGet() == players.size()) {
                        future.complete(stopHandling(new TeamMembersResponsePacket(members)));
                    }
                }
            }, 5L, TimeUnit.SECONDS);
        }

        return future;
    }
}
