package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.protocol.atarget.PacketTarget;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.protocol.JumpToPacket;
import net.maybemc.sylas.protocol.ReportSelectedPacket;
import net.maybemc.sylas.protocol.SpectatePacket;
import net.maybemc.sylas.repository.report.ReportRepository;
import net.maybemc.sylas.schema.report.ReportStatus;
import net.md_5.bungee.api.ProxyServer;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class ReportSelectedPacketListener implements PacketListener<ReportSelectedPacket> {
    private final ReportRepository repository;
    private final PacketClient packetClient;

    public ReportSelectedPacketListener(ReportRepository repository, PacketClient packetClient) {
        this.repository = repository;
        this.packetClient = packetClient;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, ReportSelectedPacket packet) {
        if (ProxyServer.getInstance().getPlayer(packet.getPlayer()) == null) {
            return instantResult(continueHandling());
        }

        return Futures.thenCompose(repository.getReportSelection(packet.getPlayer()), report -> {
            if (report.getStatus() == ReportStatus.OPEN) {
                UUID target = report.getTarget();
                return packetClient.sendPacket(new SpectatePacket(packet.getPlayer(), target).target(PacketTarget.playerServer(target)), SpectatePacket.class).thenApply(response -> {
                    if (response != null && packet.getPlayer().equals(response.getSpectator())) {
                        packetClient.sendPacket(new JumpToPacket(packet.getPlayer(), target));
                        return continueHandling(true);
                    } else {
                        return continueHandling(false);
                    }
                });
            }
            return instantResult(continueHandling(false));
        }, 10L, TimeUnit.SECONDS).exceptionally(throwable -> {
            throwable.printStackTrace();
            return continueHandling(false);
        });
    }
}
