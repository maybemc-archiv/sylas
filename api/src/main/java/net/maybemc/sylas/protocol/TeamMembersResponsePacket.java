package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class TeamMembersResponsePacket extends Packet {
    private List<TeamMember> members;

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        byteBuf.writeInt(members.size());
        for (TeamMember member : members) {
            writeString(byteBuf, member.name);
            writeString(byteBuf, member.groupId);
            writeString(byteBuf, member.server);
        }
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
        int size = byteBuf.readInt();
        members = new ArrayList<>(size);

        for (int i = 0; i < size; i++) {
            TeamMember teamMember = new TeamMember(readString(byteBuf), readString(byteBuf), readString(byteBuf));
            members.add(teamMember);
        }
    }

    @Data
    public static class TeamMember {
        private final String name;
        private final String groupId;
        private final String server;
    }
}
