package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.notify.Notifier;
import net.maybemc.sylas.protocol.KickInfoPacket;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class KickInfoPacketListener implements PacketListener<KickInfoPacket> {
    private final Notifier notifier;

    public KickInfoPacketListener(Notifier notifier) {
        this.notifier = notifier;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, KickInfoPacket kickPacket) {
        notifier.notifyKick(kickPacket);
        return instantResult(stopHandling());
    }
}
