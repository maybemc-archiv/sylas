package net.maybemc.sylas.command;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.BridgeServiceProperty;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import de.dytanic.cloudnet.ext.bridge.player.NetworkServiceInfo;
import de.dytanic.cloudnet.ext.bridge.player.ServicePlayer;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class SendCommand extends Command implements TabExecutor {
    private final LanguagePack languagePack;
    private final IPlayerManager playerManager;

    public SendCommand(LanguagePack languagePack) {
        super("send", "sylas.send.command");
        this.languagePack = languagePack;
        this.playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        /*
        send <Target> <Server>
         */
        ProxiedPlayer sender = (ProxiedPlayer) commandSender;
        if (args.length != 2) {
            sender.sendMessage(languagePack.translate(sender.getUniqueId(), "send-command-usage"));
            return;
        }

        CompletableFuture<List<UUID>> targetPlayers;
        try {
            Type type = Type.valueOf(args[0].toUpperCase());
            if (!sender.hasPermission("sylas.send.command.type." + type.name())) {
                sender.sendMessage(languagePack.translate(sender.getUniqueId(), "send-command-type-no-permission"));
                return;
            }

            targetPlayers = type.collectPlayers(sender);
        } catch (IllegalArgumentException ignored) {
            targetPlayers = findPlayer(args[0]);
        }

        Futures.addCallback(targetPlayers, new FutureCallback<List<UUID>>() {
            @Override
            public void onSuccess(List<UUID> uuids) {
                if (uuids == null || uuids.isEmpty()) {
                    sender.sendMessage(languagePack.translate(sender.getUniqueId(), "send-command-no-target-found"));
                    return;
                }

                CloudNetDriver.getInstance().getCloudServiceProvider().getCloudServiceByNameAsync(args[1]).onComplete(infoSnapshot -> {
                    if (infoSnapshot == null) {
                        sender.sendMessage(languagePack.translate(sender.getUniqueId(), "send-command-target-server-not-found"));
                    } else {
                        for (UUID uuid : uuids) {
                            playerManager.getPlayerExecutor(uuid).connect(infoSnapshot.getName());
                        }

                        sender.sendMessage(languagePack.translate(sender.getUniqueId(), "send-command-success", uuids.size(), infoSnapshot.getName()));
                    }
                }).onFailure(throwable -> {
                    Meybee.getLogger().log(Level.WARNING, "Error finding send server for " + args[1], throwable);
                    sender.sendMessage(languagePack.translate(sender.getUniqueId(), "send-command-target-server-error", throwable.getMessage()));
                });
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                Meybee.getLogger().log(Level.WARNING, "Error finding send targets for " + args[0], throwable);
                sender.sendMessage(languagePack.translate(sender.getUniqueId(), "send-command-error", throwable.getMessage()));
            }
        }, 5L, TimeUnit.SECONDS);
    }

    private CompletableFuture<List<UUID>> findPlayer(String name) {
        CompletableFuture<List<UUID>> future = new CompletableFuture<>();
        playerManager.getFirstOnlinePlayerAsync(name).onComplete(cloudPlayer -> {
            if (cloudPlayer == null) {
                future.complete(Collections.emptyList());
            } else {
                future.complete(Collections.singletonList(cloudPlayer.getUniqueId()));
            }
        }).onFailure(future::completeExceptionally);
        return future;
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length == 1) {
            try {
                List<String> names = new ArrayList<>(playerManager.onlinePlayers().asNamesAsync().get(1L, TimeUnit.SECONDS));
                for (Type value : Type.values()) {
                    if (sender.hasPermission("sylas.send.command.type." + value.name())) {
                        names.add(0, value.name());
                    }
                }
                return names.stream()
                    .sorted(String::compareTo)
                    .filter(name -> name.toLowerCase().startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                e.printStackTrace();
            }
        } else if (args.length == 2) {
            return ProxyServer.getInstance().getServers().keySet().stream()
                .sorted(String::compareTo)
                .filter(s -> s.toLowerCase().startsWith(args[1].toLowerCase()))
                .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public enum Type {
        CURRENT {
            @Override
            public CompletableFuture<List<UUID>> collectPlayers(ProxiedPlayer executor) {
                IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
                CompletableFuture<List<UUID>> future = new CompletableFuture<>();
                playerManager.getOnlinePlayerAsync(executor.getUniqueId()).onComplete(cloudPlayer -> {
                    NetworkServiceInfo serviceInfo = cloudPlayer.getConnectedService();
                    if (serviceInfo == null) {
                        future.complete(Collections.emptyList());
                    } else {
                        CloudNetDriver.getInstance().getCloudServiceProvider().getCloudServiceByNameAsync(serviceInfo.getServerName()).onComplete(infoSnapshot -> {
                            Optional<Collection<ServicePlayer>> property = infoSnapshot.getProperty(BridgeServiceProperty.PLAYERS);
                            if (property.isPresent()) {
                                future.complete(property.get().stream()
                                    .map(ServicePlayer::getUniqueId)
                                    .collect(Collectors.toList()));
                            } else {
                                future.complete(Collections.emptyList());
                            }
                        }).onFailure(future::completeExceptionally);
                    }
                }).onFailure(future::completeExceptionally);
                return future;
            }
        },
        ALL {
            @Override
            public CompletableFuture<List<UUID>> collectPlayers(ProxiedPlayer executor) {
                IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
                CompletableFuture<List<UUID>> future = new CompletableFuture<>();
                playerManager.onlinePlayers().asUUIDsAsync().onComplete(uuids -> {
                    future.complete(new ArrayList<>(uuids));
                }).onFailure(future::completeExceptionally);
                return future;
            }
        };

        public abstract CompletableFuture<List<UUID>> collectPlayers(ProxiedPlayer executor);
    }
}
