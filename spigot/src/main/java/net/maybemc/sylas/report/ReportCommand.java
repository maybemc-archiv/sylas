package net.maybemc.sylas.report;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import me.lucko.helper.menu.Gui;
import me.lucko.helper.scheduler.HelperExecutors;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.meybee.api.user.scope.UserScope;
import net.maybemc.meybee.api.user.scope.nick.Nick;
import net.maybemc.sylas.repository.exception.TranslatableExceptionMessage;
import net.maybemc.sylas.repository.report.ReportRepository;
import net.maybemc.sylas.schema.report.ReportReason;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class ReportCommand implements CommandExecutor, TabCompleter {
    private final LanguagePack languagePack;
    private final ReportRepository repository;
    private final UserRepository userRepository;

    public ReportCommand(LanguagePack languagePack) {
        this.languagePack = languagePack;
        this.repository = Meybee.getInstance().getProvider(ReportRepository.class);
        this.userRepository = Meybee.getInstance().getProvider(UserRepository.class);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        /*
        report <Name> <Reason> ["please-confirm"]
         */

        if (args.length == 1 || args.length == 2 || args.length == 3) {
            String issuer = player.getUniqueId().toString();
            String targetName = args[0];
            if (player.getName().equalsIgnoreCase(targetName)) {
                sender.sendMessage(languagePack.translate(player.getUniqueId(), "report-command-self-report-not-possible"));
                return false;
            }

            IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
            playerManager.getFirstOnlinePlayerAsync(targetName).onComplete(cloudPlayer -> {
                List<User> users;
                if (cloudPlayer == null) {
                    users = findNick(args[0]);
                } else {
                    users = Collections.singletonList(userRepository.getOfflineUser(cloudPlayer.getUniqueId()));
                }

                if (!users.isEmpty()) {
                    User targetUser = users.get(0);
                    if (args.length == 1) {
                        openGui(player, targetName);
                        return;
                    }

                    Optional<ReportReason> optionalReason = repository.findReportReason(args[1]);
                    if (!optionalReason.isPresent()) {
                        sender.sendMessage(languagePack.translate(player.getUniqueId(), "report-command-reason-not-found"));
                        return;
                    }

                    if (args.length == 3 && args[2].equalsIgnoreCase("please-confirm")) {
                        ReportConfirmGui.open(userRepository.getUser(player.getUniqueId()), targetUser, args[0], optionalReason.get().getId(), languagePack, repository);
                        return;
                    }

                    Futures.addCallback(repository.report(issuer, targetUser.getUniqueId(), optionalReason.get(), "", Collections.emptyList()), new FutureCallback<String>() {
                        @Override
                        public void onSuccess(String reportId) {
                            if (reportId == null) {
                                sender.sendMessage(languagePack.translate(player.getUniqueId(), "report-command-success-error"));
                            } else {
                                sender.sendMessage(languagePack.translate(player.getUniqueId(), "report-command-success", reportId));
                                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1F, 1F);
                            }
                        }

                        @Override
                        public void onFailure(@NotNull Throwable throwable) {
                            throwable = throwable.getCause();

                            if (throwable instanceof TranslatableExceptionMessage) {
                                sender.sendMessage(((TranslatableExceptionMessage) throwable).translate(languagePack, player.getUniqueId()));
                            } else {
                                sender.sendMessage(languagePack.translate(player.getUniqueId(), "report-command-error", throwable.getMessage()));
                                Meybee.getLogger().log(Level.WARNING, "Error reporting player " + targetUser.getUniqueId(), throwable);
                            }
                        }
                    }, HelperExecutors.sync());
                } else {
                    sender.sendMessage(languagePack.translate(player.getUniqueId(), "report-command-target-not-found"));
                }
            }).onFailure(throwable -> {
                sender.sendMessage(languagePack.translate(player.getUniqueId(), "report-command-target-not-found"));
                Meybee.getLogger().log(Level.WARNING, "Error finding player " + args[0], throwable);
            });
            return true;
        } else {
            sender.sendMessage(languagePack.translate(player.getUniqueId(), "report-command-usage"));
        }
        return false;
    }

    private List<User> findNick(String name) {
        for (User onlineUser : userRepository.getOnlineUsers()) {
            UserScope<Nick> nickScope = onlineUser.getUserScope(User.NICK_KEY);
            if (nickScope.hasValueLoaded()) {
                Nick nick = nickScope.getValueSyncSilent();

                if (nick.getName().equalsIgnoreCase(name)) {
                    return Collections.singletonList(onlineUser);
                }
            }
        }
        return Collections.emptyList();
    }

    private void openGui(Player player, String target) {
        Gui gui = new ReportGui(player, target, languagePack, repository);
        gui.open();
    }

    @Nullable
    @Override
    public List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (args.length == 1) {
            return Bukkit.getOnlinePlayers().stream()
                .map(HumanEntity::getName)
                .collect(Collectors.toList());
        }
        return null;
    }
}
