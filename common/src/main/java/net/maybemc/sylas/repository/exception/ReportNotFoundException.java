package net.maybemc.sylas.repository.exception;

/**
 * @author Nico_ND1
 */
public class ReportNotFoundException extends RuntimeException implements TranslatableExceptionMessage {
    @Override
    public String getLocaleKey() {
        return "exception-reportnotfound";
    }
}
