package net.maybemc.sylas.chat.log;

import com.google.common.collect.Maps;
import net.maybemc.sylas.schema.chatlog.ChatlogEntry;
import net.maybemc.sylas.schema.chatlog.ChatlogEntryFlag;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Nico_ND1
 */
public class ChatlogCollector implements Runnable {
    private final Map<UUID, List<ChatlogEntry>> entries = Maps.newConcurrentMap();
    private final Lock lock = new ReentrantLock();

    @Override
    public void run() {
        try {
            lock.lock();

            long time = TimeUnit.MINUTES.toMillis(15);
            for (Map.Entry<UUID, List<ChatlogEntry>> entry : entries.entrySet()) {
                entry.getValue().removeIf(e -> e.getTime() + time < System.currentTimeMillis());

                if (entry.getValue().isEmpty()) {
                    entries.remove(entry.getKey());
                }
            }
        } finally {
            lock.unlock();
        }
    }

    public void chat(UUID player, String message, ChatlogEntryFlag... entryFlags) {
        computeEntries(player).add(new ChatlogEntry(System.currentTimeMillis(), ChatlogEntry.Type.MESSAGE, player.toString(), message, entryFlags));
    }

    public void switchServer(UUID player, String server) {
        computeEntries(player).add(new ChatlogEntry(System.currentTimeMillis(), ChatlogEntry.Type.SERVER_SWITCH, player.toString(), server, new ChatlogEntryFlag[0]));
    }

    private List<ChatlogEntry> computeEntries(UUID player) {
        return entries.computeIfAbsent(player, uuid -> new ArrayList<>());
    }

    public List<ChatlogEntry> getEntries(UUID player) {
        List<ChatlogEntry> playerEntries = entries.get(player);
        if (playerEntries == null) {
            return null;
        }

        ChatlogEntryCollector collector = new ChatlogEntryCollector(playerEntries, entries);
        return collector.combineEntries();
    }
}
