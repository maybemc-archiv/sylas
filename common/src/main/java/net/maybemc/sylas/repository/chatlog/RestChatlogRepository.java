package net.maybemc.sylas.repository.chatlog;

import net.maybemc.sylas.repository.RestRepository;
import net.maybemc.sylas.repository.exception.ChatlogNoMessageException;
import net.maybemc.sylas.repository.exception.UnknownStatusException;
import org.jetbrains.annotations.NotNull;

import javax.ws.rs.core.Response;
import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class RestChatlogRepository extends RestRepository implements ChatlogRepository {
    @Override
    public CompletableFuture<String> createChatlog(@NotNull String target) {
        return CompletableFuture.supplyAsync(() -> {
            Response response = testResponse(createJsonTarget("chatlog")
                .header("target", target)
                .post(newEntity()));

            switch (response.getStatus()) {
                case 404:
                    throw new ChatlogNoMessageException();
                case 200:
                    return response.readEntity(String.class);
                default:
                    throw new UnknownStatusException(response);
            }
        }, asyncExecutor());
    }
}
