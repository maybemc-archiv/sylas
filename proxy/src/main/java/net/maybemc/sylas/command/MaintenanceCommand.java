package net.maybemc.sylas.command;

import net.maybemc.meybee.api.i18n.LanguagePack;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;

/**
 * @author Nico_ND1
 */
// TODO
public class MaintenanceCommand extends Command implements Runnable {
    private MaintenanceCountdown countdown;

    public MaintenanceCommand(LanguagePack languagePack) {
        super("maintenance", "sylas.command.maintenance", "wartung");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {

    }

    @Override
    public void run() {
        if (countdown == null || countdown.interrupted) {
            return;
        }

        if (countdown.secondsLeft == 0) {
            enableMaintenance(countdown.reason);
        } else if (countdown.secondsLeft % 60 == 0 || countdown.secondsLeft == 30 ||
            countdown.secondsLeft == 15 || countdown.secondsLeft <= 10) {
            broadcastCountdown(countdown.secondsLeft);
        }

        countdown.secondsLeft--;
    }

    private void enableMaintenance(String reason) {

    }

    private void broadcastCountdown(int secondsLeft) {

    }

    /*
    maintenance status
    maintenance start [TimeInMinutes] [Reason]
    maintenance countdown interrupt
    maintenance countdown continue
    maintenance stop
    maintenance confirm
     */

    public class MaintenanceCountdown {
        private int secondsLeft;
        private String reason;

        private boolean interrupted;
    }

}
