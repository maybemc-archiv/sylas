package net.maybemc.sylas.command;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.protocol.atarget.PacketTarget;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.sylas.chat.NameTranslator;
import net.maybemc.sylas.protocol.KickPacket;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Nico_ND1
 */
public class KickCommand extends Command {
    private final LanguagePack languagePack;
    private final PacketClient packetClient;

    public KickCommand(LanguagePack languagePack, PacketClient packetClient) {
        super("kick", "sylas.kick.command");
        this.languagePack = languagePack;
        this.packetClient = packetClient;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        UserRepository userRepository = Meybee.getInstance().getProvider(UserRepository.class);

        if (args.length >= 1) {
            String[] reasonArray = new String[args.length - 1];
            System.arraycopy(args, 1, reasonArray, 0, reasonArray.length);
            String reason = String.join(" ", reasonArray);
            if (reason.isEmpty()) {
                reason = "/";
            }

            AtomicReference<String> reasonReference = new AtomicReference<>(reason);
            Futures.addCallback(userRepository.findUsers(args[0]), new FutureCallback<List<User>>() {
                @Override
                public void onSuccess(List<User> users) {
                    if (users.isEmpty()) {
                        player.sendMessage(languagePack.translate(player.getUniqueId(), "kick-command-target-not-found"));
                    } else {
                        execute(player, users.get(0).getUniqueId(), reasonReference.get());
                    }
                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {
                    FutureCallback.super.onFailure(throwable);
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "kick-command-error", throwable.getMessage()));
                }
            });
        } else {
            player.sendMessage(languagePack.translate(player.getUniqueId(), "kick-command-usage"));
        }
    }

    private void execute(ProxiedPlayer player, UUID target, String reason) {
        Futures.addCallback(packetClient.sendPacket(new KickPacket(player.getUniqueId(), target, reason).target(PacketTarget.playerProxy(target)), Boolean.class), new FutureCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                if (!aBoolean) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "kick-command-target-not-found"));
                } else {
                    NameTranslator.sendNameMessage(player, languagePack, "kick-command-success", target, 0);
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                FutureCallback.super.onFailure(throwable);
                player.sendMessage(languagePack.translate(player.getUniqueId(), "kick-command-error", throwable.getMessage()));
            }
        });
    }
}
