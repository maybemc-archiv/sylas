package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.notify.Notifier;
import net.maybemc.sylas.protocol.BanCreatedPacket;
import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.repository.ban.RestBanRepository;
import net.maybemc.sylas.schema.ban.Ban;
import net.maybemc.sylas.schema.ban.BanType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class BanCreatedPacketListener implements PacketListener<BanCreatedPacket> {
    private final LoginListener loginListener;
    private final Notifier notifier;

    public BanCreatedPacketListener(LoginListener loginListener, Notifier notifier) {
        this.loginListener = loginListener;
        this.notifier = notifier;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection connection, BanCreatedPacket packet) {
        Ban ban = packet.getBan();

        notifier.notifyBanCreated(ban);

        BanRepository banRepository = Meybee.getInstance().getProvider(BanRepository.class);
        if (banRepository instanceof RestBanRepository restBanRepository) {
            restBanRepository.invalidateBanCache(ban.getTarget(), ban.getReason().getType());
        }

        if (ban.getReason().getType() == BanType.NETWORK) {
            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(ban.getTarget());
            if (player != null && player.isConnected()) {
                player.disconnect(loginListener.getLoginScreen(ban, player.getUniqueId()));
            }
            return instantResult(stopHandling());
        }
        return instantResult(stopHandling());
    }
}
