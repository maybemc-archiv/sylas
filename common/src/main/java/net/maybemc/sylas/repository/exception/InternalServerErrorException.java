package net.maybemc.sylas.repository.exception;

import lombok.AllArgsConstructor;
import net.maybemc.meybee.api.i18n.LanguagePack;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public class InternalServerErrorException extends RuntimeException implements TranslatableExceptionMessage {
    private final String error;

    @Nullable
    public String getError() {
        return error;
    }

    @Override
    public String getLocaleKey() {
        return "exception-internalservererror";
    }

    @Override
    public String translate(LanguagePack languagePack, UUID player) {
        return languagePack.translate(player, getLocaleKey(), error);
    }
}
