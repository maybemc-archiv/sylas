package net.maybemc.sylas.schema.report;

import lombok.Data;

/**
 * @author Nico_ND1
 */
@Data
public class ReportReason {
    private final String id;
    private final int inventorySlot;
    private final String inventoryIcon;
    private final String[] followingBanReasonIds;
}
