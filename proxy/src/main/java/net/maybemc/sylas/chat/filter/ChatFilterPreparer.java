package net.maybemc.sylas.chat.filter;

import java.util.Map;

/**
 * @author Nico_ND1
 */
public class ChatFilterPreparer {
    private final Map<Character, Character> replacements;

    public ChatFilterPreparer(Map<Character, Character> replacements) {
        this.replacements = replacements;
    }

    public String prepare(String input) {
        char[] chars = input.toLowerCase().toCharArray();
        char[] charsCopy = new char[chars.length];
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            Character replacement = replacements.get(c);

            if (replacement != null) {
                charsCopy[i] = replacement;
            } else {
                charsCopy[i] = c;
            }
        }

        return new String(charsCopy);
    }
}
