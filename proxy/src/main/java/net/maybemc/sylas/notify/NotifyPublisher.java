package net.maybemc.sylas.notify;

import net.maybemc.meybee.api.user.scope.UserScope;
import net.maybemc.meybee.api.user.scope.publisher.DatabasePublisher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nico_ND1
 */
public class NotifyPublisher extends DatabasePublisher<NotifyEntry> {
    public NotifyPublisher() {
        super("sylas");
    }

    @Override
    public void publish_(Connection connection, UserScope<NotifyEntry> userScope, NotifyEntry notifyEntry) throws SQLException {
        try {
            try (PreparedStatement update = connection.prepareStatement("UPDATE notify SET value=? WHERE player=? AND type=?;");
                 PreparedStatement insert = connection.prepareStatement("INSERT INTO notify(player, type, value) VALUES (?, ?, ?);")) {
                for (Map.Entry<NotifyType, Boolean> entry : notifyEntry.getDifferentValues().entrySet()) {
                    update.setBoolean(1, entry.getValue());
                    update.setString(2, userScope.getUser().getUniqueId().toString());
                    update.setInt(3, entry.getKey().ordinal());

                    if (update.executeUpdate() == 0) {
                        insert.setString(1, userScope.getUser().getUniqueId().toString());
                        insert.setInt(2, entry.getKey().ordinal());
                        insert.setBoolean(3, entry.getValue());
                        insert.executeUpdate();
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public NotifyEntry load_(Connection connection, UserScope<NotifyEntry> userScope) throws SQLException {
        try (PreparedStatement select = connection.prepareStatement("SELECT type, value FROM notify WHERE player=?;")) {
            select.setString(1, userScope.getUser().getUniqueId().toString());

            Map<NotifyType, Boolean> values = new HashMap<>();
            ResultSet resultSet = select.executeQuery();
            while (resultSet.next()) {
                values.put(NotifyType.values()[resultSet.getInt("type")], resultSet.getBoolean("value"));
            }
            return new NotifyEntry(values);
        }
    }
}
