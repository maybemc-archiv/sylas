package net.maybemc.sylas.repository.user;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public interface UserRepository {

    void login(
        @NotNull UUID player,
        @NotNull String ip
    );

    CompletableFuture<Void> kick(
        @NotNull UUID player,
        @NotNull String reason
    );

    CompletableFuture<Integer> getBanPoints(
        @NotNull UUID player
    );

}
