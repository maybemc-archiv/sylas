package net.maybemc.sylas.command;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.sylas.chat.NameTranslator;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class FindCommand extends Command implements TabExecutor {
    private final LanguagePack languagePack;
    private final IPlayerManager playerManager;

    public FindCommand(LanguagePack languagePack) {
        super("find", "sylas.find.command");
        this.languagePack = languagePack;
        this.playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) commandSender;
        if (args.length == 1) {
            playerManager.getFirstOnlinePlayerAsync(args[0]).onComplete(cloudPlayer -> {
                if (cloudPlayer == null) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "find-command-player-not-found"));
                } else {
                    NameTranslator.sendNameMessage(player, languagePack, "find-command-success", cloudPlayer.getUniqueId(), 0, null, cloudPlayer.getConnectedService().getServerName());
                }
            }).onFailure(throwable -> player.sendMessage(languagePack.translate(player.getUniqueId(), "find-command-error", throwable.getMessage())));
        } else {
            player.sendMessage(languagePack.translate(player.getUniqueId(), "find-command-usage"));
        }
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length == 1) {
            try {
                List<String> names = new ArrayList<>(playerManager.onlinePlayers().asNamesAsync().get(1L, TimeUnit.SECONDS));
                return names.stream()
                    .sorted(String::compareTo)
                    .filter(name -> name.toLowerCase().startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                e.printStackTrace();
            }
        }
        return Collections.emptyList();
    }
}
