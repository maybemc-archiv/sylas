package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class JumpToPacket extends Packet {
    private UUID player;
    private UUID pTarget;

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        writeUUID(byteBuf, player);
        writeUUID(byteBuf, pTarget);
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
        player = readUUID(byteBuf);
        pTarget = readUUID(byteBuf);
    }
}
