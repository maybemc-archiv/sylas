package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class SpectatePacket extends Packet {
    private UUID spectator;
    private UUID target_;

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        writeUUID(byteBuf, spectator);
        writeUUID(byteBuf, target_);
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
        spectator = readUUID(byteBuf);
        target_ = readUUID(byteBuf);
    }
}

/*
cloud:
    ReportSelectPacket
        send prepare spectator packet to target server and await response
        response -> send to server
    ReportJudgePacket
        send report stop packet to target

proxy:
    Command /follow <Name>
        send prepare spectator packet to target server and then jumpto packet to server

spigot:
    ReportStopPacket
        if target is the one of spectator's session then send him to lobby
    SPECTATOR SESSION TARGET QUITS
        send message with click for /follow <Name>
 */
