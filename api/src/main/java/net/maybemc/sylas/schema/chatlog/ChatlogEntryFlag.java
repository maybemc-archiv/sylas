package net.maybemc.sylas.schema.chatlog;

import lombok.Data;

/**
 * @author Nico_ND1
 */
@Data
public class ChatlogEntryFlag {
    private final int startIndex;
    private final int endIndex;
    private final Type type;

    public enum Type {
        CHATFILTER,
        SPAM,
        UNKNOWN;
    }
}
