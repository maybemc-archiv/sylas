package net.maybemc.sylas.command;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.meybee.api.user.scope.UserScope;
import net.maybemc.meybee.api.user.scope.publisher.PublisherType;
import net.maybemc.sylas.notify.Notifier;
import net.maybemc.sylas.notify.NotifyEntry;
import net.maybemc.sylas.notify.NotifyType;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.jetbrains.annotations.NotNull;

import java.util.logging.Level;

/**
 * @author Nico_ND1
 */
public class NotifyCommand extends Command {
    private final LanguagePack languagePack;
    private final UserRepository userRepository;

    public NotifyCommand(LanguagePack languagePack) {
        super("notify", "sylas.notify.command");
        this.languagePack = languagePack;
        this.userRepository = Meybee.getInstance().getProvider(UserRepository.class);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) commandSender;
        User user = userRepository.getUser(player.getUniqueId());
        UserScope<NotifyEntry> userScope = user.getUserScope(Notifier.NOTIFY_SCOPE_KEY);

        Futures.addCallback(userScope.loadValue(), new FutureCallback<NotifyEntry>() {
            @Override
            public void onSuccess(NotifyEntry notifyEntry) {
                if (args.length == 1) {
                    NotifyType notifyType;
                    try {
                        notifyType = NotifyType.valueOf(args[0].toUpperCase());
                        boolean nextValue = !notifyEntry.getValue(notifyType);
                        notifyEntry.setValue(notifyType, nextValue);

                        userScope.setValue(notifyEntry);
                        userScope.publish(PublisherType.DATABASE);
                    } catch (IllegalArgumentException ignored) {
                        player.sendMessage(languagePack.translate(player.getUniqueId(), "notify-command-invalid-type"));
                        return;
                    }
                }

                ComponentBuilder builder = new ComponentBuilder().append(TextComponent.fromLegacyText(languagePack.translate(player.getUniqueId(), "notify-command"))).append("\n ");
                for (NotifyType value : NotifyType.values()) {
                    builder.append("[" + languagePack.translate(player.getUniqueId(), "notify-command-" + value.name()) + "]")
                        .color(notifyEntry.getValue(value) ? ChatColor.GREEN : ChatColor.RED)
                        .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/notify " + value.name()))
                        .append("\n");
                }

                player.sendMessage(builder.create());
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                Meybee.getLogger().log(Level.WARNING, "Error setting notify value of " + player.getUniqueId(), throwable);
                player.sendMessage(languagePack.translate(player.getUniqueId(), "notify-command-error", throwable.getMessage()));
            }
        });
    }
}
