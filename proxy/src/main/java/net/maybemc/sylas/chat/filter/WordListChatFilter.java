package net.maybemc.sylas.chat.filter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * @author Nico_ND1
 */
public class WordListChatFilter extends SimpleRegexChatFilter {
    private final String listName;
    private final String punishReasonId;
    private final String reportReasonId;
    private final boolean cancelSend;

    public WordListChatFilter(JsonObject configuration, JsonDeserializationContext context) {
        super(new ArrayList<>());
        this.listName = configuration.get("listName").getAsString();

        if (configuration.has("punishReasonId")) {
            punishReasonId = configuration.get("punishReasonId").getAsString();
        } else {
            punishReasonId = null;
        }

        if (configuration.has("reportReasonId")) {
            reportReasonId = configuration.get("reportReasonId").getAsString();
        } else {
            reportReasonId = null;
        }

        if (configuration.has("cancelSend")) {
            cancelSend = configuration.get("cancelSend").getAsBoolean();
        } else {
            cancelSend = true;
        }

        readList();
    }

    private void readList() {
        File file = new File("meybee/config/sylas/chatfilter/" + listName + ".list");

        List<String> rawWords = new ArrayList<>();
        try (Scanner scanner = new Scanner(file, "UTF-8")) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line != null && !line.isEmpty()) {
                    rawWords.add(line.toLowerCase());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        createFilterWord(rawWords);
        System.out.println("we have " + rawWords.size() + " raw words and " + words.size() + " words");
    }

    private void createFilterWord(List<String> rawWords) {
        Pattern[] patterns = rawWords.stream()
            .map(Pattern::compile)
            .toArray(Pattern[]::new);
        words.add(new ChatFilterWord(listName, patterns, punishReasonId, reportReasonId, true, cancelSend));
    }
}
