package net.maybemc.sylas;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.protocol.PacketRegistry;
import net.maybemc.meybee.api.protocol.server.PacketServer;
import net.maybemc.sylas.protocol.BanBypassDetectedPacket;
import net.maybemc.sylas.protocol.BanCreatedPacket;
import net.maybemc.sylas.protocol.BroadcastMessagePacket;
import net.maybemc.sylas.protocol.ChatClearPacket;
import net.maybemc.sylas.protocol.ChatClearPacketListener;
import net.maybemc.sylas.protocol.ChatlogRequestListener;
import net.maybemc.sylas.protocol.ChatlogRequestPacket;
import net.maybemc.sylas.protocol.ConnectionNotifyPacket;
import net.maybemc.sylas.protocol.JumpToPacket;
import net.maybemc.sylas.protocol.JumpToPacketListener;
import net.maybemc.sylas.protocol.KickInfoPacket;
import net.maybemc.sylas.protocol.OpenReportsPacket;
import net.maybemc.sylas.protocol.PardonCreatedPacket;
import net.maybemc.sylas.protocol.RedirectionPacketListener;
import net.maybemc.sylas.protocol.ReportCreatedPacket;
import net.maybemc.sylas.protocol.ReportJudgedPacket;
import net.maybemc.sylas.protocol.ReportSelectedPacket;
import net.maybemc.sylas.protocol.RequestOnlinePlayersPacket;
import net.maybemc.sylas.protocol.RequestOnlinePlayersPacketListener;
import net.maybemc.sylas.protocol.SendExploitNotifyPacket;
import net.maybemc.sylas.protocol.SylasPacketRegister;
import net.maybemc.sylas.protocol.TeamChatMessagePacket;
import net.maybemc.sylas.protocol.TeamMembersRequestPacket;
import net.maybemc.sylas.protocol.TeamMembersRequestPacketListener;
import net.maybemc.sylas.repository.RestClientProvider;
import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.repository.ban.RestBanRepository;
import net.maybemc.sylas.repository.report.ReportRepository;
import net.maybemc.sylas.repository.report.RestReportRepository;
import net.maybemc.sylas.repository.user.RestUserRepository;
import net.maybemc.sylas.repository.user.UserRepository;

import javax.ws.rs.client.Client;

/**
 * @author Nico_ND1
 */
public class ModuleLoader {
    public static void init() {
        Meybee.getLogger().info("Binding sylas with meybee");

        Meybee.getInstance().registerProviderLoader(Client.class, new RestClientProvider());
        Meybee.getInstance().registerProvider(BanRepository.class, new RestBanRepository());
        Meybee.getInstance().registerProvider(ReportRepository.class, new RestReportRepository());
        Meybee.getInstance().registerProvider(UserRepository.class, new RestUserRepository());

        PacketServer server = Meybee.getInstance().getProvider(PacketServer.class, PacketRegistry.DEFAULT_NAME);
        SylasPacketRegister.register(server.getPacketRegistry());
        server.getPacketRegistry().registerListener(ChatlogRequestPacket.class, new ChatlogRequestListener(server), 0);
        server.getPacketRegistry().registerListener(BanCreatedPacket.class, new RedirectionPacketListener<>(server, "proxy"), 0);
        server.getPacketRegistry().registerListener(TeamMembersRequestPacket.class, new TeamMembersRequestPacketListener(server), 0);
        server.getPacketRegistry().registerListener(JumpToPacket.class, new JumpToPacketListener(), 0);
        server.getPacketRegistry().registerListener(PardonCreatedPacket.class, new RedirectionPacketListener<>(server, "proxy"), 0);
        server.getPacketRegistry().registerListener(KickInfoPacket.class, new RedirectionPacketListener<>(server, "proxy"), 0);
        server.getPacketRegistry().registerListener(BroadcastMessagePacket.class, new RedirectionPacketListener<>(server, "proxy"), 0);
        server.getPacketRegistry().registerListener(TeamChatMessagePacket.class, new RedirectionPacketListener<>(server, "proxy"), 0);
        server.getPacketRegistry().registerListener(ConnectionNotifyPacket.class, new RedirectionPacketListener<>(server, "proxy"), 0);
        server.getPacketRegistry().registerListener(OpenReportsPacket.class, new RedirectionPacketListener<>(server, "proxy"), 0);
        server.getPacketRegistry().registerListener(ChatClearPacket.class, new ChatClearPacketListener(server), 0);
        server.getPacketRegistry().registerListener(ReportSelectedPacket.class, new RedirectionPacketListener<>(server, "proxy"), 0);
        server.getPacketRegistry().registerListener(ReportJudgedPacket.class, new RedirectionPacketListener<>(server, "proxy"), 0);
        server.getPacketRegistry().registerListener(ReportCreatedPacket.class, new RedirectionPacketListener<>(server, "proxy"), 0);
        server.getPacketRegistry().registerListener(RequestOnlinePlayersPacket.class, new RequestOnlinePlayersPacketListener(), 0);
        server.getPacketRegistry().registerListener(BanBypassDetectedPacket.class, new RedirectionPacketListener<>(server, "proxy"), 0);
        server.getPacketRegistry().registerListener(SendExploitNotifyPacket.class, new RedirectionPacketListener<>(server, "proxy"), 0);
    }
}
