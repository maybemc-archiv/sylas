package net.maybemc.sylas.schema;

import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Nico_ND1
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pagination<T> {
    @Expose
    private int maxPages;
    @Expose
    private List<T> items;
}
