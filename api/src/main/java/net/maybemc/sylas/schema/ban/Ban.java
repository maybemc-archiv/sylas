package net.maybemc.sylas.schema.ban;

import lombok.Data;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
@Data
public class Ban {
    private final String id;
    private final long time;
    private final long originalDuration;
    private final long duration;
    private final String creator;
    private final UUID target;
    private final BanReason reason;
    private final String appendant;
    private final int pointsEntryId;
    private Pardon activePardon;
    private Supervision supervision;

    public boolean isActive() {
        return isEndless() || time + duration > System.currentTimeMillis();
    }

    public boolean isEndless() {
        return duration == -1L;
    }

    public long getEndTime() {
        if (isEndless()) {
            return -1L;
        }
        return System.currentTimeMillis() + duration;
    }

    public long getRemainingTime() {
        return time + duration - System.currentTimeMillis();
    }
}
