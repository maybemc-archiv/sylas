package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;
import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.schema.ban.Ban;
import net.maybemc.sylas.schema.ban.BanReason;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
@Getter
public class BanCreatedPacket extends Packet {
    protected final BanRepository banRepository;
    protected Ban ban;

    public BanCreatedPacket(BanRepository banRepository, Ban ban) {
        this.banRepository = banRepository;
        this.ban = ban;
    }

    public BanCreatedPacket(BanRepository banRepository) {
        this.banRepository = banRepository;
    }

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        writeString(byteBuf, ban.getId());
        byteBuf.writeLong(ban.getTime());
        byteBuf.writeLong(ban.getDuration());
        writeString(byteBuf, ban.getCreator());
        writeUUID(byteBuf, ban.getTarget());
        writeString(byteBuf, ban.getReason().getId());
        writeString(byteBuf, ban.getAppendant());
        byteBuf.writeInt(ban.getPointsEntryId());
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
        String id = readString(byteBuf);
        long time = byteBuf.readLong();
        long duration = byteBuf.readLong();
        String creator = readString(byteBuf);
        UUID target = readUUID(byteBuf);
        String reasonId = readString(byteBuf);
        BanReason reason = banRepository.findBanReason(reasonId).orElseThrow(() -> new PacketReadWriteException("reason not found: " + reasonId));
        String appendant = readString(byteBuf);
        int pointsEntryId = byteBuf.readInt();

        this.ban = new Ban(id, time, duration, duration, creator, target, reason, appendant, pointsEntryId);
    }
}
