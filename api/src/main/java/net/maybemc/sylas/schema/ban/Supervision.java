package net.maybemc.sylas.schema.ban;

import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Supervision {
    @Expose
    private UUID supervisorUniqueId;
    @Expose
    private long time;
}
