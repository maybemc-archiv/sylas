package net.maybemc.sylas.protocol;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import de.dytanic.cloudnet.ext.bridge.player.NetworkServiceInfo;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class JumpToPacketListener implements PacketListener<JumpToPacket> {
    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, JumpToPacket packet) {
        IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
        ICloudPlayer player = playerManager.getOnlinePlayer(packet.getPlayer());
        ICloudPlayer target = playerManager.getOnlinePlayer(packet.getPTarget());
        if (player == null || target == null) {
            return instantResult(stopHandling(false));
        }

        NetworkServiceInfo serviceInfo = target.getConnectedService();
        if (serviceInfo == null) {
            return instantResult(stopHandling(false));
        }

        player.getPlayerExecutor().connect(serviceInfo.getServerName());
        return instantResult(stopHandling(true));
    }
}
