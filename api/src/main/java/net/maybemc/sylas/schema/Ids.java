package net.maybemc.sylas.schema;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

/**
 * @author Nico_ND1
 */
@RequiredArgsConstructor
@Getter
public enum Ids {
    REPORT("r-[a-zA-Z\\d]{8}"),
    BAN("b-[a-zA-Z\\d]{8}"),
    CHATLOG("c-[a-zA-Z\\d]{8}");

    private final String regex;

    public boolean matches(@NotNull String id) {
        return id.matches(regex);
    }

    public static Ids findById(@NotNull String id) {
        for (Ids value : Ids.values()) {
            if (value.matches(id)) {
                return value;
            }
        }
        return null;
    }
}
