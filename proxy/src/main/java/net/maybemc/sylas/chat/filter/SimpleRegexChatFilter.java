package net.maybemc.sylas.chat.filter;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.maybemc.sylas.schema.chatlog.ChatlogEntryFlag;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Nico_ND1
 */
public class SimpleRegexChatFilter implements ChatFilter {
    protected final List<ChatFilterWord> words;
    // TODO: keep track of stats of every word and sort it descending by occurrence

    public SimpleRegexChatFilter(List<ChatFilterWord> words) {
        this.words = words;
    }

    public SimpleRegexChatFilter(JsonObject configuration, JsonDeserializationContext context) {
        this(new ArrayList<>());

        for (JsonElement wordElement : configuration.getAsJsonArray("words")) {
            if (wordElement.isJsonPrimitive()) {
                String word = wordElement.getAsString();
                words.add(new ChatFilterWord(
                    word,
                    new Pattern[] {Pattern.compile(word)},
                    null,
                    null,
                    true,
                    true
                ));
            } else {
                JsonObject wordObject = wordElement.getAsJsonObject();

                String word = wordObject.get("word").getAsString();
                Pattern[] patterns;

                if (wordObject.has("otherVariants")) {
                    JsonArray otherVariantsArray = wordObject.getAsJsonArray("otherVariants");
                    patterns = new Pattern[otherVariantsArray.size() + 1];

                    int i = 0;
                    patterns[i++] = Pattern.compile(word);
                    for (JsonElement variantElement : otherVariantsArray) {
                        patterns[i++] = Pattern.compile(variantElement.getAsString());
                    }
                } else {
                    patterns = new Pattern[] {Pattern.compile(word)};
                }

                String punishReasonId = null;
                if (wordObject.has("punishReasonId")) {
                    punishReasonId = wordObject.get("punishReasonId").getAsString();
                }

                String reportReasonId = null;
                if (wordObject.has("reportReasonId")) {
                    reportReasonId = wordObject.get("reportReasonId").getAsString();
                }

                boolean singleWord = true;
                if (wordObject.has("singleWord")) {
                    singleWord = wordObject.get("singleWord").getAsBoolean();
                }

                boolean cancelSend = true;
                if (wordObject.has("cancelSend")) {
                    cancelSend = wordObject.get("cancelSend").getAsBoolean();
                }

                words.add(new ChatFilterWord(word, patterns, punishReasonId, reportReasonId, singleWord, cancelSend));
            }
        }
    }

    @Override
    public ChatFilterResult testMessage(ProxiedPlayer player, String message) {
        List<ChatlogEntryFlag> flags = new ArrayList<>();
        List<ChatFilterWord> detectedWords = new ArrayList<>();

        String[] messageWords = message.split(" ");
        System.out.println("test message " + message + " words length is " + messageWords.length);
        for (ChatFilterWord word : words) {
            System.out.println("we have word " + word.getWord() + " : " + word.getOtherVariants().length);
            for (Pattern otherVariant : word.getOtherVariants()) {
                if (word.isSingleWord()) {
                    for (String words : messageWords) {
                        test(words, otherVariant, word, flags, detectedWords);
                    }
                } else {
                    test(message, otherVariant, word, flags, detectedWords);
                }
            }
        }

        return new ChatFilterResult(flags, detectedWords);
    }

    private void test(String input, Pattern pattern, ChatFilterWord word, List<ChatlogEntryFlag> flags, List<ChatFilterWord> detectedWords) {
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();

            flags.add(new ChatlogEntryFlag(start, end, ChatlogEntryFlag.Type.CHATFILTER));
            detectedWords.add(word);
        }
    }
}
