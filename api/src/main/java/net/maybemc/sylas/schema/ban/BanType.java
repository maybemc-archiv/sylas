package net.maybemc.sylas.schema.ban;

/**
 * @author Nico_ND1
 */
public enum BanType {
    MUTE,
    NETWORK,
    GAME_BEDWARS,
    GAME_GUNGAME,
    GAME_TTT
}
