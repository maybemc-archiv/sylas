package net.maybemc.sylas.chat.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.maybemc.sylas.schema.chatlog.ChatlogEntryFlag;

import java.util.List;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@Getter
public class ChatFilterResult {
    private final List<ChatlogEntryFlag> flags;
    private final List<ChatFilterWord> detectedWords;

    public ChatlogEntryFlag.Type getFlagType() {
        for (ChatlogEntryFlag flag : flags) {
            return flag.getType();
        }
        return null;
    }

    public boolean isBlocked() {
        return detectedWords.stream().anyMatch(ChatFilterWord::isCancelSend) ||
            flags.stream().anyMatch(chatlogEntryFlag -> chatlogEntryFlag.getType() == ChatlogEntryFlag.Type.SPAM);
    }
}
