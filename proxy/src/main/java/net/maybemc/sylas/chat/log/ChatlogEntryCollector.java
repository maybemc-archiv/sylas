package net.maybemc.sylas.chat.log;

import lombok.Data;
import net.maybemc.sylas.schema.chatlog.ChatlogEntry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
public class ChatlogEntryCollector {
    private final List<ChatlogEntry> playerEntries;
    private final Map<UUID, List<ChatlogEntry>> totalEntries;
    private final List<ServerSession> sessions;

    public ChatlogEntryCollector(List<ChatlogEntry> playerEntries, Map<UUID, List<ChatlogEntry>> totalEntries) {
        this.playerEntries = playerEntries;
        this.totalEntries = totalEntries;
        this.sessions = new ArrayList<>();

        calculateSessions();
    }

    private void calculateSessions() {
        Map<String, Long> startedSession = new HashMap<>();
        for (ChatlogEntry playerEntry : playerEntries) {
            if (playerEntry.getType() == ChatlogEntry.Type.SERVER_SWITCH) {
                String serverName = playerEntry.getValue();
                if (startedSession.containsKey(serverName)) {
                    sessions.add(new ServerSession(serverName, startedSession.remove(serverName), playerEntry.getTime()));
                } else {
                    startedSession.put(serverName, playerEntry.getTime());
                }
            }
        }

        if (!startedSession.isEmpty()) {
            for (String key : startedSession.keySet()) {
                Long time = startedSession.get(key);
                sessions.add(new ServerSession(key, time, -1L));
            }
        }
    }

    public List<ChatlogEntry> combineEntries() {
        List<ChatlogEntry> entries = new ArrayList<>();
        for (UUID player : totalEntries.keySet()) {
            List<ChatlogEntry> playerEntries = totalEntries.get(player);

            String server = null;
            for (ChatlogEntry playerEntry : playerEntries) {
                if (playerEntry.getType() == ChatlogEntry.Type.SERVER_SWITCH) {
                    if (server == null) {
                        server = playerEntry.getValue();
                    } else {
                        if (playerEntry.getValue().equals(server)) {
                            server = null;
                        } else {
                            server = playerEntry.getValue();
                        }
                    }
                } else if (playerEntry.getType() == ChatlogEntry.Type.MESSAGE) {
                    if (wasOnServer(server, playerEntry.getTime())) {
                        entries.add(playerEntry.cleanUp());
                    }
                }
            }
        }
        return entries;
    }

    private boolean wasOnServer(String serverName, long time) {
        for (ServerSession session : sessions) {
            if (session.serverName.equals(serverName)) {
                if (session.joinTime < time && (session.quitTime == -1L || session.quitTime > time)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Data
    public static class ServerSession {
        private final String serverName;
        private final long joinTime;
        private final long quitTime;
    }
}
