package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.scope.UserScope;
import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.schema.ban.Ban;

import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @author Nico_ND1
 */
public abstract class BanListener {
    protected final LanguagePack languagePack;
    protected final BanRepository repository;

    public BanListener(LanguagePack languagePack) {
        this.languagePack = languagePack;
        this.repository = Meybee.getInstance().getProvider(BanRepository.class);
    }

    protected Locale getLocale(UUID uuid) {
        net.maybemc.meybee.api.user.UserRepository userRepository = Meybee.getInstance().getProvider(net.maybemc.meybee.api.user.UserRepository.class);
        User user = userRepository.getOfflineUser(uuid);
        UserScope<Locale> languageScope = user.getUserScope(User.LANGUAGE_KEY);

        try {
            return languageScope.loadValue().get(2L, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            return Locale.getDefault();
        }
    }

    protected String formatDuration(UUID context, Ban duration) {
        Locale locale = getLocale(context);
        if (duration.isEndless()) {
            return languagePack.translate(locale, "login-banned-duration-endless");
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(duration.getRemainingTime());

        int months = calendar.get(Calendar.MONTH);
        int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR) - 1;
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        StringBuilder unitBuilder = new StringBuilder();
        if (months > 0) {
            formatTimeUnit(locale, unitBuilder, months, "month");
        } else if (dayOfYear > 0) {
            formatTimeUnit(locale, unitBuilder, dayOfYear, "day");
        } else if (hourOfDay > 0) {
            formatTimeUnit(locale, unitBuilder, hourOfDay, "hour");
        } else {
            formatTimeUnit(locale, unitBuilder, minute, "minute");
            formatTimeUnit(locale, unitBuilder, second, "second");
        }
        return unitBuilder.toString();
    }

    private void formatTimeUnit(Locale locale, StringBuilder unitBuilder, int value, String unitPrefix) {
        if (value == 0) {
            return;
        }

        if (value == 1) {
            unitBuilder.append(languagePack.translate(locale, "login-banned-duration-" + unitPrefix, value));
        } else {
            unitBuilder.append(languagePack.translate(locale, "login-banned-duration-" + unitPrefix + "s", value));
        }
        unitBuilder.append(" ");
    }
}
