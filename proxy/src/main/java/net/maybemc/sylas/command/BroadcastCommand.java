package net.maybemc.sylas.command;

import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.sylas.protocol.BroadcastMessagePacket;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class BroadcastCommand extends Command {
    private final LanguagePack languagePack;
    private final PacketClient packetClient;
    private long nextUse;

    public BroadcastCommand(LanguagePack languagePack, PacketClient packetClient) {
        super("broadcast", "sylas.broadcast.command");
        this.languagePack = languagePack;
        this.packetClient = packetClient;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        if (args.length == 0) {
            player.sendMessage(languagePack.translate(player.getUniqueId(), "broadcast-command-usage"));
            return;
        }

        if (nextUse > System.currentTimeMillis()) {
            player.sendMessage(languagePack.translate(player.getUniqueId(), "broadcast-command-cooldown"));
            return;
        }

        String message = String.join(" ", args);
        player.sendMessage(languagePack.translate(player.getUniqueId(), "broadcast-command-success"));
        packetClient.sendPacket(new BroadcastMessagePacket(message));
        nextUse = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(30);
    }
}
