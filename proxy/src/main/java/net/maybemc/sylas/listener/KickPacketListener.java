package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.protocol.KickInfoPacket;
import net.maybemc.sylas.protocol.KickPacket;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.concurrent.CompletableFuture;

/**
 * @author Nico_ND1
 */
public class KickPacketListener implements PacketListener<KickPacket> {
    private final PacketClient packetClient;
    private final LanguagePack languagePack;

    public KickPacketListener(PacketClient packetClient, LanguagePack languagePack) {
        this.packetClient = packetClient;
        this.languagePack = languagePack;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection packetServerConnection, KickPacket kickPacket) {
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(kickPacket.getPTarget());
        if (player != null && player.isConnected()) {
            player.disconnect(languagePack.translate(player.getUniqueId(), "kick-kicked", ChatColor.translateAlternateColorCodes('&', kickPacket.getReason())));
            packetClient.sendPacket(new KickInfoPacket(kickPacket.getIssuer(), kickPacket.getPTarget(), kickPacket.getReason()));
            return instantResult(stopHandling(true));
        }
        return instantResult(stopHandling(false));
    }
}
