package net.maybemc.sylas.repository;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.sylas.repository.exception.InternalServerErrorException;
import net.maybemc.sylas.repository.exception.InvalidParametersException;
import net.maybemc.sylas.repository.exception.TimeoutException;
import org.glassfish.jersey.client.ClientProperties;
import org.jetbrains.annotations.NotNull;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;

import static javax.ws.rs.client.Invocation.Builder;

/**
 * @author Nico_ND1
 */
public abstract class RestRepository {
    private static final ExecutorService ASYNC_EXECUTOR = Executors.newFixedThreadPool(10, new ThreadFactoryBuilder().setNameFormat("RestRepository-%s")
        .setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                Meybee.getLogger().log(Level.WARNING, "Thread " + t.getName() + " occurred an error", e);
            }
        }).build());

    @NotNull
    public Client client() {
        return ClientBuilder.newClient()
            .register(new GsonRestHelper())
            .property(ClientProperties.READ_TIMEOUT, 5000L)
            .property(ClientProperties.CONNECT_TIMEOUT, 5000L);
        //return Meybee.getInstance().getProvider(Client.class);
    }

    @NotNull
    public String url() {
        return "http://94.130.142.199:8080"; // TODO: config
    }

    @NotNull
    public WebTarget createTarget(String path) {
        return client().target(url()).path(path);
    }

    @NotNull
    public Builder createJsonTarget(String path) {
        return client().target(url()).path(path).request(MediaType.APPLICATION_JSON_TYPE);
    }

    @NotNull
    public Builder requestJson(WebTarget target) {
        return target.request(MediaType.APPLICATION_JSON_TYPE);
    }

    @NotNull
    public ExecutorService asyncExecutor() {
        return ASYNC_EXECUTOR;
    }

    public Response testResponse(Response response, int... expectableStatuses) {
        for (int expectableStatus : expectableStatuses) {
            if (response.getStatus() == expectableStatus) {
                return response;
            }
        }

        switch (response.getStatus()) {
            case 408:
                throw new TimeoutException();
            case 406:
                throw new InvalidParametersException(response.readEntity(String.class));
            case 500:
                String error = null;
                if (response.hasEntity()) {
                    try {
                        error = response.readEntity(String.class);
                    } catch (Exception ignored) {
                    }
                }
                throw new InternalServerErrorException(error);
        }
        return response;
    }

    public Entity<Form> newEntity() {
        return Entity.json(new Form());
    }

}
