package net.maybemc.sylas.command;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.protocol.atarget.PacketTarget;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.sylas.chat.NameTranslator;
import net.maybemc.sylas.protocol.PingRequestPacket;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.util.List;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
public class PingCommand extends Command {
    private static final Color DARK_GREEN = ChatColor.DARK_GREEN.getColor();
    private static final Color GREEN = ChatColor.GREEN.getColor();
    private static final Color YELLOW = ChatColor.YELLOW.getColor();
    private static final Color RED = ChatColor.RED.getColor();

    private final LanguagePack languagePack;
    private final PacketClient packetClient;

    public PingCommand(LanguagePack languagePack, PacketClient packetClient) {
        super("ping");
        this.languagePack = languagePack;
        this.packetClient = packetClient;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;
        UserRepository userRepository = Meybee.getInstance().getProvider(UserRepository.class);

        if (args.length == 1) {
            Futures.addCallback(userRepository.findUsers(args[0]), new FutureCallback<List<User>>() {
                @Override
                public void onSuccess(List<User> users) {
                    if (users.isEmpty()) {
                        player.sendMessage(languagePack.translate(player.getUniqueId(), "ping-command-target-not-found"));
                    } else {
                        execute(player, users.get(0).getUniqueId());
                    }
                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {
                    FutureCallback.super.onFailure(throwable);
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "ping-command-error", throwable.getMessage()));
                }
            });
        } else {
            execute(player, player.getUniqueId());
        }
    }

    private void execute(ProxiedPlayer player, UUID target) {
        Futures.addCallback(packetClient.sendPacket(new PingRequestPacket(target).target(PacketTarget.playerProxy(target)), Integer.class), new FutureCallback<>() {
            @Override
            public void onSuccess(Integer ping) {
                ChatColor color = colorString(ping, 30, 100, 200);
                if (target.equals(player.getUniqueId())) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "ping-command-success-self", color.toString() + ping));
                } else {
                    NameTranslator.sendNameMessage(player, languagePack, "ping-command-success-other", target, 1, color.toString() + ping, null);
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                FutureCallback.super.onFailure(throwable);
                player.sendMessage(languagePack.translate(player.getUniqueId(), "ping-command-error", throwable.getMessage()));
            }
        });
    }

    private ChatColor colorString(int actual, int greenThreshold, int yellowThreshold, int redThreshold) {
        if (actual < greenThreshold) {
            return colorTransition(actual, greenThreshold, DARK_GREEN, GREEN);
        } else if (actual < yellowThreshold) {
            return colorTransition(actual, yellowThreshold, GREEN, YELLOW);
        } else if (actual < redThreshold) {
            return colorTransition(actual, redThreshold, YELLOW, RED);
        } else {
            return ChatColor.RED;
        }
    }

    private ChatColor colorTransition(int actual, int higherBound, Color lowerColor, Color higherColor) {
        double percentage = (double) actual / (double) (higherBound - actual);
        return ChatColor.of(new Color(
            (int) (lowerColor.getRed() + percentage * (higherColor.getRed() - lowerColor.getRed())),
            (int) (lowerColor.getGreen() + percentage * (higherColor.getGreen() - lowerColor.getGreen())),
            (int) (lowerColor.getBlue() + percentage * (higherColor.getBlue() - lowerColor.getBlue()))
        ));
    }
}
