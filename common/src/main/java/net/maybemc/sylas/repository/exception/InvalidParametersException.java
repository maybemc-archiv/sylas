package net.maybemc.sylas.repository.exception;

import lombok.Getter;
import net.maybemc.meybee.api.i18n.LanguagePack;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
@Getter
public class InvalidParametersException extends RuntimeException implements TranslatableExceptionMessage {
    private final String parameter;

    public InvalidParametersException(String message) {
        super(message);
        this.parameter = message;
    }

    @Override
    public String getLocaleKey() {
        return "exception-invalidparameters";
    }

    @Override
    public String translate(LanguagePack languagePack, UUID player) {
        return languagePack.translate(player, getLocaleKey(), parameter);
    }
}
