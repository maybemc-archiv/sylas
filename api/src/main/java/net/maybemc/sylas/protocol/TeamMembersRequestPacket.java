package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;

/**
 * @author Nico_ND1
 */
public class TeamMembersRequestPacket extends Packet {
    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
    }
}
