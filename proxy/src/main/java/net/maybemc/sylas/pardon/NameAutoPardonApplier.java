package net.maybemc.sylas.pardon;

import net.maybemc.sylas.repository.ban.BanRepository;
import net.maybemc.sylas.schema.ban.Ban;
import net.maybemc.sylas.schema.ban.PardonReason;
import net.md_5.bungee.api.connection.PendingConnection;

/**
 * @author Nico_ND1
 */
public class NameAutoPardonApplier implements AutoPardonApplier {
    private final String punishReasonId;
    private final String pardonReasonId;

    public NameAutoPardonApplier(String punishReasonId, String pardonReasonId) {
        this.punishReasonId = punishReasonId;
        this.pardonReasonId = pardonReasonId;
    }

    @Override
    public boolean applyPardon(BanRepository repository, Ban ban, PendingConnection connection) {
        if (!ban.getReason().getId().equals(punishReasonId)) {
            return false;
        }

        boolean nameFound = false;
        for (String appendant : ban.getAppendant().split(";")) {
            if (appendant.equals(connection.getName())) {
                nameFound = true;
                break;
            }
        }

        if (nameFound) {
            return false;
        }

        PardonReason pardonReason = repository.findPardonReason(pardonReasonId).orElseThrow(() -> new NullPointerException("Pardon reason not found: " + pardonReasonId));
        repository.pardon("sylas-name-checker", ban.getId(), pardonReason, connection.getName(), true, true);
        return true;
    }
}
