package net.maybemc.sylas.repository.exception;

/**
 * @author Nico_ND1
 */
public class UserNotFoundException extends RuntimeException implements TranslatableExceptionMessage {
    @Override
    public String getLocaleKey() {
        return "exception-usernotfound";
    }
}
