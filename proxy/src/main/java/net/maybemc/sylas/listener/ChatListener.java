package net.maybemc.sylas.listener;

import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.sylas.chat.filter.ChatFilterExecutor;
import net.maybemc.sylas.chat.filter.ChatFilterPunisher;
import net.maybemc.sylas.chat.filter.ChatFilterResult;
import net.maybemc.sylas.chat.log.ChatlogCollector;
import net.maybemc.sylas.schema.ban.Ban;
import net.maybemc.sylas.schema.ban.BanType;
import net.maybemc.sylas.schema.chatlog.ChatlogEntryFlag;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

/**
 * @author Nico_ND1
 */
public class ChatListener extends BanListener implements Listener {
    private final ChatFilterExecutor chatFilterExecutor;
    private final ChatlogCollector chatlogCollector;
    private final ChatFilterPunisher chatFilterPunisher;

    public ChatListener(LanguagePack languagePack, ChatFilterExecutor chatFilterExecutor, ChatlogCollector chatlogCollector) {
        super(languagePack);
        this.chatFilterExecutor = chatFilterExecutor;
        this.chatlogCollector = chatlogCollector;
        this.chatFilterPunisher = new ChatFilterPunisher();
    }

    @EventHandler(priority = Byte.MIN_VALUE)
    public void onChat(ChatEvent event) {
        if (!(event.getSender() instanceof ProxiedPlayer player)) {
            return;
        }

        if (!event.getMessage().isEmpty() && event.getMessage().charAt(0) == '/') {
            return;
        }

        try {
            Ban ban = repository.getActiveBan(player.getUniqueId(), BanType.MUTE).get(3L, TimeUnit.SECONDS);
            if (ban != null) {
                disallowChat(player, event, ban);
                return;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            Meybee.getLogger().log(Level.WARNING, "Error retrieving active ban of " + player, e);
        }

        onChat(player, event);
    }

    private void onChat(ProxiedPlayer player, ChatEvent event) {
        String message = event.getMessage();
        ChatFilterResult filterResult = chatFilterExecutor.test(player, message);
        if (filterResult != null && filterResult.isBlocked()) {
            event.setCancelled(true);

            ChatlogEntryFlag.Type flagType = filterResult.getFlagType();
            if (flagType == null) {
                player.sendMessage(languagePack.translate(player.getUniqueId(), "chat-restricted"));
            } else {
                player.sendMessage(languagePack.translate(player.getUniqueId(), "chat-restricted-" + flagType.name()));
            }
            return;
        }

        ChatlogEntryFlag[] flags;
        if (filterResult == null) {
            flags = new ChatlogEntryFlag[0];
        } else {
            flags = filterResult.getFlags().toArray(new ChatlogEntryFlag[0]);
        }

        chatlogCollector.chat(player.getUniqueId(), message, flags);
        if (filterResult != null) {
            chatFilterPunisher.apply(player.getUniqueId(), filterResult);
        }
    }

    private void disallowChat(ProxiedPlayer player, ChatEvent event, Ban ban) {
        if (!ban.isActive()) {
            Meybee.getLogger().warning("Ban is present for " + ban.getTarget() + " (" + ban.getId() + ") but its not active");
            return;
        }

        event.setCancelled(true);
        player.sendMessages(languagePack.translate(ban.getTarget(), "chat-banned",
            ban.getId(),
            ban.getCreator(),
            languagePack.translate(ban.getTarget(), "ban-reason-" + ban.getReason().getId()),
            ban.getAppendant(),
            formatDuration(ban.getTarget(), ban)
        ).split("\n"));
    }
}
