package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;

import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class TeamChatMessagePacket extends Packet {
    private int id;
    private long time;
    private UUID sender;
    private String message;
    private String server;

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        byteBuf.writeInt(id);
        byteBuf.writeLong(time);
        writeUUID(byteBuf, sender);
        writeString(byteBuf, message);
        writeString(byteBuf, server);
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
        id = byteBuf.readInt();
        time = byteBuf.readLong();
        sender = readUUID(byteBuf);
        message = readString(byteBuf);
        server = readString(byteBuf);
    }
}
