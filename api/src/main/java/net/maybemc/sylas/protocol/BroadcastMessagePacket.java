package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class BroadcastMessagePacket extends Packet {
    private String message;

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        writeString(byteBuf, message);
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
        message = readString(byteBuf);
    }
}
