package net.maybemc.sylas.chat.filter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.user.User;
import net.maybemc.meybee.api.user.UserRepository;
import net.maybemc.meybee.api.user.scope.UserScope;
import net.maybemc.meybee.api.user.scope.UserScopeKey;
import net.maybemc.sylas.schema.chatlog.ChatlogEntryFlag;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Nico_ND1
 */
public class SpamChatFilter implements ChatFilter {
    private static final UserScopeKey<Tracker> TRACKER_SCOPE_KEY = UserScopeKey.createKey("sylas-chatfilter-spam-tracker", Tracker.class);

    private final long messageLifetime;
    private final TimeUnit messageLifetimeUnit;
    private final int maxMessagesInLifetime;

    public SpamChatFilter(long messageLifetime, TimeUnit messageLifetimeUnit, int maxMessagesInLifetime) {
        this.messageLifetime = messageLifetime;
        this.messageLifetimeUnit = messageLifetimeUnit;
        this.maxMessagesInLifetime = maxMessagesInLifetime;
    }

    public SpamChatFilter(JsonObject configuration, JsonDeserializationContext context) {
        this(
            configuration.get("messageLifetime").getAsLong(),
            TimeUnit.valueOf(configuration.get("messageLifetimeUnit").getAsString()),
            configuration.get("maxMessagesInLifetime").getAsInt()
        );
    }

    @Override
    public ChatFilterResult testMessage(ProxiedPlayer player, String message) {
        User user = Meybee.getInstance().getProvider(UserRepository.class).getUser(player.getUniqueId());
        UserScope<Tracker> userScope = user.getUserScope(TRACKER_SCOPE_KEY);
        if (!userScope.hasValueLoaded()) {
            userScope.setValue(new Tracker());
        }

        Tracker tracker = userScope.getValueSyncSilent();
        tracker.addMessage(message);
        tracker.cleanup();

        if (tracker.isSpamDetected()) {
            List<ChatlogEntryFlag> flags = new ArrayList<>();
            flags.add(new ChatlogEntryFlag(0, message.length() - 1, ChatlogEntryFlag.Type.SPAM));
            return new ChatFilterResult(flags, Collections.emptyList());
        }
        return null;
    }

    private class Tracker {
        private final List<Message> messages = new ArrayList<>();

        public void addMessage(String message) {
            messages.add(new Message(System.currentTimeMillis(), message));
        }

        public void cleanup() {
            messages.removeIf(message -> message.time + messageLifetimeUnit.toMillis(messageLifetime) < System.currentTimeMillis());
        }

        public boolean isSpamDetected() {
            if (areTopTwoMessagesSame()) {
                return true;
            }
            return messages.size() > maxMessagesInLifetime;
        }

        private boolean areTopTwoMessagesSame() {
            if (messages.size() < 2) {
                return false;
            }

            Message firstMessage = messages.get(messages.size() - 2);
            Message secondMessage = messages.get(messages.size() - 1);
            return firstMessage.message.equalsIgnoreCase(secondMessage.message);
        }
    }

    private static record Message(long time, String message) {
    }
}
