package net.maybemc.sylas.command;

import lombok.Data;
import net.maybemc.meybee.api.Meybee;
import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.maybemc.meybee.api.permission.Group;
import net.maybemc.meybee.api.permission.GroupRepository;
import net.maybemc.meybee.api.protocol.client.PacketClient;
import net.maybemc.sylas.protocol.TeamMembersRequestPacket;
import net.maybemc.sylas.protocol.TeamMembersResponsePacket;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class TeamMembersCommand extends Command {
    private final LanguagePack languagePack;
    private final PacketClient packetClient;

    public TeamMembersCommand(LanguagePack languagePack, PacketClient packetClient) {
        super("teamlist", "sylas.teammembers.command", "teamonline", "teammembers");
        this.languagePack = languagePack;
        this.packetClient = packetClient;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) sender;

        Futures.addCallback(packetClient.sendPacket(new TeamMembersRequestPacket(), TeamMembersResponsePacket.class), new FutureCallback<TeamMembersResponsePacket>() {
            @Override
            public void onSuccess(TeamMembersResponsePacket responsePacket) {
                List<TeamMember> members = transform(responsePacket.getMembers());
                if (members.isEmpty()) {
                    player.sendMessage(languagePack.translate(player.getUniqueId(), "teammembers-command-success-empty"));
                } else {
                    ComponentBuilder componentBuilder = new ComponentBuilder().append(TextComponent.fromLegacyText(languagePack.translate(player.getUniqueId(), "teammembers-command-success-header")));
                    for (TeamMember member : members) {
                        componentBuilder.append("\n")
                            .append(languagePack.translate(player.getUniqueId(), "teammembers-command-success-entry", member.group.getColor() + member.name, member.server))
                            .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] {
                                new TextComponent(languagePack.translate(player.getUniqueId(), "teammembers-command-success-entry-hover", member.group.getColor() + member.name))
                            })).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/jumpto " + member.name));
                    }

                    player.sendMessage(componentBuilder.create());
                }
            }

            @Override
            public void onFailure(@NotNull Throwable throwable) {
                FutureCallback.super.onFailure(throwable);
                player.sendMessage(languagePack.translate(player.getUniqueId(), "teammembers-command-error", throwable.getMessage()));
            }
        }, 10L, TimeUnit.SECONDS);
    }

    private List<TeamMember> transform(List<TeamMembersResponsePacket.TeamMember> members) {
        GroupRepository groupRepository = Meybee.getInstance().getProvider(GroupRepository.class);
        return members.stream()
            .map(teamMember -> {
                Group group = groupRepository.getGroupByID(teamMember.getGroupId()).orElseGet(() -> groupRepository.getTotalGroups().get(0));
                return new TeamMember(teamMember.getName(), group, teamMember.getServer());
            }).collect(Collectors.toList());
    }

    @Data
    private static class TeamMember {
        private final String name;
        private final Group group;
        private final String server;
    }
}
