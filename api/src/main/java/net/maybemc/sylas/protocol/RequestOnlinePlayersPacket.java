package net.maybemc.sylas.protocol;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.maybemc.meybee.api.protocol.Packet;
import net.maybemc.meybee.api.protocol.exception.PacketReadWriteException;

import java.util.List;
import java.util.UUID;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class RequestOnlinePlayersPacket extends Packet {
    private List<UUID> onlinePlayers;

    @Override
    public void write(ByteBuf byteBuf) throws PacketReadWriteException {
        writeList(byteBuf, onlinePlayers, this::writeUUID);
    }

    @Override
    public void read(ByteBuf byteBuf) throws PacketReadWriteException {
    }
}
