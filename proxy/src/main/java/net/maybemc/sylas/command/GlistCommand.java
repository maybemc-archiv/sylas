package net.maybemc.sylas.command;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.ServiceEnvironmentType;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import de.dytanic.cloudnet.ext.bridge.player.NetworkServiceInfo;
import net.maybemc.meybee.api.i18n.LanguagePack;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Nico_ND1
 */
public class GlistCommand extends Command {
    private final LanguagePack languagePack;

    public GlistCommand(LanguagePack languagePack) {
        super("glist", "sylas.command.glist");
        this.languagePack = languagePack;
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);
        playerManager.getOnlinePlayersAsync(ServiceEnvironmentType.BUNGEECORD).onComplete(iCloudPlayers -> {
            sendPlayers((ProxiedPlayer) commandSender, iCloudPlayers);
        }).onFailure(throwable -> {
            throwable.printStackTrace();
            commandSender.sendMessage(languagePack.translate(((ProxiedPlayer) commandSender).getUniqueId(), "error"));
        });
    }

    private void sendPlayers(ProxiedPlayer sender, List<? extends ICloudPlayer> iCloudPlayers) {
        Map<String, List<String>> players = new HashMap<>();
        for (ICloudPlayer iCloudPlayer : iCloudPlayers) {
            NetworkServiceInfo connectedService = iCloudPlayer.getConnectedService();
            if (connectedService == null) {
                players.computeIfAbsent("N/A", s -> new ArrayList<>()).add(iCloudPlayer.getName());
            } else {
                players.computeIfAbsent(connectedService.getServerName(), s -> new ArrayList<>()).add(iCloudPlayer.getName());
            }
        }

        long totalPlayers = players.values().stream().mapToLong(Collection::size).sum();
        players.keySet().stream().sorted().forEachOrdered(serverName -> {
            int serverPlayers = players.get(serverName).size();

            String playersString;
            if (serverPlayers > 10) {
                playersString = String.join(", ", players.get(serverName).subList(0, 10));
                playersString += languagePack.translate(sender.getUniqueId(), "glist-result-entry-big", serverPlayers - 10);
            } else {
                playersString = String.join("§7, ", players.get(serverName));
            }

            sender.sendMessage(new ComponentBuilder().append(TextComponent.fromLegacyText(
                languagePack.translate(
                    sender.getUniqueId(),
                    "glist-result-entry",
                    serverName,
                    serverPlayers,
                    playersString
                )
            )).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/server " + serverName)).create());
        });
        sender.sendMessage(languagePack.translate(sender.getUniqueId(), "glist-result-header", totalPlayers, players.size()));
    }
}
