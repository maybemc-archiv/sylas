package net.maybemc.sylas.notify;

import lombok.AllArgsConstructor;

/**
 * @author Nico_ND1
 */
@AllArgsConstructor
public enum NotifyType {
    PUNISH_LOG(true),
    TEAM_CHAT(true),
    TEAM_CONNECTIONS(true),
    REPORT_INFO(true),
    BAN_BYPASS(false);

    private final boolean defaultValue;

    public boolean getDefaultValue() {
        return defaultValue;
    }
}
