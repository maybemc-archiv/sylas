package net.maybemc.sylas.protocol;

import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServer;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * @author Nico_ND1
 */
public class TeamMembersRequestPacketListener implements PacketListener<TeamMembersRequestPacket> {
    private final PacketServer packetServer;

    public TeamMembersRequestPacketListener(PacketServer packetServer) {
        this.packetServer = packetServer;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection connection, TeamMembersRequestPacket packet) {
        CompletableFuture<HandleResult> future = new CompletableFuture<>();

        List<PacketServerConnection> connections = packetServer.findConnectionsByGroup("proxy");
        if (connections.isEmpty()) {
            return instantResult(stopHandling());
        }

        List<TeamMembersResponsePacket> responses = new ArrayList<>();
        Lock responsesLock = new ReentrantLock();
        AtomicInteger responsesGot = new AtomicInteger();
        for (PacketServerConnection serverConnection : connections) {
            Futures.addCallback(packetServer.sendPacket(serverConnection, packet, TeamMembersResponsePacket.class), new FutureCallback<TeamMembersResponsePacket>() {
                @Override
                public void onSuccess(TeamMembersResponsePacket responsePacket) {
                    try {
                        responsesLock.lock();
                        responses.add(responsePacket);
                    } finally {
                        responsesLock.unlock();
                    }

                    handle();
                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {
                    FutureCallback.super.onFailure(throwable);
                    handle();
                }

                private void handle() {
                    if (responsesGot.incrementAndGet() == connections.size()) {
                        List<TeamMembersResponsePacket.TeamMember> members = responses.stream()
                            .flatMap(responsePacket -> responsePacket.getMembers().stream())
                            .collect(Collectors.toList());

                        future.complete(stopHandling(new TeamMembersResponsePacket(members)));
                    }
                }
            }, 5L, TimeUnit.SECONDS);
        }

        return future;
    }
}
