package net.maybemc.sylas.repository.exception;

import lombok.Getter;

import javax.ws.rs.core.Response;

/**
 * @author Nico_ND1
 */
@Getter
public class UnknownStatusException extends RuntimeException {
    private final int statusCode;
    private final String statusReasonPhrase;

    public UnknownStatusException(int statusCode, String statusReasonPhrase) {
        super("Status code is " + statusCode + " (" + statusReasonPhrase + ")");
        this.statusCode = statusCode;
        this.statusReasonPhrase = statusReasonPhrase;
    }

    public UnknownStatusException(Response response) {
        this(response.getStatus(), response.getStatusInfo().getReasonPhrase());
    }
}
