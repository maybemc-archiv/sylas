package net.maybemc.sylas.protocol;

import net.maybemc.meybee.api.future.FutureCallback;
import net.maybemc.meybee.api.future.Futures;
import net.maybemc.meybee.api.protocol.listener.PacketListener;
import net.maybemc.meybee.api.protocol.server.PacketServer;
import net.maybemc.meybee.api.protocol.server.PacketServerConnection;
import net.maybemc.sylas.schema.chatlog.ChatlogEntry;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Nico_ND1
 */
public class ChatlogRequestListener implements PacketListener<ChatlogRequestPacket> {
    private final PacketServer packetServer;

    public ChatlogRequestListener(PacketServer packetServer) {
        this.packetServer = packetServer;
    }

    @Override
    public CompletableFuture<HandleResult> handle(PacketServerConnection connection, ChatlogRequestPacket packet) {
        CompletableFuture<HandleResult> future = new CompletableFuture<>();

        List<PacketServerConnection> connections;
        if (packet.getPTarget().length() == 36) {
            connections = packetServer.findConnectionsByGroup("log-receiver");
        } else {
            Optional<PacketServerConnection> optionalConnection = packetServer.findConnection(packet.getPTarget());
            if (!optionalConnection.isPresent()) {
                future.complete(stopHandling());
                return future;
            }

            connections = new ArrayList<>();
            connections.add(optionalConnection.get());
        }

        if (connections.isEmpty()) {
            return instantResult(stopHandling());
        }

        List<ChatlogResponsePacket> responses = new ArrayList<>();
        Lock responsesLock = new ReentrantLock();
        AtomicInteger responsesGot = new AtomicInteger();
        for (PacketServerConnection chatlogReceiver : connections) {
            Futures.addCallback(packetServer.sendPacket(chatlogReceiver, packet, ChatlogResponsePacket.class), new FutureCallback<ChatlogResponsePacket>() {
                @Override
                public void onSuccess(ChatlogResponsePacket chatlogResponsePacket) {
                    try {
                        responsesLock.lock();
                        responses.add(chatlogResponsePacket);
                    } finally {
                        responsesLock.unlock();
                    }

                    handle();
                }

                @Override
                public void onFailure(@NotNull Throwable throwable) {
                    FutureCallback.super.onFailure(throwable);
                    handle();
                }

                private void handle() {
                    if (responsesGot.incrementAndGet() == connections.size()) {
                        ChatlogEntry[] entries = responses.stream()
                            .map(ChatlogResponsePacket::getEntries)
                            .flatMap(Arrays::stream)
                            .toArray(ChatlogEntry[]::new);

                        future.complete(stopHandling(new ChatlogResponsePacket(entries)));
                    }
                }
            }, 5L, TimeUnit.SECONDS);
        }

        return future;
    }
}
